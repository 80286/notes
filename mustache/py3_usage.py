#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pystache
import json


def readLines(path: str) -> str:
    lines = ""

    with open(path, "r") as f:
        lines = f.read()

    return lines


def main():
    exampleInputJson = json.loads(readLines("example_input.json"))
    templateStr = readLines("template.mustache")

    print("Input json:")
    print(exampleInputJson)

    resultStr = pystache.render(templateStr, exampleInputJson)
    print("Result:")
    print(resultStr)


if __name__ == "__main__":
    main()
