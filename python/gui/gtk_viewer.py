#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

thisPath = os.path.dirname(os.path.realpath(__file__))
sys.path.append(str(os.path.join(thisPath, "src")))

from viewers.GtkLogViewer import *
from viewers.LogViewer import *


if __name__ == "__main__":
    argv = sys.argv
    LogViewer.doMain(argv, GtkLogViewer(argv))
