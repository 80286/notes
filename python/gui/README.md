## Python GUI Examples: different implementations of simple android log viewer

This simple test app:

- reads by default test log (`logs/android_log.txt`)
- collects list of android tags found in loaded log file and allows to reduce output to the selected tag
- allows to do simple search through the content of last reduced output


### Tk

Requirements:

- [tkinter](https://docs.python.org/3/library/tkinter.html#module-tkinter)

Running:
```
./tk_viewer.py [log_path]
```

Sources:

- `src/viewers/TkLogViewer.py`

### PyGTK

Requirements:

- [PyGTK](https://pygobject.readthedocs.io/en/latest/getting_started.html)
- [Glade](https://glade.gnome.org/) - used to create and adjust `.glade` layouts

Running:
```
./gtk_viewer.py [log_path]
```
Sources:

- `src/viewers/GtkLogViewer.py`
