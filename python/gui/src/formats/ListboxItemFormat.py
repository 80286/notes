class ListboxItemFormat(object):
    sep = "|"

    def build(self, idx, line):
        return f"{idx} {self.sep} {line}"

    def extractIdx(self, itemStr):
        sepIdx = itemStr.find(self.sep)

        if sepIdx <= 0:
            return None

        return itemStr[:sepIdx].strip()
