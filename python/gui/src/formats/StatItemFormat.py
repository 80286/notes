class StatItemFormat(object):
    sep = ":"
    
    def extract(self, itemStr):
        fields = itemStr.split(self.sep)
        return fields[1] if len(fields) == 2 else ""

    def build(self, numOfOccurences, value):
        itemFormat = "{0:>5}" + self.sep + "{1}"
        return itemFormat.format(numOfOccurences, value)
