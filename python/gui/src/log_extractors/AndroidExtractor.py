import re

class BaseExtractor(object):
    @staticmethod
    def extract(line, pattern, keys):
        obj = re.match(pattern, line.strip())

        return {key: obj.group(key) for key in keys} if obj else None

class AndroidExtractor(object):
    # 11-13 11:55:31.923: I/Appboy v3.7.1 .bo.app.do(15243): Notifying confirmAndUnlock listeners

    keys = ["date", "tag_type", "tag", "msg"]
    pattern = r"^(?P<{0}>\d\d-\d\d \d\d:\d\d:\d\d[.]\d+): (?P<{1}>[A-Za-z0-9])/(?P<{2}>\S+): (?P<{3}>.*)$".format(*keys)
    
    @staticmethod
    def isIgnoredKey(key):
        return key == "msg" or key == "date"

    def extract(self, line):
        return BaseExtractor.extract(line, self.pattern, self.keys)
