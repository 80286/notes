import gi

gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")
from gi.repository import Gdk, Gtk

from utils.CommonTools import *


class UiLayoutLoader(object):
    def __init__(self, path, handler, *objectIds):
        builder = Gtk.Builder()

        if objectIds:
            builder.add_objects_from_file(path, objectIds)
        else:
            builder.add_from_file(path)

        self._builder = builder
        self._path = path

        if handler:
            self.connect(handler)
    
    def get(self, itemId):
        item = self._builder.get_object(itemId)

        if not item:
            raise Exception("Item '{0}' not defined in layout file '{1}'.".format(itemId, self._path))

        return item

    def connect(self, handler):
        self._builder.connect_signals(handler)


class UiStyleLoader(object):
    stylePriority = 600
    
    @staticmethod
    def loadStyle(path):
        provider = Gtk.CssProvider()
        provider.load_from_path(path)
        screen = Gdk.Display.get_default_screen(Gdk.Display.get_default())
        Gtk.StyleContext.add_provider_for_screen(screen, provider, UiStyleLoader.stylePriority)


class UiTextBuffer(object):
    def __init__(self, gtkTextBuffer):
        self._buffer = gtkTextBuffer
        self._setupTags()

    def highlightLine(self, lineIdx):
        start = self.getLineIter(lineIdx)
        end = self.getLineIter(lineIdx + 1)
        self._buffer.apply_tag(self._tags["grey_selection"], start, end)

    def clearHighlights(self):
        start = self.getStartIter()
        end = self.getEndIter()
        self._buffer.remove_all_tags(start, end)

    def _setupTags(self):
        colors = [
            ("red", { "foreground": "red" }),
            ("grey_selection", { "background": "gray" })
        ]

        tags = {}
        buffer = self._buffer

        for (tagId, tag) in colors:
            tags[tagId] = buffer.create_tag(tagId, **tag)

        self._tags = tags

    def clear(self):
        buffer = self._buffer
        buffer.delete(buffer.get_start_iter(), buffer.get_end_iter())

    def getLineIter(self, lineIdx):
        return self._buffer.get_iter_at_line(lineIdx)

    def getStartIter(self):
        return self.getLineIter(0)

    def getEndIter(self):
        buffer = self._buffer
        size = buffer.get_line_count()

        return self.getLineIter(size)

    def append(self, line):
        it = self.getEndIter()
        self._buffer.insert(it, line)

    def getLinesText(self):
        return self._buffer.get_text(self.getStartIter(), self.getEndIter(), False)

    def getLines(self):
        return self.getLinesText().split("\n")


class UiTextView(object):
    def __init__(self, gtkTextView):
        self._textView = gtkTextView
        self._buffer = UiTextBuffer(gtkTextView.get_buffer())

    def clear(self):
        self._buffer.clear()

    def append(self, line):
        self._buffer.append(line)

    def buffer(self):
        return self._buffer

    def _scrollTo(self, it):
        self._textView.scroll_to_iter(it, 0.0, False, 0.0, 0.0)

    def scrollToStart(self):
        self._scrollTo(self._buffer.getStartIter())

    def scrollToEnd(self):
        self._scrollTo(self._buffer.getEndIter())

    def getLines(self):
        return self._buffer.getLines()
    
    def scrollToLine(self, dstLineIdx):
        self._scrollTo(self._buffer.getLineIter(dstLineIdx))

    def highlightLine(self, lineIdx):
        self._buffer.highlightLine(lineIdx)

    def clearHighlights(self):
        self._buffer.clearHighlights()


class UiLabel(object):
    def __init__(self, gtkLabel):
        self._label = gtkLabel

    def set(self, text):
        self._label.set_label(text)


class UiListbox(object):
    def __init__(self, gtkListbox):
        self._listbox = gtkListbox

    def _show(self):
        self._listbox.show_all()

    def _clear(self):
        childs = self._listbox.get_children()

        for child in childs:
            self._listbox.remove(child)

    def _append(self, itemStr):
        label = Gtk.Label(itemStr)
        label.set_xalign(0.0)

        row = Gtk.ListBoxRow()
        row.add(label)

        self._listbox.add(row)

    def clear(self):
        self._clear()
        self._show()

    def append(self, itemStr):
        self._append(itemStr)
        self._show()

    def fill(self, listOfItems):
        self._clear()

        for itemStr in listOfItems:
            self._append(itemStr)

        self._show()

    def connect(self, callbackId, callback):
        self._listbox.connect(callbackId, callback)

    def getSelectedRowIdx(self):
        return self._listbox.get_selected_row().get_index()

    def getItemLabel(self, idx):
        row = self._listbox.get_row_at_index(self.getSelectedRowIdx())
        return row.get_children()[0].get_label()


class UiCheckbox(object):
    def __init__(self, gtkCheckbox):
        self._box = gtkCheckbox

    def isActive(self):
        return self._box.get_active()


class UiEntry(object):
    def __init__(self, gtkEntry):
        self._entry = gtkEntry

    def get(self):
        return self._entry.get_text()
    
    def set(self, text):
        self._entry.set_text(text)

    def clear(self):
        self.set("")


class GtkFilterWidget(object):
    def __init__(self, builder, inputText, itemFormat):
        self._inputText = inputText
        self._listboxItemFormat = itemFormat

        self.listbox = UiListbox(builder.get("lbox"))
        self.entry = UiEntry(builder.get("entry"))
        self.ignoreCaseCheckbox = UiCheckbox(builder.get("cbox_case"))
        self.wholeLineMatchCheckbox = UiCheckbox(builder.get("cbox_line"))

        builder.connect(self)

    def onKeyPressEvent(self, gtkEntry, gdkEventKey):
        val = gdkEventKey.keyval

        if val == Gdk.KEY_Escape:
            self.entry.clear()
            self._inputText.clearHighlights()
        elif val == Gdk.KEY_Return:
            wholeLineMatch = self.wholeLineMatchCheckbox.isActive()
            ignoreCase = self.ignoreCaseCheckbox.isActive()
            pattern = self.entry.get().strip()
            lines = self._inputText.getLines()

            fmt = lambda idx, line: self._listboxItemFormat.build(idx, line)
            error, result = SearchTools.matchPattern(pattern, lines, fmt, wholeLineMatch, ignoreCase)

            if error:
                self.listbox.fill([str(result)])
                return

            self.listbox.fill(result)

    def onRowActivate(self, listbox, row):
        inputText = self._inputText

        idx = self.listbox.getSelectedRowIdx()
        activatedValue = self.listbox.getItemLabel(idx)
        dstLineIdx = int(self._listboxItemFormat.extractIdx(activatedValue))

        inputText.clearHighlights()
        inputText.scrollToLine(dstLineIdx)
        inputText.highlightLine(dstLineIdx)
