#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import ttk
from utils.CommonTools import *


class TkCustomText(Text):
    """ http://stackoverflow.com/questions/3781670/how-to-highlight-text-in-a-tkinter-text-widget """

    outputTags = {
        "black":    "#000000",
        "red":      "#ff0000",
        "green":    "#00ff00",
        "blue":     "#0000ff",
        "grey":     "#888888",
        "purple":   "#800080",
        "olive":    "#808000",
        "teal":     "#008080",
        "navy":     "#000080"
    }

    def __init__(self, *args, **kwargs):
        Text.__init__(self, *args, **kwargs)

    def clearHighlights(self):
        for tag in TkCustomText.outputTags:
            self.tag_remove(tag, "1.0", END)

    def highlightPattern(self, pattern, tag, start="1.0", end="end", regexp=True):
        '''Apply the given tag to all text that matches the given pattern

        If 'regexp' is set to True, pattern will be treated as a regular
        expression according to Tcl's regular expression syntax.
        '''

        start = self.index(start)
        end = self.index(end)
        self.mark_set("matchStart", start)
        self.mark_set("matchEnd", start)
        self.mark_set("searchLimit", end)

        count = IntVar()
        while True:
            index = self.search(pattern, "matchEnd", "searchLimit", count=count, regexp=regexp)
            if index == "":
                break
            if count.get() == 0:
                break
            self.mark_set("matchStart", index)
            self.mark_set("matchEnd", "%s+%sc" % (index, count.get()))
            self.tag_add(tag, "matchStart", "matchEnd")

    def setTags(self, tags):
        for tag, rgb in tags.items():
            self.tag_configure(tag, foreground=rgb)

    def apply(self, highlighter):
        if not isinstance(highlighter, list):
            highlighter = highlighter()

        for (pattern, color) in highlighter:
            self.highlightPattern(pattern, color)


class TkTools(object):
    @staticmethod
    def buildOutputFrame(parent):
        outputFrame = ttk.Frame(parent)
        outputFrame.pack(side=TOP, fill=BOTH, expand=True)

        outputText = TkCustomText(outputFrame)

        xScrollbar = Scrollbar(outputFrame, orient=HORIZONTAL)
        xScrollbar.config(command=outputText.xview)
        xScrollbar.pack(side=BOTTOM, fill=X)

        yScrollbar = Scrollbar(outputFrame)
        yScrollbar.config(command=outputText.yview)
        yScrollbar.pack(side=RIGHT, fill=Y)

        outputText.config(xscrollcommand=xScrollbar.set, yscrollcommand=yScrollbar.set)

        return (outputFrame, outputText)

    @staticmethod
    def buildLabeledListbox(parent, title, buttonsFrameFunction=None):
        frame = ttk.Frame(parent)
        listbox = Listbox(frame)
        listboxTitle = None

        if title:
            listboxTitle = Label(frame, text=title, bg="black", fg="white")
            listboxTitle.pack(side=TOP, padx=5, pady=5, fill=X)

        if buttonsFrameFunction:
            buttonsFrameFunction(frame)

        scrollbar = Scrollbar(frame)
        scrollbar.pack(side=RIGHT, fill=Y)
        scrollbar.config(command=listbox.yview)

        listbox.config(yscrollcommand=scrollbar.set)

        listbox.pack(fill=BOTH, expand=True)
        frame.pack(fill=BOTH, expand=True)

        return (listbox, frame, listboxTitle)

    @staticmethod
    def buildListbox(parent, **kwargs):
        listboxFrame = ttk.Frame(parent)

        listboxFrame.pack(side=kwargs.get("side", "left"), fill=kwargs.get("fill", BOTH), expand=kwargs.get("expand", False))

        (listbox, _, listboxTitle) = TkTools.buildLabeledListbox(listboxFrame, kwargs.get("title"), kwargs.get("buttonsFrameFunction", None))

        return (listbox, listboxFrame, listboxTitle)

    @staticmethod
    def fillListbox(listbox, content):
        listbox.delete(0, listbox.size())

        for i, line in enumerate(content):
            listbox.insert(i, line)

    @staticmethod
    def textClear(text):
        text.config(state=NORMAL)
        text.delete(1.0, END)
        text.config(state=DISABLED)

    @staticmethod
    def textSkipToLine(text, dstLineIdx):
        text.see("{0}.0".format(dstLineIdx))

    @staticmethod
    def textResetScrollbar(text):
        text.see(END)


class TkFilterWidget(object):
    def __init__(self, parent, inputText, listboxItemFormat, **kwargs):
        self.parent = parent
        self.text = inputText
        self.listboxItemFormat = listboxItemFormat

        self.inputMatchWholeLine = IntVar()
        self.inputIgnoreCase = IntVar()
        self.frame = ttk.Frame(parent)
        self.buildFilterWidget(**kwargs)

    def reset(self):
        self.outputListbox.delete(0, self.outputListbox.size())
        self.outputListbox.config(height=1)
        self.text.clearHighlights()

    def buildFilterInputEntry(self, onInputAction):
        self.inputEntry = Entry(self.parent)
        self.inputEntry.bind('<Key>', lambda event: onInputAction(event, self))

        defs = [
            (self.inputMatchWholeLine,  "Whole line match", 1),
            (self.inputIgnoreCase,      "Ignore case",      1),
        ]

        for (var, title, defaultValue) in defs:
            var.set(defaultValue)
            button = Checkbutton(self.inputEntry, text=title, variable=var)
            button.pack(side=RIGHT)

    def buildFilterWidget(self, **kwargs):
        onClickCallback = kwargs.get("onClick")
        onPressKey = kwargs.get("onPressKey", self.onPressKey)

        self.buildFilterInputEntry(onPressKey)

        (self.outputListbox, self.outListboxFrame, _) = TkTools.buildListbox(self.frame, title=kwargs.get("title"), expand=kwargs.get("expand"))
        self.outputListbox.config(height=1, highlightthickness=2)
        self.outputListbox.bind('<Double-Button-1>', lambda event: onClickCallback(self, event))

        self.inputEntry.pack(fill=X)
        self.frame.pack(expand=True, fill=BOTH)

    def genericOnPressKey(self, event):
        inputEntry = self.inputEntry
        listbox = self.outputListbox
        text = self.text

        key = repr(event.char).strip("'")
        pattern = inputEntry.get()

        if key == r'\r':
            wholeLineMatch = self.inputMatchWholeLine.get()
            ignoreCase = self.inputIgnoreCase.get()
            lines = text.get('1.0', END).splitlines()

            fmt = lambda idx, line: self.listboxItemFormat.build(idx, line)
            error, result = SearchTools.matchPattern(pattern, lines, fmt, wholeLineMatch, ignoreCase)

            if error:
                TkTools.fillListbox(listbox, [str(result)])
                return False

            text.clearHighlights()
            text.apply([(pattern, "green")])

            TkTools.fillListbox(listbox, result)
            listbox.config(height=5)

            return True
        elif key == r'\x1b':
            inputEntry.delete(0, "end")
            self.reset()
            return False
        else:
            return False

    @staticmethod
    def getOnPressKeyStrResponse(doBreak):
        return "break" if doBreak else ""

    def onPressKey(self, event, filterObj):
        return filterObj.getOnPressKeyStrResponse(self.genericOnPressKey(event))


# https://gist.github.com/mp035/9f2027c3ef9172264532fcd6262f3b01
class TkScrollFrame(Frame):
    def __init__(self, parent):
        super().__init__(parent)

        self.canvas = Canvas(self, borderwidth=0)
        self.viewPort = Frame(self.canvas)
        self.vsb = Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.vsb.set)

        self.vsb.pack(side="right", fill="y")
        self.canvas.pack(side="left", fill="both", expand=True)
        self.canvas_window = self.canvas.create_window((4, 4), window=self.viewPort, anchor="nw", tags="self.viewPort")

        self.viewPort.bind("<Configure>", self.onFrameConfigure)
        self.canvas.bind("<Configure>", self.onCanvasConfigure)

        self.onFrameConfigure(None)

    def onFrameConfigure(self, event):
        '''Reset the scroll region to encompass the inner frame'''
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def onCanvasConfigure(self, event):
        '''Reset the canvas window to encompass inner frame when required'''
        canvas_width = event.width
        self.canvas.itemconfig(self.canvas_window, width=canvas_width)
