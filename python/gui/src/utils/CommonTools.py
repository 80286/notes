import re


class SearchTools(object):
    @staticmethod
    def matchPattern(inputPattern, inputLines, fmt, wholeLineMatch, ignoreCase):
        reFlags = re.IGNORECASE if ignoreCase else 0
        reObj = None

        try:
            reObj = re.compile(inputPattern, flags=reFlags)
        except Exception as e:
            return True, str(e)

        if wholeLineMatch:
            inputPattern = f"^.*{inputPattern}.*$"

        filteredOutput = [fmt(i, line) for i, line in enumerate(inputLines) if reObj and reObj.search(line)]

        return False, filteredOutput
