from log_extractors.AndroidExtractor import *


class LogObj(object):
    def __init__(self, lines):
        self._logExtractor = AndroidExtractor()
        self.fields = []

        self._extractDetails(lines)
        self._buildStats()

    def _extractDetails(self, lines):
        for i, line in enumerate(lines):
            lineDetails = self._logExtractor.extract(line)

            if not lineDetails:
                continue

            lineDetails["idx"] = i
            lineDetails["line"] = line

            self.fields.append(lineDetails)

    def _isIgnoredKey(self, key):
        return key == "idx" or key == "line" or self._logExtractor.isIgnoredKey(key)

    def _buildStats(self):
        isIgnoredKey = self._isIgnoredKey
        stats = {k:{} for k in self._logExtractor.keys if not isIgnoredKey(k)}

        for field in self.fields:
            for key, value in field.items():
                if isIgnoredKey(key):
                    continue

                currentValue = stats[key].get(value)

                if currentValue is None:
                    stats[key][value] = 1
                else:
                    stats[key][value] += 1

        self.stats = stats

    def getMaxValueLength(self, key):
        lengths = [len(field[key]) for field in self.fields]
        return max(lengths) if lengths else 15

    def buildShortForm(self, field, length=None):
        if length:
            fmt = "{tag:<" + str(length) + "}: {msg}"
            return fmt.format(tag=field["tag"], msg=field["msg"])
        else:
            return f"{field['tag']}: {field['msg']}"
    
    def getSortedStatItems(self, statKey, statItemFmt):
        sortFunc = lambda val: val[1]

        items = list(self.stats[statKey].items())
        items.sort(key=sortFunc, reverse=True)
        sortedItems = [statItemFmt.build(numOfOccurences, key) for key, numOfOccurences in items]

        return sortedItems
