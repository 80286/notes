from LogObj import *
from DisplaySettings import *


class LogViewer(object):
    def __init__(self, gui):
        self._gui = gui
        self._displaySettings = DisplaySettings()

    def loadLogObj(self, logObj):
        settings = self._displaySettings

        gui = self._gui
        gui.setActiveSettings(settings)
        gui.setReloadCallback(lambda: self.loadLogObj(logObj))

        padding = logObj.getMaxValueLength("tag")

        gui.onInsertBegin()

        for field in logObj.fields:
            if settings.isFieldAllowed(field):
                shortFormStr = logObj.buildShortForm(field, padding) + "\n"
                gui.onFieldMatch(field, shortFormStr)
        
        gui.onInsertEnd(logObj)

    def loadLog(self, lines):
        logObj = LogObj(lines)
        self.loadLogObj(logObj)

    def loadLogFile(self, logPath):
        print("Loading log: '{0}'...".format(logPath))

        with open(logPath, "r", errors="replace") as f:
            lines = f.readlines()
            self.loadLog(lines)

    @staticmethod
    def doMain(argv, viewer):
        logViewer = LogViewer(viewer)

        logPath = argv[-1] if len(argv) == 2 else "logs/android_log.txt"
        logViewer.loadLogFile(logPath)

        viewer.main()
