from formats.StatItemFormat import *
from formats.ListboxItemFormat import *

class AbstractViewer(object):
    def __init__(self):
        self._activeSettings = None
        self._reload = None
        self._itemFmt = ListboxItemFormat()
        self._statFmt = StatItemFormat()

    def getListboxItemFormat(self):
        return self._itemFmt

    def getStatItemFormat(self):
        return self._statFmt

    def setReloadCallback(self, reloadCallback):
        self._reload = reloadCallback

    def reload(self):
        if self._reload:
            self._reload()

    def getActiveSettings(self):
        return self._activeSettings

    def setActiveSettings(self, settings):
        self._activeSettings = settings
    
    def main(self):
        raise NotImplementedError

    def onInsertBegin(self):
        raise NotImplementedError

    def onFieldMatch(self, field, logObj, padding):
        raise NotImplementedError

    def onInsertEnd(self, logObj):
        raise NotImplementedError
