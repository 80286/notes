from tkinter import *
from tkinter import ttk

from DisplaySettings import *
from viewers.AbstractViewer import *
from utils.TkTools import *


class TkLogViewerFrame(ttk.Frame):
    defaultFont = ("Consolas", 10)

    patterns = [
        (r"[0-9]+",                     "grey"),
        (r"[/\-A-Za-z_]+[.][A-Za-z]+",  "blue"),
    ]

    def __init__(self, parent, viewer):
        self._viewer = viewer

        ttk.Frame.__init__(self, parent)

        notebook = ttk.Notebook(parent)

        self._buildTagsFrame(notebook)
        self._buildRawLogFrame(notebook)

        notebook.add(self._tagsFrame, text="Tags")
        notebook.add(self._rawLogFrame, text="Raw log")
        notebook.pack(fill=BOTH, expand=True)

        self.pack()

    def _buildTagsFrame(self, parent):
        mainFrame = ttk.Frame(parent)

        statusLabel = Label(mainFrame, text="-", bg="black", fg="white")
        statusLabel.pack(side=TOP, padx=5, pady=5, fill=X)

        (_, outputText) = TkTools.buildOutputFrame(mainFrame)

        outputText.config(state=DISABLED)
        outputText.pack(fill=BOTH, expand=True)
        outputText.setTags(TkCustomText.outputTags)

        self._filterObj = TkFilterWidget(mainFrame, outputText, self._viewer.getListboxItemFormat(),
                                      title="Search",
                                      expand=True,
                                      onClick=self._onOutListboxClick,
                                      onPressKey=self._onPressKey)

        resetButton = Button(mainFrame, text="Reset", command=self._onResetPressed)
        resetButton.pack(fill=X)
        mainFrame.pack(expand=True, fill=BOTH)

        self.outputText = outputText
        self._tagsFrame = mainFrame
        self._tagsFrameStatus = statusLabel

        self._buildFilterFrame(mainFrame)

    def _buildRawLogFrame(self, parent):
        frame = ttk.Frame(parent)

        (_, rawText) = TkTools.buildOutputFrame(frame)

        rawText.config(state=DISABLED)
        rawText.pack(fill=BOTH, expand=True)
        rawText.setTags(TkCustomText.outputTags)

        self.rawText = rawText
        self._rawLogFrame = frame
        self._rawTextFilterObj = TkFilterWidget(frame, rawText, self._viewer.getListboxItemFormat(),
                                             title="Search",
                                             expand=True,
                                             onClick=self._onOutListboxClick,
                                             onPressKey=self._onPressKey)

        frame.pack(expand=True, fill=BOTH)

    def _statListboxItemToIdx(self, listbox):
        selection = listbox.curselection()
        selectionIdx = int(selection[0]) if len(selection) > 0 else 0
        itemTitle = listbox.get(selectionIdx)
        return self._viewer.getStatItemFormat().extract(itemTitle)

    def _reloadOutputText(self):
        self._filterObj.reset()
        TkTools.textClear(self.outputText)
        self._viewer.reload()
        self._resetOutputScrollbar()
        self.outputText.apply(self.patterns)
        self._refreshStatusLabel()

    def _refreshStatusLabel(self, newText=None):
        activeSettings = self._viewer.getActiveSettings()

        if not newText:
            newText = activeSettings.toString()

        self._tagsFrameStatus.config(text=newText)

    def _onResetPressed(self):
        activeSettings = self._viewer.getActiveSettings()
        activeSettings.reset(DisplaySettings.tag)
        self._reloadOutputText()

    def _onTagSelected(self, event):
        activeSettings = self._viewer.getActiveSettings()
        activeSettings.setActiveValue(DisplaySettings.tag, self._statListboxItemToIdx(event.widget))
        self._reloadOutputText()

    def _buildListboxFromDef(self, parent, title, selectCallback):
        (listbox, _, _) = TkTools.buildListbox(parent, title=title, side=LEFT, expand=True)
        listbox.config(width=40)
        listbox.bind('<Double-Button-1>', selectCallback)

        return listbox

    def _buildFilterFrame(self, parent):
        filterFrame = ttk.Frame(parent)

        self.tagListbox = self._buildListboxFromDef(filterFrame, "Tags", self._onTagSelected)
        filterFrame.pack(side=BOTTOM, expand=True, fill=BOTH)

        return filterFrame

    def _skipToListboxItem(self, filterObj, outListboxIdx):
        outListbox = filterObj.outputListbox

        if outListbox.size() == 0:
            return

        itemLabel = outListbox.get(outListboxIdx)
        dstLineIdx = filterObj.listboxItemFormat.extractIdx(itemLabel)

        if dstLineIdx:
            TkTools.textSkipToLine(filterObj.text, dstLineIdx)

    def _onOutListboxClick(self, filterObj, event):
        listbox = event.widget
        selection = listbox.curselection()
        selectionIdx = int(selection[0]) if len(selection) > 0 else 0
        self._skipToListboxItem(filterObj, selectionIdx)

    def _onPressKey(self, event, filterObj):
        doBreak = filterObj.genericOnPressKey(event)

        if doBreak:
            self._skipToListboxItem(filterObj, 0)

        return TkFilterWidget.getOnPressKeyStrResponse(doBreak)

    def _resetOutputScrollbar(self):
        TkTools.textResetScrollbar(self.outputText)

    def _updateStateOfTextWidgets(self, state):
        self.outputText.config(state=state)
        self.rawText.config(state=state)

    def onExit(self, root):
        root.destroy()


class TkLogViewer(AbstractViewer):
    def __init__(self, argv):
        super().__init__()

        self._root = Tk()
        self._root.geometry("1024x768")
        self._rootFrame = TkLogViewerFrame(self._root, self)
        self._root.protocol("WM_DELETE_WINDOW", lambda: self._root.destroy())
        self._root.option_add('*TCombobox*Listbox.font', TkLogViewerFrame.defaultFont)

    def main(self):
        self._root.mainloop()

    def onInsertBegin(self):
        frame = self._rootFrame
        frame._updateStateOfTextWidgets(NORMAL)
        frame._resetOutputScrollbar()

    def onFieldMatch(self, field, shortFormStr):
        frame = self._rootFrame
        frame.outputText.insert(END, shortFormStr)
        frame.rawText.insert(END, field["line"])

    def onInsertEnd(self, logObj):
        frame = self._rootFrame

        frame._updateStateOfTextWidgets(DISABLED)
        TkTools.fillListbox(frame.tagListbox, logObj.getSortedStatItems("tag", self.getStatItemFormat()))
