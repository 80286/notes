import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from viewers.AbstractViewer import *
from DisplaySettings import *
from utils.GtkTools import *


class GtkLogViewerWidgets(object):
    def __init__(self, handler):
        rootId = "root"

        getPath = lambda filename: f"layouts/{filename}"
        loadPrefab = lambda path, handler: UiLayoutLoader(getPath(f"{path}.glade"), handler, rootId)

        UiStyleLoader.loadStyle(getPath("style.css"))
        main = UiLayoutLoader(getPath("main.glade"), handler)

        filterPrefab = loadPrefab("filter", None)
        valuePrefab = loadPrefab("values", None)
        rawFilterPrefab = loadPrefab("filter", None)

        mainFilterBox = main.get("filter_box")
        mainFilterBox.pack_start(filterPrefab.get(rootId), True, True, 0)

        mainRawLogBox = main.get("raw_log_filter_box")
        mainRawLogBox.pack_start(rawFilterPrefab.get(rootId), True, True, 0)

        mainValuesBox = main.get("values_box")
        mainValuesBox.pack_start(valuePrefab.get(rootId), True, True, 0)

        valuesLabel = UiLabel(valuePrefab.get("title"))
        valuesLabel.set("Tags")

        self.valuesListbox = UiListbox(valuePrefab.get("values_listbox"))
        self.valuesListbox.connect("row-activated", handler.onValueActivated)

        self.rawOutputText = UiTextView(main.get("rawText"))
        self.outputText = UiTextView(main.get("outputText"))

        self.rawOutputFilter = GtkFilterWidget(rawFilterPrefab, self.rawOutputText, handler.getListboxItemFormat())
        self.outputFilter = GtkFilterWidget(filterPrefab, self.outputText, handler.getListboxItemFormat())

        main.get(rootId).show_all()


class GtkLogViewer(AbstractViewer):
    def __init__(self, argv):
        super().__init__()
        self._widgets = GtkLogViewerWidgets(self)

    def main(self):
        Gtk.main()

    def onInsertBegin(self):
        self._widgets.outputText.scrollToStart()

    def onFieldMatch(self, field, shortFormStr):
        widgets = self._widgets
        widgets.outputText.append(shortFormStr)
        widgets.rawOutputText.append(field["line"])

    def onInsertEnd(self, logObj):
        self._widgets.valuesListbox.fill(logObj.getSortedStatItems("tag", self.getStatItemFormat()))

    def onResetButtonPress(self, *args):
        activeSettings = self.getActiveSettings()
        activeSettings.reset(DisplaySettings.tag)
        self._widgets.outputText.clearHighlights()
        self._reloadOutputText()

    def _reloadOutputText(self):
        outputText = self._widgets.outputText
        outputText.clear()
        self.reload()
        outputText.scrollToStart()

    def onValueActivated(self, listbox, row):
        rowStr = row.get_children()[0].get_label().strip()
        tagValue = self.getStatItemFormat().extract(rowStr)

        activeSettings = self.getActiveSettings()
        activeSettings.setActiveValue(DisplaySettings.tag, tagValue)

        self._reloadOutputText()

    def onDestroy(self, *args):
        Gtk.main_quit()
