class DisplaySettings(object):
    tag = "tag"

    def __init__(self):
        self._activeValues = {}

    def reset(self, key):
        if key in self._activeValues.keys():
            self._activeValues.pop(key)

    def setActiveValue(self, key, value):
        self._activeValues[key] = value

    def _isValueAllowed(self, key, value):
        activeValue = self._activeValues.get(key)

        if activeValue is None:
            return True
        else:
            return activeValue == value

    def isFieldAllowed(self, field):
        return all([self._isValueAllowed(key, value) for key, value in field.items()])

    def toString(self):
        return ", ".join(["{0}: {1}".format(k, v) for k, v in self._activeValues.items() if v])
