class Tools {
    // https://stackoverflow.com/a/7220510
    syntaxHighlight(json) {
        if(typeof json != 'string') {
            json = JSON.stringify(json, undefined, 4);
        }

        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';

            if(/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if(/true|false/.test(match)) {
                cls = 'boolean';
            } else if(/null/.test(match)) {
                cls = 'null';
            }

            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

    getOutputPanel() {
        return document.getElementById("output-panel");
    }

    getQuickSearchInput() {
        return document.getElementById("left-panel-quick-search");
    }

    getOutput() {
        return document.getElementById("output");
    }

    setOutputText(text) {
        this.getOutput().innerHTML = text;
    }

    buildEventHeader(obj) {
        if(!obj)
        {
            return;
        }

        var num = obj.num ?? "";
        var st1 = obj.st1 ?? "";
        var st2 = obj.st2 ?? "";
        var st3 = obj.st3 ?? "";
        var v = obj.v ?? "";

        return `${num}: ${st1}/${st2}/${st3}/${name}/${v}`;
    }

    loadTestEvents(app) {
        app.events = Array(80).fill().map((_, idx) => ({label: `${idx}: st1/st2/st3/name/v`, json: `Test-${idx}`, idx: idx}));
    }

    showFileDialog(callback) {
        var input = document.createElement('input');

        input.type = 'file';
        input.onchange = callback
        input.click();
    }

    doQuickSearch(events, pattern) {
        var matchedEvents = Array();

        for(var i = 0; i < events.length; i++) {
            var eventObj = events[i];

            for(const [k, v] of Object.entries(eventObj.json)) {
                if((k && String(k).includes(pattern)) || (v && String(v).includes(pattern))) {
                    matchedEvents.push({obj: eventObj.json, k: k, v: v});
                }
            }
        }

        console.log(`Found ${matchedEvents.length} results.`);
        return matchedEvents;
    }

    showOutput() {
        app.results = Array();
        this.getOutput().style.visiblity = "visible";
    }

    showResults() {
        this.setOutputText("");
        this.getOutput().style.visiblity = "hidden";
    }
}

var tools = new Tools();
var app = new Vue({ 
    el: '#app',
    data: {
        events: Array(),
        results: Array()
    },
    methods: {
        onShowEvent: function(eventIdx) {
            tools.showOutput();
            tools.setOutputText(tools.syntaxHighlight(app.events[eventIdx].json));
        },

        onQuickSearch: function() {
            var pattern = tools.getQuickSearchInput().value;
            var matchedEvents = tools.doQuickSearch(app.events, pattern);

            tools.showResults();
            app.results = Array(matchedEvents.length).fill().map((_, idx) => ({
                obj: matchedEvents[idx].obj,
                label: (tools.buildEventHeader(matchedEvents[idx].obj) + ` : ${matchedEvents[idx].k}=${matchedEvents[idx].v}`)
            }));
        },

        onQuickSearchReset: function() {
            tools.getQuickSearchInput().value = "";
            tools.showOutput();
        },

        onClearEvents: function() {
            app.events = Array();
            tools.setOutputText("");
        },
        
        onSaveSession: function() {
            // tools.showFileDialog(e => {
            //     var file = e.target.files[0];

            //     if(file != "")
            //     {
            //         var jsonArray = Array(app.events.length).map((obj, idx) => obj.json);
            //         var jsonStr = JSON.stringify(jsonArray, null, 4);
            //     }
            // });
        },

        onLoadTestSession: function() {
            tools.loadTestEvents(app);
        },

        onLoadSession: function() {
            tools.showFileDialog(e => {
                var file = e.target.files[0];

                if(file != "")
                {
                    var reader = new FileReader();

                    reader.readAsText(file, 'UTF-8');
                    reader.onload = readerEvent => {
                        app.events = Array();

                        var content = readerEvent.target.result;
                        var jsonContent = JSON.parse(content);

                        for(var i = 0; i < jsonContent.length; i++) {
                            const obj = jsonContent[i];

                            app.events.push({
                                label: tools.buildEventHeader(obj),
                                json: obj,
                                idx: i
                            });
                        }

                        tools.setOutputText(tools.syntaxHighlight(jsonContent[0]));
                    }
                }
            });
        }
    }
});

tools.loadTestEvents(app);
