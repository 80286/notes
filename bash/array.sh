#!/bin/bash

function testPrintArrayWhile() {
    # -n: makes ref to the array
    local -n arrayToPrint="$1"
    local testArrayLen="${#arrayToPrint[@]}"
    local -i i=0

    echo "Example (while):"
    while [ $i -lt ${testArrayLen} ] ;do
        echo "${i}: ${arrayToPrint[$i]}"
        i=$((i+1))
    done
}

function testPrintArrayFor() {
    local -n arrayToPrint="$1"
    local -i i

    echo "Example (for i=0; i < 3; ...):"
    for (( i=0 ; i < ${testArrayLen} ; i=$((i+1)) )) ; do
    	echo "${i}: ${testArray[$i]}"
    done
}

function testPrintArray() {
    declare -la testArray=(
        "A0"
        "A1"
        "A2"
    )
    local testArrayLen="${#testArray[@]}"
    echo "Length of test array: ${testArrayLen}"

    testPrintArrayWhile testArray
    testPrintArrayFor testArray
}

testPrintArray
