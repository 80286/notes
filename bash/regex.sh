#!/bin/bash

function doTestMatch() {
    local pattern="^[0-9]+$"
    local stdinStr

    read stdinStr

    if [[ ${stdinStr} =~ ${pattern} ]]; then
        echo "${stdinStr} matches to the ${pattern}"
    else
        echo "${stdinStr} doesn't match to the '${pattern}'"
    fi
}

doTestMatch
