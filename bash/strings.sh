#!/bin/bash

function testStr() {
    local s="file.tar.gz"

    echo "Full str:        	                : $s"
    echo

    echo "str without first character: : ${s:1}"
    echo "First character of str: ${s:0:1}"

    echo "str without dot suffix v1	: ${s%.*}"
    echo "str without dot suffix v2	: ${s%%.*}"

    echo "str without dot prefix v1	: ${s#*.}"
    echo "str without dot prefix v2	: ${s##*.}"

    echo "Pattern replacement		: ${s/.*/.tgz}"

    echo "Converting to uppercase	: ${s^^}"
    echo "Converting to lowercase	: ${s,,}"

    echo
}

testStr
