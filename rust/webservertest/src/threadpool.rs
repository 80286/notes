use std::thread;
use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;

type Job = Box<dyn FnOnce() + Send + 'static>;

enum WorkerMessage {
    NewJob(Job),
    Terminate,
}

type ReceiverType = Arc<Mutex<mpsc::Receiver<WorkerMessage>>>;

struct Worker {
    id: u32,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    pub fn new(id: u32, receiver: ReceiverType) -> Worker {
        let thread = thread::spawn(move || {
            while let Ok(msg) = receiver.lock().unwrap().recv() {

                match msg {
                    WorkerMessage::NewJob(job) => {
                        println!("Worker {} got a job. Executing...", id);
                        job()
                    },
                    WorkerMessage::Terminate => {
                        println!("Worker {} received Terminate request.", id);
                        break
                    },
                }
            } // lock will be held until here
        });

        Worker {
            id: id,
            thread: Some(thread),
        }
    }
}

pub struct ThreadPool {
    sender: mpsc::Sender<WorkerMessage>,
    workers: Vec<Worker>,
}

impl ThreadPool {
    pub fn new(n: usize) -> ThreadPool {
        assert!(n > 0);

        let mut workers = Vec::with_capacity(n);

        let (sender, receiver) = mpsc::channel();

        // create synchronized receiver as "smart ptr" obj:
        let receiver = Arc::new(Mutex::new(receiver));

        for worker_id in 0..n {
            // do a safe copy of receiver to share it for all workers:
            workers.push(Worker::new(worker_id as u32, Arc::clone(&receiver)));
        }

        ThreadPool {
            sender: sender,
            workers: workers,
        }
    }

    pub fn execute<F>(&self, f: F)
        where F: FnOnce() + Send + 'static {

        let job = WorkerMessage::NewJob(Box::new(f));

        self.sender.send(job).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        let prefix = "ThreadPool.drop";

        println!("{}: sending Terminate msg to all workers...", prefix);

        for _ in &self.workers {
            self.sender.send(WorkerMessage::Terminate).unwrap();
        }

        for worker in &mut self.workers {
            println!("{}: shutting down worker {}...", prefix, worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}
