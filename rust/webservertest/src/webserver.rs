pub mod webserver {
    use std::net::TcpListener;
    use std::net::TcpStream;
    use std::fs;
    use std::thread;
    use std::time::Duration;

    use crate::threadpool::ThreadPool;

    // We bring std::io::prelude into scope to get access to certain traits that let us read from and write to the stream:
    use std::io::prelude::*;

    pub struct WebServer {
        addr: String,
    }

    impl WebServer {
        pub fn new(addr: String) -> WebServer {
            WebServer {
                addr: addr,
            }
        }

        fn build_response(response_title: &String) -> String {
            let sep = "\r\n";
            let mut content = fs::read_to_string("templates/response.html").unwrap();

            content = content.replace("{title}", response_title);
            content = content.replace("{h1_title}", response_title);
            content = content.replace("{par}", "test_paragraph");

            format!("HTTP/1.1 200 OK{sep}Content-Length: {len}{sep}{sep}{content}", 
                    sep = sep,
                    len = content.len(),
                    content = content)
        }

        fn handle_connection(mut stream: TcpStream) {
            let prefix = "WebServer.handle_connection";
            let stream_str = format!("{:?}", stream);
            let mut buffer = [0; 1024];
            
            println!("{}: {} has connected.", prefix, stream_str);

            stream.read(&mut buffer).unwrap();
            println!("{}: received:\n\n{}", prefix, String::from_utf8_lossy(&buffer[..]));

            let response = if buffer.starts_with(b"GET /") {
                if buffer.starts_with(b"GET /sleep") {
                    println!("{}: calling sleep for {} request...", prefix, stream_str);
                    thread::sleep(Duration::from_secs(5));
                }

                let response_title = format!("Serving content for: {}", stream_str);
                let response = WebServer::build_response(&response_title);

                println!("{}: sending response:\n\n{}", prefix, response);
                response
            } else {
                WebServer::build_response(&String::from("Unknown request: resource not found"))
            };


            stream.write(response.as_bytes()).unwrap();
            stream.flush().unwrap();

            println!("{}: done.", prefix);
        }

        pub fn listen(&self) {
            let prefix = "WebServer.listen";
            let listener = TcpListener::bind(self.addr.clone()).unwrap();

            let pool = ThreadPool::new(4);

            println!("{}: {}: waiting for connections...", prefix, self.addr);
            for stream in listener.incoming() {
                let stream = stream.unwrap();

                pool.execute(|| {
                    WebServer::handle_connection(stream);
                });
            }
        }
    }
}
