mod webserver;
mod threadpool;
pub use webserver::webserver::WebServer;

fn main() {
    let web_server = WebServer::new(String::from("127.0.0.1:9000"));

    web_server.listen();
}
