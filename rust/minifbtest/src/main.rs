extern crate minifb;

mod drawer;
mod app;
use crate::app::App;

fn main() {
    let mut app = App::new(1024, 768);

    app.init();
    app.launch();
}
