use minifb::Window;

pub struct Drawer {
    buffer: Vec<u32>,
    window_width: usize,
    window_height: usize,
}

impl Drawer {
    pub fn new(w: usize, h: usize) -> Drawer {
        Drawer {
            window_width: w,
            window_height: h,
            buffer: vec![0; w * h],
        }
    }

    pub fn get_width(&self) -> usize {
        self.window_width as usize
    }

    pub fn get_height(&self) -> usize {
        self.window_height as usize
    }

    fn get_pixel_idx(&self, x: i32, y: i32) -> usize {
        (y as usize) * self.window_width + (x as usize)
    }

    pub fn clear(&mut self) {
        for i in self.buffer.iter_mut() {
            *i = 0;
        }
    }

    pub fn draw(&mut self, x: i32, y: i32, color: u32) {
        if x < 0 || x >= (self.window_width as i32) || y < 0 || (y >= self.window_height as i32) {
            return;
        }

        let idx = self.get_pixel_idx(x, y);

        self.buffer[idx] = color;
    }

    pub fn draw_line(&mut self, ix1: i32, iy1: i32, ix2: i32, iy2: i32, color: u32) {
        let (x1, x2) = (ix1 as f32, ix2 as f32);
        let (y1, y2) = (iy1 as f32, iy2 as f32);

        let a = (y2 - y1) / (x2 - x1);
        let b = y2 - (a * x2);

        if a.abs() >= 1.0 {
            let (m1, m2) = if y1 < y2 { (iy1, iy2) } else { (iy2, iy1) };

            for y in m1..(m1 + (m2 - m1)) {
                let x = if (ix2 - ix1) == 0 { ix1 } else { ((y as f32 - b) / a) as i32 };

                self.draw(x, y, color);
            }
        } else {
            let (m1, m2) = if x1 < x2 { (ix1, ix2) } else { (ix2, ix1) };

            for x in m1..(m1 + (m2 - m1)) {
                let y = (a * (x as f32) + b) as i32;

                self.draw(x, y, color);
            }
        }
    }

    pub fn update_window(&self, window: &mut Window) {
        window.update_with_buffer(&self.buffer, self.window_width, self.window_height).unwrap();
    }
}
