use crate::drawer::Drawer;

use minifb::{Key, Window, WindowOptions};
use std::cell::RefCell;
use std::rc::Rc;


struct SharedPtr;
type SharedPtrType<T> = Rc<RefCell<T>>;
impl SharedPtr {
    fn new<T>(v: T) -> SharedPtrType<T> {
        Rc::new(RefCell::new(v))
    }
}

pub struct App {
    window: Option<SharedPtrType<Window>>,
    drawer: Drawer,
    ctx: DrawContext,
}

struct DrawContext {
    ang: f32,
    color: u32
}

fn u8rgb2u32(r: u8, g: u8, b: u8) -> u32 {
    let r32 = r as u32;
    let g32 = (g as u32) << (8 as u32);
    let b32 = (b as u32) << (16 as u32);

    r32 | g32 | b32 | 0xff
}

impl App {
    fn build_window(w: usize, h: usize) -> Window {
        let mut window = Window::new("Test", w, h, WindowOptions::default())
            .unwrap_or_else(|e| {
                panic!("{}", e);
        });

        window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));
        window
    }

    fn draw(&mut self) {
        let r: f32 = 250.0;
        let mx = (self.drawer.get_width() / 2) as i32;
        let my = (self.drawer.get_height() / 2) as i32;
        let dx = (mx as f32 + r * self.ctx.ang.cos()) as i32;
        let dy = (my as f32 + r * self.ctx.ang.sin()) as i32;
        let max_ang = 6.28;

        self.drawer.draw_line(mx, my, dx, dy, self.ctx.color);
        self.drawer.draw(dx, dy, self.ctx.color);

        if self.ctx.ang > max_ang {
            self.drawer.clear();
            self.ctx.ang = 0.0;
            self.ctx.color = 0xffffffff;
        } else {
            let new_color = ((self.ctx.ang / max_ang) * 255.0) as u8;

            self.ctx.ang += 0.01;
            self.ctx.color = u8rgb2u32(new_color, 255 - new_color, new_color);
        }
    }

    pub fn launch(&mut self) {
        let window_opt = self.window.as_ref().unwrap().clone();
        let mut window = window_opt.borrow_mut();

        while window.is_open() && !window.is_key_down(Key::Escape) {
            self.draw();
            self.drawer.update_window(&mut window);
        }
    }

    pub fn init(&mut self) {
        let drawer = &mut self.drawer;
        self.window = Some(SharedPtr::new(App::build_window(drawer.get_width(), drawer.get_height())));
    }

    pub fn new(w: usize, h: usize) -> App {
        App {
            drawer: Drawer::new(w, h),
            window: None,
            ctx: DrawContext{ ang: 0.0, color: 0xffffffff },
        }
    }
}
