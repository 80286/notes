Refs:

- [rustup](https://rustup.rs)
- [opengl-in-rust-from-scratch](http://nercury.github.io/rust/opengl/tutorial/2018/02/08/opengl-in-rust-from-scratch-00-setup.html)

Crates:

- [sdl2 crate](https://crates.io/crates/sdl2)
- [gl crate](https://crates.io/crates/gl)
