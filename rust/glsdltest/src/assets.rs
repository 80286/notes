use std::fs;
use std::io::{self, Read};
use std::ffi;
use std::path::{Path, PathBuf};

use crate::render::program::Program;
use crate::render::shader::Shader;

#[derive(Debug)]
pub enum AssetsError {
    UnableToGetExePath,
    UnableToGetExeParentPath,
    Io(io::Error),
    FileContainsNil,
}

impl From<io::Error> for AssetsError {
    fn from(other: io::Error) -> Self {
        AssetsError::Io(other)
    }
}

#[derive(Debug)]
pub struct Assets {
    root: PathBuf,
}

impl Drop for Assets {
    fn drop(&mut self) {
        println!("Assets.drop();");
    }
}

impl Assets {
    pub fn new(relative_path: &Path) -> Result<Assets, AssetsError> {
        let exec_full_path = std::env::current_exe().map_err(|_| AssetsError::UnableToGetExePath)?;
        let exec_dir_path = exec_full_path.parent().ok_or(AssetsError::UnableToGetExeParentPath)?;

        Ok(Assets {
            root: exec_dir_path.join(relative_path)
        })
    }

    pub fn load_cstring(&self, asset_name: &String) -> Result<ffi::CString, AssetsError> {
        // If error will occur in below call then AssetsError will make use of From impl for
        // AssetsError:
        let mut file = fs::File::open(self.root.join("../..").join(asset_name))?;
        let mut buffer: Vec<u8> = Vec::with_capacity(file.metadata()?.len() as usize + 1);

        file.read_to_end(&mut buffer)?;

        if buffer.iter().find(|i| **i == 0).is_some() {
            return Err(AssetsError::FileContainsNil);
        }

        Ok(unsafe { ffi::CString::from_vec_unchecked(buffer) })
    }

    fn load_shaders(&self, name: &str) -> Result<Vec<Shader>, String> {
        let mut shaders: Vec<Shader> = Vec::new();

        for &(ext, shader_type) in crate::render::shader::SHADER_EXT_TO_ENUM.iter() {
            let src_name = format!("{}.{}", name, ext);
            let src = self.load_cstring(&src_name).map_err(|e| format!("Error while loading '{}': {:?}", src_name, e))?;

            shaders.push(Shader::from_source_cstr(&src, shader_type).unwrap());
        }

        Ok(shaders)
    }

    pub fn load_program(&self, name: &str) -> Result<Program, String> {
        let shaders = self.load_shaders(name)?;

        Program::from_shaders(&shaders)
    }
}
