pub mod app {
    use crate::render::program::Program;
    use crate::render::vertex::*;
    use crate::render::batcher::{AbstractVertexBatcher, GlVertexBatcher};
    use crate::assets::Assets;
    use std::path::Path;

    pub struct App {
        assets: Assets,
        sdl_context: Option<sdl2::Sdl>,
        window: Option<sdl2::video::Window>,
        video_subsystem: Option<sdl2::VideoSubsystem>,
        program: Option<Program>,
        _gl_context: Option<sdl2::video::GLContext>,
        window_width: u32,
        window_height: u32,
        batcher: GlVertexBatcher,
    }

    impl Drop for App {
        fn drop(&mut self) {
            println!("App.drop();");
        }
    }

    impl App {
        fn load_shaders(&self) -> Program {
            let program = self.assets.load_program("assets/shaders/triangle");

            program.unwrap()
        }

        fn init_batcher(&mut self) {
            let mut vertices: Vec<Vertex> = vec![
                Vertex::new((-0.5, -0.5, 0.0).into(), (1.0, 0.0, 0.0).into()),
                Vertex::new(( 0.5, -0.5, 0.0).into(), (0.0, 1.0, 0.0).into()),
                Vertex::new(( 0.0,  0.5, 0.0).into(), (0.0, 0.0, 1.0).into()),
            ];

            self.batcher.setup_viewport(self.window_width, self.window_height);
            self.batcher.push_vec(&mut vertices);
            self.batcher.init();
        }

        fn init(&mut self) {
            let sdl_context = sdl2::init().unwrap();
            let video_subsystem = sdl_context.video().unwrap();

            let gl_attr = video_subsystem.gl_attr();
            gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
            gl_attr.set_context_version(4, 5);

            let window = video_subsystem
                .window("gl-sdl-test", self.window_width, self.window_height)
                .opengl()
                .resizable()
                .build()
                .unwrap();

            gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);

            self._gl_context = Some(window.gl_create_context().unwrap());
            self.sdl_context = Some(sdl_context);
            self.video_subsystem = Some(video_subsystem);
            self.window = Some(window);
        }

        pub fn build(w: u32, h: u32) -> Result<App, String> {
            let mut app = App {
                assets: Assets::new(Path::new("")).unwrap(),
                window: None,
                sdl_context: None,
                video_subsystem: None,
                _gl_context: None,
                program: None,
                window_width: w,
                window_height: h,
                batcher: GlVertexBatcher::new(4),
            };

            app.init();
            app.program = Some(app.load_shaders());
            app.init_batcher();

            Ok(app)
        }

        fn draw(&self) {
            let program = self.program.as_ref().unwrap();
            program.set_used();

            self.batcher.draw();
        }

        pub fn start_loop(&self) {
            let sdl_context = self.sdl_context.as_ref().unwrap();
            let window = self.window.as_ref().unwrap();

            let mut event_pump = sdl_context.event_pump().unwrap();

            'main: loop {
                for event in event_pump.poll_iter() {
                    match event {
                        sdl2::event::Event::Quit {..} => break 'main,
                        _ => {},
                    }
                }

                self.draw();
                window.gl_swap_window();
            }
        }
    }
}
