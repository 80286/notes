pub mod program;
pub mod shader;
pub mod vertex;
pub mod batcher;

use std;
use std::ffi::CString;

fn build_whitespace_cstring_with_len(dst_string_len: usize) -> CString {
    // Build empty vector of unsigned bytes (uint8_t):
    let mut buffer: Vec<u8> = Vec::with_capacity(dst_string_len as usize + 1);

    // Insert space dst_string_len times into a vector:
    buffer.extend([b' '].iter().cycle().take(dst_string_len as usize));

    // Return as CString:
    unsafe { CString::from_vec_unchecked(buffer) }
}
