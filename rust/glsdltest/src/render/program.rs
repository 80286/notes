use crate::render::shader::Shader;
use crate::render::*;

pub struct Program {
    id: gl::types::GLuint,
}

impl Program {
    pub fn id(&self) -> gl::types::GLuint {
        self.id
    }

    pub fn from_shaders(shaders: &Vec<Shader>) -> Result<Program, String> {
        let program_id = unsafe {
            gl::CreateProgram()
        };

        for shader in shaders {
            unsafe {
                gl::AttachShader(program_id, shader.id());
            }
        }

        unsafe {
            gl::LinkProgram(program_id);
        }

        let mut link_status: gl::types::GLint = 1;
        unsafe {
            gl::GetProgramiv(program_id, gl::LINK_STATUS, &mut link_status);
        }

        if link_status == 0 {
            let mut info_log_length : gl::types::GLint = 0;
            unsafe {
                gl::GetProgramiv(program_id, gl::INFO_LOG_LENGTH, &mut info_log_length);
            }

            let error = build_whitespace_cstring_with_len(info_log_length as usize);
            unsafe {
                gl::GetProgramInfoLog(program_id, info_log_length, std::ptr::null_mut(), error.as_ptr() as *mut gl::types::GLchar);
            }

            return Err(error.to_string_lossy().into_owned());
        }

        for shader in shaders {
            unsafe {
                gl::DetachShader(program_id, shader.id());
            }
        }

        Ok(Program {id: program_id})
    }

    pub fn set_used(&self) {
        unsafe {
            gl::UseProgram(self.id);
        }
    }
}

impl Drop for Program {
    fn drop(&mut self) {
        println!("Program.drop();");

        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}
