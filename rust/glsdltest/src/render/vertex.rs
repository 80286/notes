#[repr(C, packed)]
pub struct f32x3 {
    x: f32,
    y: f32,
    z: f32,
}

impl From<(f32, f32, f32)> for f32x3 {
    fn from(other: (f32, f32, f32)) -> Self {
        f32x3 {
            x: other.0,
            y: other.1,
            z: other.2,
        }
    }
}

#[repr(C, packed)]
pub struct Vertex {
    pos: f32x3,
    color: f32x3,
}

impl Vertex {
    pub fn new(a: f32x3, b: f32x3) -> Self {
        Self {
            pos: a,
            color: b
        }
    }
}
