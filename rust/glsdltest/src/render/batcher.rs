use crate::render::vertex::*;

pub trait AbstractVertexBatcher {
    fn draw(&self);
    fn setup_viewport(&self, w: u32, h: u32);
}

const NUM_OF_COMPONENTS_PER_ATTR: usize = 3;

pub struct GlVertexBatcher {
    vertices: Vec<Vertex>,
    vbo: gl::types::GLuint,
    vao: gl::types::GLuint,
    size: usize,
}

impl GlVertexBatcher {
    pub fn new(capacity: usize) -> Self {
        Self {
            vertices: Vec::with_capacity(capacity),
            size: capacity,
            vbo: 0,
            vao: 0,
        }
    }

    pub fn push(&mut self, v: Vertex) {
        self.vertices.push(v);
        self.size += 1;
    }

    pub fn push_vec(&mut self, v: &mut Vec<Vertex>) {
        self.vertices.append(v);
        self.size += v.len();
    }

    pub fn init(&mut self) {
        let (vbo, vao) = GlVertexBatcher::setup_vertex_buffers(&mut self.vertices);

        self.vbo = vbo;
        self.vao = vao;
    }

    unsafe fn setup_vertex_attr(idx: u32, stride: usize, offset: usize) {
        gl::EnableVertexAttribArray(idx as gl::types::GLuint); 
        gl::VertexAttribPointer(
            idx as gl::types::GLuint, // index of vertex attr (location=idx)
            NUM_OF_COMPONENTS_PER_ATTR as gl::types::GLint, // num of components per vertex attr
            gl::FLOAT, // type of data
            gl::FALSE, // normalized
            stride as gl::types::GLint,
            offset as *const gl::types::GLvoid);
    }

    pub fn setup_vertex_buffers(vertices: &Vec<Vertex>) -> (u32, u32) {
        let mut vbo: gl::types::GLuint = 0;

        unsafe {
            gl::GenBuffers(1, &mut vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::BufferData(gl::ARRAY_BUFFER,
                           (vertices.len() * std::mem::size_of::<Vertex>()) as gl::types::GLsizeiptr,
                           vertices.as_ptr() as *const gl::types::GLvoid,
                           gl::STATIC_DRAW);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        }

        let mut vao: gl::types::GLuint = 0;

        unsafe {
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);

            {
                let stride = std::mem::size_of::<Vertex>();
                let num_of_attributes = 2;
                let mut offset = 0;

                for layout_idx in 0..num_of_attributes {
                    GlVertexBatcher::setup_vertex_attr(layout_idx, stride, offset);

                    offset += std::mem::size_of::<f32x3>();
                }
            }

            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }

        (vbo, vao)
    }
}

impl AbstractVertexBatcher for GlVertexBatcher {
    fn draw(&self) {
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
            gl::BindVertexArray(self.vao);
            gl::DrawArrays(gl::TRIANGLES, 0, self.vertices.len() as gl::types::GLsizei);
        }
    }

    fn setup_viewport(&self, w: u32, h: u32) {
        unsafe {
            gl::Viewport(0, 0, w as i32, h as i32);
            gl::ClearColor(0.8, 0.8, 0.8, 1.0);
        }
    }
}
