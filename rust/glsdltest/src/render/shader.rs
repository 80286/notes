use gl;
use std;
use std::ffi::CStr;
use crate::render::*;

pub const SHADER_EXT_TO_ENUM: [(&str, gl::types::GLenum); 2] = [
    ("frag", gl::FRAGMENT_SHADER),
    ("vert", gl::VERTEX_SHADER),
];

pub struct Shader {
    id: gl::types::GLuint,
}

impl Shader {
    pub fn id(&self) -> gl::types::GLuint {
        self.id
    }

    pub fn from_source_cstr(
        source: &CStr,
        kind: gl::types::GLenum) -> Result<Shader, String> {
        let id = unsafe { gl::CreateShader(kind) };

        unsafe {
            gl::ShaderSource(id, 1, &source.as_ptr(), std::ptr::null());
            gl::CompileShader(id);
        }

        let mut compile_status: gl::types::GLint = 1;
        unsafe {
            gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut compile_status);
        }

        if compile_status == 0 {
            let mut info_log_length: gl::types::GLint = 0;
            unsafe {
                gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut info_log_length);
            }

            let error = build_whitespace_cstring_with_len(info_log_length as usize);

            unsafe {
                gl::GetShaderInfoLog(id, info_log_length, std::ptr::null_mut(), error.as_ptr() as *mut gl::types::GLchar);
            }

            return Err(error.to_string_lossy().into_owned());
        }

        Ok(Shader{ id: id })
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.id);
        }
    }
}
