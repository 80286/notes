extern crate sdl2;
extern crate gl;

pub mod app;
pub mod render;
pub mod assets;
pub use crate::app::app::App;

fn main() {
    println!("main();");

    let app = App::build(1024, 768).unwrap();

    app.start_loop();
}
