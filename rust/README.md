## Various Rust examples

- preparing rust environment: `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh` - check [rustup](https://rustup.rs/)
- initializaing new project: `cargo new <dst_dir_name>`
- initializing new project inside `PWD`: `cargo init`
- building project: `cargo build`
- building & running project: `cargo run`
- running tests: `cargo test`

### Description of projects

- `glsdltest` - example of SDL + OpenGL rust app, created while reading [this](http://nercury.github.io/rust/opengl/tutorial/2018/02/08/opengl-in-rust-from-scratch-00-setup.html)
- `websservertest` - simple HTTP server written in Rust created while reading chapter from [Rust Programming Language](https://doc.rust-lang.org/book/ch20-00-final-project-a-web-server.html)
- `notes` - bunch of different examples created while reading [Rust Programming Language](https://doc.rust-lang.org/book/title-page.html) and watching [Rust ownership and Borrowing](https://www.youtube.com/watch?v=lQ7XF-6HYGc)
