use winit::event::KeyEvent;

pub struct Input {
}

impl Input {
    pub fn handle_event(&self, event: KeyEvent) {
        match event.physical_key {
            winit::keyboard::PhysicalKey::Code(key) => {
                println!("Input.handle_event: key={}", key as u32)
            },
            _ => ()
        }
    }
}
