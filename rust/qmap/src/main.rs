extern crate winit;

use winit::application::ApplicationHandler;
use winit::error::EventLoopError;
use winit::event::WindowEvent;
use winit::window::{Window, WindowId};
use winit::event_loop::{EventLoop, ActiveEventLoop};

use crate::input::Input;
mod input;

pub struct TestApp {
    window: Option<Window>,
    input:  Input
}

impl ApplicationHandler for TestApp {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        let window_attrs = Window::default_attributes()
            .with_title("Test")
            .with_inner_size(winit::dpi::LogicalSize::new(1280, 1024));

        self.window = Some(event_loop.create_window(window_attrs).unwrap());
    }

    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        window_id: WindowId,
        event: WindowEvent,
    ) {
        let _ = window_id;
        let window = self.window.as_ref().unwrap();

        match event {
            WindowEvent::CloseRequested => event_loop.exit(),
            WindowEvent::KeyboardInput { device_id: _, event, is_synthetic: _ } => {
                self.input.handle_event(event)
            }
            WindowEvent::RedrawRequested => {
                window.pre_present_notify();
            }
            _ => ()
        }
    }
}

impl TestApp {
    pub fn new() -> TestApp {
        TestApp {
            window: None,
            input: Input {}
        }
    }

    fn run(&mut self) -> Result<(), EventLoopError> {
        let event_loop = EventLoop::new()?;

        println!("Starting...");
        event_loop.run_app(self)
    }
}

fn main() {
    let mut app = TestApp::new();
    let _ = app.run();
}
