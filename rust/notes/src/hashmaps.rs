use std::collections::HashMap;

pub fn run() {
    let prefix = "hashmaps";

    let mut letters = HashMap::new();

    letters.insert(String::from("a"), 1);
    letters.insert(String::from("b"), 2);

    // Only Inserting a Value If the Key Has No Value
    // (https://doc.rust-lang.org/book/ch08-03-hash-maps.html#only-inserting-a-value-if-the-key-has-no-value):
    letters.entry(String::from("c")).or_insert(3);

    println!("{}: letters: {:?}", prefix, letters);

    for (k, v) in &letters {
        println!("{}; letters[{}] = '{}'", prefix, k, v);
    }

    let b = letters.get("b").unwrap();
    assert_eq!(*b, 2);
}
