#[derive(Debug)]
struct TestStruct<T> {
    a: T,
    b: T,
}

impl<T> TestStruct<T> {
    fn get_a(&self) -> &T {
        &self.a
    }

    fn get_b(&self) -> &T {
        &self.b
    }
}

pub fn run() {
    let prefix = "generics";
    let a = TestStruct{
        a: 1 as i32,
        b: 2
    };

    println!("{}: a: {:?}; a.get_a() = {}; a.get_b() = {}", prefix, a, a.get_a(), a.get_b());
}
