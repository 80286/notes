// Rust Lifetimes: https://www.youtube.com/watch?v=1QoT9fmPYr8

const TEST_CONST_1: &str = "test_const_1";
const TEST_CONST_2: &str = "test_const_2";

// example usage of static lifetime refs (refs that are valid through the life of program)
fn test_const_func(a: &'static str, b: &'static str) -> &'static str {
    if a > b {
        a
    } else {
        b
    }
}

// define lifetime 'a and link it to the used references
fn get_str_ref<'a>(a: &'a str, b: &'a str) -> &'a str {
    if a > b {
        a
    } else {
        b
    }
}

// lifetime subtyping: b has to live at least as long as a ('b: 'a)
fn get_int_ref<'a, 'b: 'a>(a: &'a i32, b: &'b i32) -> &'a i32 {
    if a > b {
        a
    } else {
        b
    }
}

fn test_1<'a>(a: i32, b: f64, c: &'a str, d: &'a str) -> &'a str {
    if a == 1 && b > 2.0 {
        c
    } else {
        d
    }
}

// Below code will not compile: there is potential danger of dangling references, so we have to
// specify lifetimes for used here refs like we did it in test_1 function.
// fn test_1_without_lifetimes(a: i32, b: f64, c: &str, d: &str) -> &str {
//     if a == 1 && b > 2.0 {
//         c
//     } else {
//         d
//     }
// }

fn get_vec_slice(v: &[i32]) -> &[i32] {
    // no need to specify lifetime here - if we're returing ref to the input parameter ref
    // then rust knows that these refs share same lifetime
    &v[0..2]
}

#[allow(dead_code)]
fn get_vec_slice_2<'a>(v: &'a [i32], w: &'a [i32]) -> &'a [i32] {
    // compiler can't determine the lifetime, we have to set it explicitly

    if v.len() > w.len() {
        &v[0..2]
    } else {
        &w[0..2]
    }
}

// mixing syntax for generic types and lifetimes:
#[allow(dead_code)]
fn generics_example_and_lifetime<'a, T: std::cmp::PartialOrd>(a: &'a T, b: &'a T) -> &'a T {
    if a > b {
        a
    } else {
        b
    }
}

#[derive(Debug)]
struct TestLifetimeStruct<'a> {
    a: Vec<i32>,
    b: &'a str,
}

pub fn run() {
    let prefix = "lifetimes";
    let x = "a";

    {
        let y = "b";
        let _z = test_1(1, 2.5, x, y);
    }

    let a = "a";
    let b = "b";
    let str_ref = get_str_ref(a, b);
    println!("{}: get_str_ref('{}', '{}') -> '{}'", prefix, a, b, str_ref);

    let c: i32 = 20;
    let d: i32 = 21;
    let num_ref = get_int_ref(&c, &d);
    println!("{}: get_int_ref({}, {}) -> {}", prefix, c, d, num_ref);

    let v: Vec<i32> = vec![10, 20, 30, 40];
    let slice = get_vec_slice(&v);
    println!("{}: get_vec_slice({:?}) -> {:?}", prefix, v, slice);

    let const_test_result = test_const_func(TEST_CONST_1, TEST_CONST_2);
    println!("{}: test_const_func('{}', '{}') -> '{}'", prefix, TEST_CONST_1, TEST_CONST_2, const_test_result);

    let test_struct = TestLifetimeStruct{
        a: vec![1, 2, 3],
        b: prefix,
    };
    println!("{}: test_struct: {:?}, (a, b) = ({:?}, {})", prefix, test_struct, test_struct.a, test_struct.b);
}
