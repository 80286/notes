use std::rc::Rc;
use std::cell::RefCell;

#[derive(Debug)]
struct RcTestStruct {
    a: u32,
    b: String,
}

impl Drop for RcTestStruct {
    fn drop(&mut self) {
        println!("RcTestStruct.drop();");
    }
}

impl RcTestStruct {
    pub fn new(a: u32, b: &str) -> Self {
        Self { a: a, b: String::from(b), }
    }
}

fn f_rc<T>(prefix: &str, a: &Rc<T>, b: Option<&Rc<T>>) where T: std::fmt::Debug {
    let f_prefix = format!("{}: f_rc", prefix);

    match b {
        Some(value) =>  println!("{}: count (a, b) = ({}, {})", f_prefix, Rc::strong_count(a), Rc::strong_count(value)),
        None =>         println!("{}: count a: {}", f_prefix, Rc::strong_count(a)),
    }

    println!("{}: a: {:?}", f_prefix, a);
}

fn test_rc() {
    let prefix = "sharedptr: rc";

    {
        let _a = Rc::new(RcTestStruct::new(0, "a"));
        {
            let _b = Rc::clone(&_a);

            f_rc(prefix, &_a, Some(&_b));
        }

        f_rc(prefix, &_a, None);
    }
}

fn test_refcell() {
    let prefix = "sharedptr: refcell";
    let build_shared_ptr = |v: RcTestStruct| {
        Rc::new(RefCell::new(v))
    };

    let a = build_shared_ptr(RcTestStruct::new(0, "."));

    println!("{}: 1; a={:?}", prefix, a);

    {
        let b = Rc::clone(&a);

        let mut x = b.borrow_mut();

        x.a = 5;
        x.b = String::from("?");

        println!("{}: 2: b={:?} ({:?})", prefix, x, b);
    }

    println!("{}: 3: a={:?}", prefix, a);
}

pub fn run() {
    test_rc();
    test_refcell();
}
