fn test_privimive_str() {
    let prefix = "test_privimive_str";
    let test_string = "a b c d";

    println!("{prefix}: test_string.len(): {len}", prefix = prefix, len = test_string.len());

    // Splitting strings:
    for word in test_string.split_whitespace() { 
        println!("{0}: str split '{1}' -> '{2}' (len: {3})", prefix, test_string, word, word.len());
    }

    // Trimming strings:
    let trim_str_example = "  b   ";
    let trimmed_string = trim_str_example.trim();
    println!("{}: '{}'.trim() -> '{}'", prefix, trim_str_example, trimmed_string);

    // Convert primitive str to String:
    let _converted: String = prefix.to_string();
}

fn test_str_objects() {
    let prefix = "test_str_objects";
    let mut test_string = String::from("a b c d");

    println!("{}: String split '{}' len: {}", prefix, test_string, test_string.len());

    // Splitting strings:
    for word in test_string.split_whitespace() {
        println!("{}: String '{}' -> '{}'", prefix, test_string, word);
    }

    // Appending single characters:
    test_string.push(' ');
    test_string.push('e');

    // Appending substrings:
    test_string.push_str(" f");

    // Empty check:
    assert_eq!(false, test_string.is_empty());

    // Checking substrings:
    let part = "b c";
    if test_string.contains(part) {
        println!("{}: '{}' contains '{}'", prefix, test_string, part);
    }
    
    // Replacing substrings:
    let _replacement_test_str = test_string.replace(part, "??");
    assert_eq!(_replacement_test_str, "a ?? d e f");

    // Creating Strings with certain capacity:
    let mut test_string_2 = String::with_capacity(5);

    // will print 5:
    println!("{}: #1: capacity '{}': {}", prefix, test_string_2, test_string_2.capacity());

    for _ in 0..10 {
        test_string_2.push('.');
    }

    // will print 10:
    println!("{}: #2: capacity '{}': {}", prefix, test_string_2, test_string_2.capacity());
}

pub fn run() {
    test_privimive_str();
    test_str_objects();
}
