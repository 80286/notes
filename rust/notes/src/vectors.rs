fn test_vectors() {
    let prefix = "test_vectors";
    let v: Vec<i32> = vec![0, 1, 2];

    println!("{}: v={:?}", prefix, v);
    println!("{}: {:?}.len(): {}", prefix, v, v.len());
    println!("{}: size of bytes for Vec {:?}: {}", prefix, v, std::mem::size_of_val(&v));

    // Iterating through vector:
    for item in v.iter() {
        println!("{}: item: {}", prefix, item);
    }

    // Building string using vector of strings:
    let items_str: Vec<&str> = vec!["x", "y", "z"];
    let sep: &str = "/";
    println!("{}: {:?}.join({}): '{}'", prefix, items_str, sep, items_str.join(sep));

    let mut v2: Vec<i32> = vec![0, 2];
    // Modyfing vector:
    v2[1] = 1;

    // Adding items to vector:
    v2.push(3);
    let last_item = v2.pop().unwrap();
    assert_eq!(last_item, 3);

    let v3: Vec<i32> = vec![1, 2, 3];
    let mut v3d = v3.clone();

    // Modifying single vector items in loop:
    for num in v3d.iter_mut() {
        *num *= 2;
    }
    println!("{}: {:?} * 2 -> {:?}", prefix, v3, v3d);
}

pub fn run() {
    test_vectors();
}
