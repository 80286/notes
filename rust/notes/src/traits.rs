trait TestTrait {
    fn do_test(&self) -> String;
}

#[derive(Debug)]
struct TestStruct {
    a: i32,
    b: i32
}

impl TestTrait for TestStruct {
    fn do_test(&self) -> String {
        format!("TestTrait impl [a={}, b={}]", self.a, self.b)
    }
}

impl TestStruct {
    fn new() -> TestStruct {
        TestStruct {
            a: 1,
            b: 1,
        }
    }
}

// function that accepts only objects with some trait:
fn do_sth_on_obj_with_trait(item: &impl TestTrait) {
    println!("traits: do_sth_on_obj_with_trait test: <{}>", item.do_test());
}

// Definition same as above func (syntax sugar) + allow to list all allowed traits:
fn do_sth_on_obj_with_trait_2<T: TestTrait + std::fmt::Debug>(item: &T) {
    println!("traits: do_sth_on_obj_with_trait_2 test: <{}> debug trait: <{:?}>", item.do_test(), item);
}

fn show_extension_example() {
    // Example: implementing methods for builtin types:
    trait Show {
        fn show(&self) -> String;
    }
    impl Show for u32 {
        fn show(&self) -> String {
            format!("[{}]", self)
        }
    }
    impl Show for f32 {
        fn show(&self) -> String {
            format!("<{}>", self)
        }
    }

    println!("{}", (3 as u32).show());
    println!("{}", (3.0 as f32).show());
}

pub fn run() {
    let prefix = "traits";
    let a = TestStruct::new();

    println!("{}: TestTrait impl test: <{}>", prefix, a.do_test());
    do_sth_on_obj_with_trait(&a);
    do_sth_on_obj_with_trait_2(&a);

    show_extension_example();
}
