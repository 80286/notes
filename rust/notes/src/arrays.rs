fn test_primitive_arrays() {
    let prefix = "test_primitive_arrays";
    let items: [i32; 3] = [0, 1, 2];

    println!("{}: items={:?}", prefix, items);
    println!("{}: {:?}.len(): {}", prefix, items, items.len());
    println!("{}: size of bytes for {:?}: {}", prefix, items, std::mem::size_of_val(&items));

    // Iterating through array:
    for item in items {
        println!("{}: item: {}", prefix, item);
    }

    // Building string using array of strings:
    let items_str: [&str; 3] = ["x", "y", "z"];
    let sep: &str = "/";
    println!("{}: {:?}.join({}): '{}'", prefix, items_str, sep, items_str.join(sep));

    let mut items2: [i32; 2] = [0, 2];
    // Modyfing array:
    items2[1] = 1;

    // Slices:
    let slice: &[i32] = &items[0..1];
    println!("{}: example slice: {:?}[0..1] -> {:?}", prefix, items, slice);
}

pub fn run() {
    test_primitive_arrays();
}
