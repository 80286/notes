use std::thread;
use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::time::Duration;

fn thread_simple_test() {
    let prefix = "thread_simple_test";
    let heap_test_str = String::from("?");

    // Rust doesn't know how long ref to the captured var will be valid.
    // Use move keyword to move ownership of the env vars into body of thread lambda.
    let handle = thread::spawn(move || {
        println!("{}: thread: heap_test_str='{}'", prefix, heap_test_str);

        for i in 1..10 {
            println!("{}: thread: i={}", prefix, i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    // Below call will not compile because heap_test_str has been moved into thread func
    // and heap_test_str is no longer valid inside function that started thread:
    // println!("{}: test ref to heap_test_str: {}", prefix, heap_test_str);

    for i in 1..10 {
        println!("{}: outside thread: i={}", prefix, i);
    }

    println!("{}: waiting for thread...", prefix);
    handle.join().unwrap();
}

fn message_passing_test() {
    // mpsc = multiple producer, single consumer
    // tx - transmitter
    // rx - receiver
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let v = String::from("?");

        println!("message_passing_test: thread: sending msg '{}' using transmitter...", v);
        tx.send(v).unwrap();

        // notice: v is not more valid here due to the change of its ownership!
        // println!("{}", v);
    });

    // recv call will block main thread until a value will be sent down to the channel
    // use try_recv() instead for non-block call
    // there is no need to call join()
    let received = rx.recv().unwrap();
    println!("message_passing_test: received '{}'", received);
}

// Sending Multiple Values and Seeing the Receiver Waiting:
fn message_passing_test_2() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let values = vec![
            String::from("a1"),
            String::from("a2"),
            String::from("a3"),
        ];

        for value in values {
            println!("message_passing_test_2: thread: sending msg '{}'...", value);

            tx.send(value).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    // treat rx as iterator for receiving messages:
    for received_value in rx {
        println!("message_passing_test_2: main thread received msg '{}'", received_value);
    }
}

// Creating Multiple Producers by Cloning the Transmitter
fn message_passing_test_3() {
    let (tx, rx) = mpsc::channel();

    let tx1 = tx.clone();
    thread::spawn(move || {
        let values = vec![
            String::from("t1-a"),
            String::from("t1-b"),
            String::from("t1-c"),
        ];

        for value in values {
            println!("message_passing_test_3: thread-1: sending msg '{}'...", value);

            tx1.send(value).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        let values = vec![
            String::from("t2-a"),
            String::from("t2-b"),
            String::from("t2-c"),
        ];

        for value in values {
            println!("message_passing_test_3: thread-2: sending msg '{}'...", value);

            tx.send(value).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received_value in rx {
        println!("message_passing_test_3: main thread received msg '{}'", received_value);
    }
}

fn mutex_example() {
    // Arc - atomic reference counted type, atomic version of std::rc::Rc
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for i in 0..3 {
        let counter = Arc::clone(&counter);

        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();

            println!("mutex_example: thread {}: increasing num={} using mutex lock...", i, num);
            *num += 1;
        });

        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }
}

pub fn run() {
    // https://doc.rust-lang.org/book/ch16-01-threads.html
    thread_simple_test();

    // https://doc.rust-lang.org/book/ch16-02-message-passing.html
    message_passing_test();
    message_passing_test_2();
    message_passing_test_3();

    // https://doc.rust-lang.org/book/ch16-03-shared-state.html
    mutex_example();
}
