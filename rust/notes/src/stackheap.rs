fn stack_procedure(mut param: f64) {
    param += 0.5;
    println!("stack_procedure: param={}", param);
    // external value didn't change
}

fn heap_procedure(param: &Box<f64>) {
    // by using reference (&) of var, ownership of external variable will be borrowed
    // to the scope of this function
    println!("heap_procedure: param={}", param);
}

#[allow(unused_variables)]
fn stack_vars_examples() {
    let s_i32: i32 = 1;
    let s_f32: f32 = 1.25;
    let s_bool: bool = false;
    let s_str: &str = "/";
    let s_char: char = '1';

    if s_i32 == 1 {
        let s_i64: i64 = 2;
    } // s_i64 goes out of scope

    // ok: values of basic types stored on the stack will be copied because size of the them are known at compile time:
    let s_i32_2 = s_i32;

    let s_f64: f64 = 0.0;
    println!("stack_vars_examples: s_f64 before call: {}", s_f64);
    stack_procedure(s_f64);
    println!("stack_vars_examples: s_f64 after call: {}", s_f64);
}

#[allow(unused_variables)]
fn heap_vars_examples() {
    let h_vec: Vec<i8> = Vec::new();
    let h_str: String = String::from("?");
    let h_box: Box<i8> = Box::new(30);

    let h_box_2 = h_box;

    // fail: below println! call will terminate compilation
    // there should be only one owner of value at the time.
    // Ownership of h_box var has changed after above assignment to h_box_2 var, that's why we
    // can't directly refer to the h_box var.
    // println!("h_box: {}", h_box);

    // Possible solution:
    // assign new cloned object
    let h_box_cloned = h_box_2.clone();

    let v_box: Box<f64> = Box::new(10.0);
    println!("heap_vars_examples: v_box before call: {}", v_box);
    heap_procedure(&v_box);
    println!("heap_vars_examples: v_box after call: {}", v_box);
}

pub fn run() {
    stack_vars_examples();
    heap_vars_examples();
}
