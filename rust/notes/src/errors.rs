use std::fs::File;
use std::io;
use std::io::Read;

fn do_question_mark_test() -> Result<String, io::Error> {
    let mut f = File::open("test")?;

    let mut s = String::new();
    f.read_to_string(&mut s)?;

    // Question mark operator used after call that returns obj of Result type will terminate implemented
    // function and return Err(msg) (of type io::Error in this case)
    //
    // also: in case of Ok result ? operator will call unwrap() on the Result object.

    Ok(s)
}

fn do_match_for_result() {
    // let prefix = "errors:do_question_mark_test";
    // let mut f = Fil
    let f = File::open("test");

    let result = match f {
        Ok(_) => String::from("OK"),
        Err(msg) => format!("Failed: {}", msg),
    };

    println!("do_match_for_result: {}", result);
}

pub fn run() {
    let prefix = "errors";
    let result = do_question_mark_test();

    match result {
        Ok(s) => println!("{}: [match ok] read content of file into String: {}", prefix, s),
        Err(e) => println!("{}: [match err] error occured while reading file: {}", prefix, e)
    }

    do_match_for_result();
}
