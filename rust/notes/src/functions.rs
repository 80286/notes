fn show_filtered_values<F>(is_allowed: F, n: u32) where F: Fn(u32) -> bool {
    for i in 0..n {
        if is_allowed(i) {
            println!("show_filtered_values: {} matched.", i);
        }
    }
}

pub fn run() {
    let add_func = |a: u32, b: u32| a + b;

    let a = add_func(1, 3);
    assert_eq!(a, 4);

    let z = 10;
    let add_func_closure = |a: u32, b: u32| a + b + z;
    assert_eq!(add_func_closure(1, 2), 13);

    show_filtered_values(|x: u32| x % 2 == 0, 10);
}
