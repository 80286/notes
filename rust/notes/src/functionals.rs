#[derive(Clone, Debug)]
struct ExStruct {
    a: u32,
    b: String,
}

fn get_sum_of_array(v: &[i32]) -> i32 {
    v.iter().sum()
}

fn get_doubled_vec(v: &[i32]) -> Vec<i32> {
    v.iter().map(|x| x * 2).collect()
}

// Take ref to the vector and return vector of refs from input vector:
fn get_selected_structs_and_return_refs<T, F>(v: &Vec<T>, f: F) -> Vec<&T>
    where F: FnMut(&&T) -> bool {

    v.iter().filter(f).collect()
}

// Take ref to the vector and return vector with copy of items from input vector:
fn get_selected_structs_and_return_copied_items(v: &Vec<ExStruct>) -> Vec<ExStruct> {
    v.iter()
     .filter(|s| s.a == 1 && s.b == "a")
     .map(|s| s.clone())
     .collect()
}

fn zip_example(prefix: &str) {
    let v: [i32; 4] = [0, 1, 2, 3];
    let w: [&str; 4] = ["a", "b", "c", "d"];

    let x: Vec<String> = v.iter()
        .zip(&w)
        .map(|(&a, &b)| format!("[{}:{}]", a, b))
        .collect();

    println!("{}: zip_example: zip({:?}, {:?}) -> {:?}", prefix, v, w, x);
}

fn fold_example(prefix: &str) {
    let v: Vec<i32> = vec![1, 2, 3];
    let w = v.iter().fold(0, |a, b| a + b);
    let x = v.iter().fold(1, |a, b| a * b);

    println!("{}: fold_example: {:?} -> fold(0, |a, b| a + b) -> {:?}", prefix, v, w);
    println!("{}: fold_example: {:?} -> fold(1, |a, b| a * b) -> {:?}", prefix, v, x);
}

pub fn run() {
    let prefix = "functionals";
    let v: [i32; 4] = [0, 1, 2, 3];

    let v_sum = get_sum_of_array(&v);
    println!("{}: 1: get_sum_of_array({:?}) -> {}", prefix, v, v_sum);

    let v_map: Vec<i32> = get_doubled_vec(&v);
    println!("{}: 2: get_doubled_vec({:?}) -> {:?}", prefix, v, v_map);

    let ev: Vec<ExStruct> = vec![
        ExStruct{ a: 1, b: String::from("a") },
        ExStruct{ a: 2, b: String::from("b") },
        ExStruct{ a: 3, b: String::from("c") },
    ];

    let ev_filtered = get_selected_structs_and_return_refs(&ev, |s| s.a == 1 && s.b == "a");
    let ev_filtered_2 = get_selected_structs_and_return_copied_items(&ev);

    println!("{}: 3: get_selected_structs_and_return_refs({:?}) -> {:?}", prefix, ev, ev_filtered);
    println!("{}: 4: get_selected_structs_and_return_copied_items({:?}) -> {:?}", prefix, ev, ev_filtered_2);

    zip_example(&prefix);
    fold_example(&prefix);
}
