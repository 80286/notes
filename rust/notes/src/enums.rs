enum TestEnum {
    ValueA,
    ValueB,
    ValueC,
}

fn to_str(v: TestEnum) -> String {
    match v {
        TestEnum::ValueA => return String::from("VA"),
        TestEnum::ValueB => return String::from("VB"),
        TestEnum::ValueC => return String::from("VC"),
    }
}

enum TestEnum2 {
    Int(i32),
    Float(f64),
    Text(String),
}

pub fn run() {
    let a = to_str(TestEnum::ValueA);
    assert_eq!(a, "VA");

    let b = TestEnum::ValueB;
    assert_eq!(to_str(b), "VB");

    assert_eq!(to_str(TestEnum::ValueC), "VC");

    // Using enum to store multiple types (https://doc.rust-lang.org/book/ch08-01-vectors.html):
    let _v = vec![
        TestEnum2::Int(10),
        TestEnum2::Int(11),
        TestEnum2::Float(0.1),
        TestEnum2::Text(String::from("?")),
    ];
}
