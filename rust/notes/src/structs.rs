use std::fmt;

struct TestTupleStruct(u8, u8, u8);

// Thanks to the impl. of Debug trait we can print instances of TestTupleStruct:
impl fmt::Debug for TestTupleStruct {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("TestTupleStruct")
            .field("0", &self.0)
            .field("1", &self.1)
            .field("2", &self.2)
            .finish()
    }
}

struct TestStruct {
    a: String,
    b: String,
}

impl TestStruct {
    fn new(a: &str, b: &str) -> TestStruct {
        TestStruct {
            a: String::from(a),
            b: String::from(b),
        }
    }

    fn to_string(&self) -> String {
        format!("[{} : {}]", self.a, self.b)
    }

    fn clear(&mut self) {
        self.a = String::from("");
        self.b = String::from("");
    }

    fn set(&mut self, new_str: &str) {
        self.a = new_str.to_string();
        self.b = new_str.to_string();
    }

    fn to_tuple(self) -> (String, String) {
        (self.a, self.b)
    }
}

impl fmt::Debug for TestStruct {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("TestStruct")
            .field("a", &self.a)
            .field("b", &self.b)
            .finish()
    }
}

// Simplified way of implementing default debug & clone traits instead of creating manual trait impl.:
#[derive(Debug, Clone)]
struct AnotherTestStruct {
    a: u32,
    b: u32,
}

pub fn run() {
    let prefix = "structs";

    let a = TestTupleStruct(1, 2, 3);
    println!("{}: a: {:?}; (a.0, a.1, a.2) = ({}, {}, {})", prefix, a, a.0, a.1, a.2);

    let mut b = TestStruct::new("a", "b");
    println!("{}: b: {:?}; (b.a, b.b) = (\"{}\", \"{}\"); b.to_string(): {}", prefix, b, b.a, b.b, b.to_string());

    b.clear();
    println!("{}: b after clear() call: {:?}", prefix, b);

    b.set("?");
    println!("{}: b after set() call: {:?}", prefix, b);

    println!("{}: b.to_tuple: {:?}", prefix, b.to_tuple());

    let c = AnotherTestStruct{ a: 25, b: 50 };
    println!("{}: c: {:?}; (c.a, c.b)=({}, {}); c.clone(): {:?}", prefix, c, c.a, c.b, c.clone());
}
