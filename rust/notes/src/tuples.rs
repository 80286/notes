fn test_fun1() -> (u32, String) {
    (1, String::from("test_fun1"))
}

fn test_fun2() -> (u32, &'static str) {
    (2, "test_fun2")
}

fn test_print<T, V, F>(prefix: &str, f: F) 
    where 
        F: Fn() -> (T, V),
        T: std::fmt::Debug,
        V: std::fmt::Debug {

    // Generic type params T and V have to implement Debug trait due to the printing instances using '{:?}'

    let test_fun_result = f();
    println!("{}: test_fun_result={:?}", prefix, test_fun_result);

    let (tfa, tfb): (T, V) = test_fun_result;
    println!("{}: tfa={:?}, tfb='{:?}'", prefix, tfa, tfb);
}

pub fn run() {
    let prefix = "tuples";
    let a: (i32, i64) = (1, 2);
    let b: (&str, i32) = ("b", 2);

    println!("{}: a={:?}; a.0={}, a.1={}", prefix, a, a.0, a.1);
    println!("{}: b={:?}; b.0='{}', b.2={}", prefix, b, b.0, b.1);

    test_print(prefix, test_fun1);
    test_print(prefix, test_fun2);
}
