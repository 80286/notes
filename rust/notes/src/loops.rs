pub fn run() {
    let prefix = "loops";
    let mut i: i32 = 0;

    // Infinite loop:
    loop {
        if i == 5 {
            println!("{}: break", prefix);
            break;
        }

        println!("{}: loop; i={}", prefix, i);
        i += 1;
    }

    i = 0;
    while i < 3 {
        if i < 2 {
            println!("{}: while: i < 2 ({})", prefix, i);
        } else {
            println!("{}: while: i >= 2 ({})", prefix, i);
        }
        i += 1;
    }

    for i in 25..27 {
        println!("{}: for i={}", prefix, i);
    }
}
