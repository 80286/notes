fn test_if() -> bool {
    let (a, b) = (10, 5);

    if (a == 10 && b == 5) || false {
        return true
    }
    else if b == 4 {
        return false
    }

    false
}

pub fn run() {
    let _a = test_if();
    let _b : bool = if 1 == 0 { true } else { false };

    let prefix = "conditions";
    let f = |x| x * 2;
    let c = (1..10).map(f).collect::<Vec<i32>>();
    println!("{}: {:?}", prefix, c);
}
