use std::env;

mod arrays;
mod strings;
mod tuples;
mod vectors;
mod conditions;
mod loops;
mod functions;
mod structs;
mod enums;
mod stackheap;
mod hashmaps;
mod errors;
mod generics;
mod traits;
mod threads;
mod lifetimes;
mod functionals;
mod sharedptr;

mod tests;

fn main() {
    let argv: Vec<String> = env::args().collect();
    let argv0 = argv[0].clone();
    println!("main: argv={:?}; argv0: '{}'", argv, argv0);

    strings::run();
    tuples::run();
    arrays::run();
    vectors::run();
    conditions::run();
    loops::run();
    functions::run();
    structs::run();
    enums::run();
    stackheap::run();
    hashmaps::run();
    errors::run();
    generics::run();
    traits::run();
    threads::run();
    lifetimes::run();
    functionals::run();
    sharedptr::run();
}
