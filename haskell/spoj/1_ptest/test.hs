readInt :: IO Int
readInt = readLn

main =  do
        a <- readInt
        b <- readInt
        print (a + b)
