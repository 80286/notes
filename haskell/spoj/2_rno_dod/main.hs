-- Needed for: replicateM
import Control.Monad

readInt :: IO Integer
readInt = readLn

strToInt :: String -> Integer
strToInt x = (read x :: Integer)

calcSumForLine :: String -> Integer
calcSumForLine line = (sum . map strToInt . words) line

printLines :: [Integer] -> IO()
printLines [] = return ()
printLines (x:xs) = do
                    print (x)
                    printLines xs

amen :: Integer -> IO()
amen x | x <= 0 = return ()
       | x > 0  = do
                  _ <- getLine
                  values <- getLine
                  print (calcSumForLine values)
                  amen (x - 1)

main = do
    numOfTests <- readInt
    amen numOfTests
