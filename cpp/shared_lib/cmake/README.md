### Building example shared library using cmake

Building & running example:

```
mkdir build
cd build
cmake ..
make
./TestLibUsage
```

Building standalone library:

```
cd libs/lib
mkdir build
cd build
cmake ..
```
