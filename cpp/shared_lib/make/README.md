### Building example shared library using make

Building & running example:

```
make
LD_LIBRARY_PATH="$(realpath .)/lib" ./usage
```

Building standalone so file:
```
make -C lib
```
