%skeleton "lalr1.cc"
%require "3.8.1"
%header

%define api.token.constructor
%define api.token.prefix {TOK_}
%define api.value.type variant

%define parse.assert
%define parse.error verbose
%define parse.lac full
%define parse.trace

%locations

%lex-param { Driver& drv }
%lex-param { Lexer& lexer }
%parse-param { Driver& drv }
%parse-param { Lexer& lexer }

%code top {
    /* This code will be moved into the top of implementation of parser (Parser.cpp) */
    #include "Driver.h"
    #include "Lexer.h"

    static yy::parser::symbol_type yylex(Driver& drv, Lexer& lexer) {
        return lexer.get_next_token();
    }
}

%code requires {
    /* This code will be moved into the top of generated header (Parser.hpp) */
    #include <string.h>

    class Driver;
    class Lexer;
}

%token
    ASSIGN  ":="
    MINUS   "-"
    PLUS    "+"
    STAR    "*"
    SLASH   "/"
    LPAREN  "("
    RPAREN  ")"
    SEMICOLON ";"
    COMMA ","
;

%token END 0 "eof"
%token <std::string> IDENTIFIER "identifier";
%token <int> NUMBER "number";
%nterm <int> exp;

%%

%start unit;
unit: assignments {};

assignments:
    %empty                  {}
|   assignments assignment  {};

assignment:
    IDENTIFIER ASSIGN exp { drv.m_variables[$1] = $3; };

%left "+" "-";
%left "*" "/";

exp:
    "number"
|   "identifier"        { $$ = drv.m_variables[$1]; }
|   exp PLUS exp        { $$ = $1 + $3; }
|   exp MINUS exp       { $$ = $1 - $3; }
|   exp STAR exp        { $$ = $1 * $3; }
|   exp SLASH exp       { $$ = $1 / $3; }
|  LPAREN exp RPAREN    { $$ = $2; }

%%

void yy::parser::error(const yy::location& l, const std::string& msg) {
    std::cerr << "error: " << l << ": " << msg << std::endl;
}
