%{
/* this code will be moved into impl. of scanner (Lexer.cpp) */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Lexer.h"
#include "Parser.hpp"

#define yyterminate return yy::parser::make_EOF();

#undef YY_DECL
#define YY_DECL yy::parser::symbol_type Lexer::get_next_token()

%}

%option noyywrap
%option batch
%option debug
%option c++
%option nodefault
%option yyclass="Lexer"

blank   [ \t\r]
int     [1-9][0-9]*
id      [A-Za-z][A-Za-z0-9]*

%%

{blank}     {
                m_driver.step();
            }

\n+         {
                m_driver.nextLine();
            }

{int}       {
                const int number = strtoull(yytext, 0, 10);
                printf("Found num '%s'(int: %d); Location: %s\n", yytext, number);

                return yy::parser::make_NUMBER(number, m_driver.incLocation(yytext));
            }

{id}        {
                printf("Found id '%s'\n", yytext);
                return yy::parser::make_IDENTIFIER(yytext, m_driver.incLocation(yytext));
            }

":="        {
                return yy::parser::make_ASSIGN(m_driver.incLocation(yytext));
            }

"/"         {
                return yy::parser::make_SLASH(m_driver.incLocation(yytext));
            }

"*"         {
                return yy::parser::make_STAR(m_driver.incLocation(yytext));
            }

"+"         {
                return yy::parser::make_PLUS(m_driver.incLocation(yytext));
            }

"-"         {
                return yy::parser::make_MINUS(m_driver.incLocation(yytext));
            }

"("         {
                return yy::parser::make_LPAREN(m_driver.incLocation(yytext));
            }

")"         {
                return yy::parser::make_RPAREN(m_driver.incLocation(yytext));
            }

;           {
                return yy::parser::make_SEMICOLON(m_driver.incLocation(yytext));
            }

,           {
                return yy::parser::make_COMMA(m_driver.incLocation(yytext));
            }

.           {
                throw yy::parser::syntax_error(m_driver.getLocation(), "Invalid character: " + std::string(yytext));
            }

<<EOF>>     {
                return  yy::parser::make_END(m_driver.incLocation(yytext));
            }
%%
