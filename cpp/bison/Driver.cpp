#include "Driver.h"
#include "Parser.hpp"
#include "Lexer.h"

Driver::Driver() {
}

int Driver::parse(const std::string& file) {
    Lexer lexer(*this);
    yy::parser parser(*this, lexer);

    const auto result = parser.parse();
    printf("result: %d\n", result);

    return result;
}

void Driver::print() const {
    printf("Scope:\n");

    for(const auto& [k, v]: m_variables) {
        printf("\t[%s] = %d\n", k.c_str(), v);
    }
}

yy::location Driver::incLocation(const char* str) {
    const auto end = m_position + strlen(str);
    yy::location result(m_position, end);

    m_position = end;

    return result;
}

yy::location Driver::getLocation() {
    return yy::location(m_position, m_position);
}

void Driver::step() {
    m_position += 1;
}

void Driver::nextLine() {
    m_position.lines(1);
}
