### Simple flex+bison example

## Usage

Requirements:

- bison
- flex
- g++

Running:

```
make
./main
```

Example input:
```
a:=5
b:=3 * b
c:=b * 5
<EOF>
```

## lexer.l

TODO

## parser.y

`%define parse-param ...` - from the manual: declare that one or more argument-declaration are additional yyparse arguments.

`%define lex-param ...` - from the manual: specify that argument-declaration are additional yylex argument declarations.

`%define parse.error verbose` - force verbose output on errors.

`%define parse.trace` - improvement for debugging correctness of grammar

`%define parse.lac full` - enable LAC (lookahead correction), another tool to improve reporting of syntax errors. 

`%define api.token.prefix {TOK_}` - will append `TOK_` prefix to the name of token constants in output hpp file.

Below list of tokens:

```
%token
    STAR  "*"
    (...)
;
```

defines tokens produced by lexer and related aliases that can simplify notation of grammar rules.

For example instead of creating rule `EXP STAR EXP` we'll be able to simplify it by writing `EXP * EXP`.


## Refs

- [bison manual](https://www.gnu.org/software/bison/manual/bison.html)
- [bison-flex-cpp-example](https://github.com/ezaquarii/bison-flex-cpp-example)
- [Lucy](https://github.com/HuuugeGames/Lucy)
