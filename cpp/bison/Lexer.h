#ifndef LEXER_H
#define LEXER_H

#undef yyFlexLexer
#include <FlexLexer.h>
#include "Driver.h"

class Lexer: public yyFlexLexer {
public:
    Lexer(Driver& driver) : m_driver(driver) {}

    yy::parser::symbol_type get_next_token();

private:
    Driver& m_driver;
};


#endif
