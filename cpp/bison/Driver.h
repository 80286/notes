#ifndef DRIVER_H
#define DRIVER_H

#include <string>
#include <map>
#include "Parser.hpp"
#include "location.hh"

class Driver {
public:
    Driver();

    int parse(const std::string& f);

    void print() const;

    std::map<std::string, int> m_variables;
    int m_result;

    yy::location incLocation(const char* str);
    yy::location getLocation();

    void step();
    void nextLine();

private:
    yy::position m_position;
};

#endif
