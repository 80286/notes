### Docker client commands

- `docker images` - list all downloaded docker images that are ready to use
- `docker pull <image>` - fetch image from [Docker registry](https://hub.docker.com/search?q=&type=image)
- `docker pull <image>:<version>` - pull specific version of image for example: `docker pull ubuntu:18.04` 

- `docker run <image>` - setup container that uses `<image>` (and quit if no commands were specified)
- `docker run <image> <command_to_exec_inside_container>` - setup container that uses `<image>`, execute given `<cmd>` and quit
- `docker run -it <image>` - setup container and start interactive mode
- `docker run --rm <image> (...)` - setup container and remove it after executing some commands
- `docker run --name <name> (...)`
- `docker run -p 1234:5678 (...)` - forward internal container port `5678` to the `1234` port on docker host
- `docker run --net <net_name> (...)` - setup container and assign it to some network (check `docker network` commands below).

- `docker ps` - list all running containers
- `docker ps -a` - list all running and stopped containers

- `docker container ls`
- `docker container logs <name|container_id>` - inspect logs of currently running container

- `docker rm <name|container_ids>` - remove instance/instances of container using id (use `docker ps -a` to list instances)
- `docker rm $(docker ps -a -q -f status=exited)` or `docker container prune`

- `docker stop <name|container_id>` - stop running container using id/name

Networking:

- `docker network ls`
- `docker network inspect <name|network_id>`
- `docker network create <name>`

Important note: names of the containers (specified using `--name`) from same docker network are resolved to proper IP addresses.

### Dockerfile

- `FROM image:version` - specify source image
- `WORKDIR path`
- `COPY path1 path2`
- `RUN cmd` - commands called once to prepare container to correctly execute `CMD` (in short: installing dependencies)
- `CMD ["argv0", "argv1"]` - commands that should be launched after running instance of container
- `EXPOSE 5000` - set container port that should be exposable to the docker host

- `docker build -t <user/name> .` - build an image using `Dockerfile` from `.`. After successful execution use `docker images` to verify its presence.
- `docker run <user/name>`


### docker-compose

docker-compose.yml

```
version: 3
services:
    service1:
        image: <image>
        container_name: c1
        volumes:
            - ./local-docker-host-path:/opt/container-path
    service2:
        image: <image>
        command: <cmd>
        environment:
            - ENV_VAR1="test"
        depends_on:
            - service1
        container_name: c2
        ports:
            - 1234:3567
        volumes:
            - containerdata1:/usr/share/data
volumes:
    containerdata1:
        driver: local
```

- `docker-compose up` - use in directory that contains `docker-compose.yml`
- `docker-compose ps`
- `docker-compose down -v`
- `docker-compose run <service1> <cmd>` - similar to `docker run` but refers to the names of the services defined in `docker-compose.yml`

### Refs

- [Docker curriculum](https://docker-curriculum.com/)
- [Docker curriculum: github](https://github.com/prakhar1989/docker-curriculum)
