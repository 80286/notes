<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8" media-type="xml" indent="yes"/>

<!-- This XSL template: -->
<!-- - copies all nodes of input XML file -->
<!-- - modify selected node attributes/values -->

<xsl:template match="@* | node()">
    <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
</xsl:template>

<xsl:template match="node[@name='4']/key">
    <xsl:copy>
        <xsl:attribute name="attr"><xsl:value-of select="concat('NEW_ATTR_', @attr)" /></xsl:attribute>
        <xsl:value-of select="concat('NEW_TEXT_', text())" />
    </xsl:copy>
</xsl:template>

</xsl:transform>
