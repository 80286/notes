package basics

import scala.concurrent._
import scala.concurrent.duration._
import ExecutionContext.Implicits.global
import scala.util.{Try, Success, Failure}

object FutureTest extends App {
  implicit class FutureIntExt(f: Future[Int]) {
    def doCheck1(): Future[Int] = {
      f.filter { v =>
        println("doCheck1.filter")
        v % 2 == 0
      }.transform {
        case s @ Success(v) =>
          println(s"doCheck1.transform: Success: $v % 2 == 0")
          s

        case f @ Failure(v) =>
          println(s"doCheck1.transform: Failure: $v % 2 != 0")
          f
      }
    }

    def doCheck2(): Future[Int] = {
      f.filter { v =>
        println("doCheck2.filter")
        v % 3 == 0
      }.transform {
        case s @ Success(v) =>
          println(s"doCheck2.transform: Success $v % 3 == 0")
          s
        case f @ Failure(v) =>
          println(s"doCheck2.transform: Failure: $v % 3 != 0")
          f
      }
    }
  }

  private def callFutureFor(value: Int): Unit = {
    println(s"Calling test for value: $value")

    Try {
      val f = Future(value)
        .doCheck1()
        .doCheck2()
        .map(v => println(s"Future finished with result: $v"))

      Await.result(f, 5.seconds)
    } match {
      case Failure(v) => println(s"callFutureFor($value) failed: $v")
      case s => s
    }

    println("------------------------------")
  }

  private def doFailedFutureTest: Unit = {
    val f = Future(10)
      .filter { v =>
        println(s"filter1")
        v % 3 == 0
      }.filter { v =>
        println(s"filter2: this code can't be reached by this future")
        v % 11 == 0
      }.map { v =>
        println(s"Final: $v")
      }

    Await.result(f, 5.seconds)
  }

  def run = {
    callFutureFor(value = 12)
    callFutureFor(value = 10)
    callFutureFor(value = 9)
    callFutureFor(value = 1)

    doFailedFutureTest
  }

  run
}
