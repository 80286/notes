package config

import com.typesafe.config.ConfigFactory
import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.Props

private class TestActor extends Actor with ActorLogging {
  private def logCfgIntValue(key: String) = {
    val value = context.system.settings.config.getInt(key)
    log.error(s"TestActor: config[$key] = $value")
  }

  private def logCfgStrValue(key: String) = {
    val value = context.system.settings.config.getString(key)
    log.error(s"TestActor: config[$key] = $value")
  }

  override def receive: Receive = {
    case msg =>
      log.info(s"TestActor received msg: $msg")

      logCfgIntValue(key = "amen.test.a")
      logCfgStrValue(key = "akka.loglevel")
  }
}

object AkkaConfigTest extends App {
  def testStringConfig = {
    val configStr =
    """
      | akka {
      |   loglevel = "INFO"
      | }
      | amen {
      |   test {
      |     a = 1
      |   }
      | }
    """.stripMargin

    println("### testStringConfig")
    val config = ConfigFactory.parseString(configStr)
    val actorSystem = ActorSystem("AkkaStringConfigTest", ConfigFactory.load(config))
    val testActor = actorSystem.actorOf(Props[TestActor], "testActor")
    testActor ! "test-msg"
  }

  def testResourceConfig = {
    println("### testResourceConfig")
    // ActorSystem will load resources/application.conf by default:
    val actorSystem = ActorSystem("AkkaResourceConfigTest")
    val testActor = actorSystem.actorOf(Props[TestActor], "testActor")
    testActor ! "test-msg"
  }

  def testResourceSpecialConfig = {
    println("### testResourceSpecialConfig")
    val config = ConfigFactory.load().getConfig("amen")
    val actorSystem = ActorSystem("AkkaResourceSpecialConfigTest", config)
    val testActor = actorSystem.actorOf(Props[TestActor], "testActor")
    testActor ! "test-msg"
  }

  def testAnotherResourceConfig = {
    val configKey ="amen.test.v" 
    val configPath = "testdir/application.conf"
    val config = ConfigFactory.load(configPath)
    val configValue = config.getString(configKey) 
    println(s"Config for $configKey from $configPath: $configValue")
  }

  def testAnotherResourceFormats = {
    val key = "amen.test.v"

    val jsonConfigFilename = "json/testconf.json"
    val jsonConfig = ConfigFactory.load(jsonConfigFilename)
    val jsonValue = jsonConfig.getString(key)

    val propConfigFilename = "properties/testconf.properties"
    val propConfig = ConfigFactory.load(propConfigFilename)
    val propValue = propConfig.getString(key)

    println("### testAnotherResourceFormats")
    println(s"$jsonConfigFilename: $key -> $jsonValue")
    println(s"$propConfigFilename: $key -> $propValue")
  }

  testStringConfig
  testResourceConfig
  testResourceSpecialConfig
  testAnotherResourceConfig
  testAnotherResourceFormats
}
