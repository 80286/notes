package actors3

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.Props
import akka.actor.ActorSystem
import akka.actor.PoisonPill

object ActorLifecycle extends App {

  case class StartChild(name: String)
  class TestActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case StartChild(name) => context.actorOf(Props[TestActor], name)
    }
    override def preStart(): Unit = {
      log.info(s"preStart for actor ${self.path}")
    }
    override def postStop(): Unit = {
      log.info(s"postStop for actor ${self.path}")
    }
  }

  private def showHowPoisonPillRemovesSubActors = {

    val as = ActorSystem("as")
    val parent = as.actorOf(Props[TestActor], "parent")

    parent ! StartChild("child-1")
    parent ! StartChild("child-2")
    // Sending PoisonPill to parent will kill all child actors of parent first, and then parent itself
    parent ! PoisonPill
  }

  object Fail
  object FailChild
  object CheckChild
  object Check

  class ParentActor extends Actor with ActorLogging {
    private val child = context.actorOf(Props[ChildActor], "supervisedChild")

    override def receive: Receive = {
      case FailChild =>
        log.warning("Failing child...")
        child ! Fail
      case CheckChild =>
        log.warning("Checking child...")
        child ! Check
    }
  }

  class ChildActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case Fail =>
        log.warning("Throwing exception...")
        throw new RuntimeException("amen")

      case Check =>
        log.info(s"Check passed (${self.path})")
    }

    // preRestart is called by old instance
    override def preRestart(reason: Throwable, message: Option[Any]): Unit =
      log.info(s"preRestart: ${self.path} restarting because of: ${reason.getMessage()}")

    // postRestart is called by new instance
    override def postRestart(reason: Throwable): Unit =
      log.info(s"postRestart: ${self.path}: restarted")

    override def preStart(): Unit =
      log.info(s"preStart: ${self.path}")

    override def postStop(): Unit =
      log.info(s"postStop: ${self.path}")
  }

  private def run = {

    val as = ActorSystem("as")
    val supervisor = as.actorOf(Props[ParentActor], "parent")
    supervisor ! FailChild
    supervisor ! CheckChild
  }

  // showHowPoisonPillRemovesSubActors
  run
}
