package actors3

import akka.actor.Actor
import akka.actor.ActorLogging
import scala.io.Source
import java.io.File
import akka.actor.ActorSystem
import akka.actor.Props
import akka.pattern.BackoffSupervisor
import akka.pattern.BackoffOpts
import java.time.Duration
import java.time
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy

object BackoffSupervisorPattern extends App {

  case class ReadFile(filename: String)

  class TestActor extends Actor with ActorLogging {
    private var dataSource: Source = null

    override def preStart(): Unit = {
      log.warning("Actor is starting...")
    }
    override def postStop(): Unit = {
      log.warning("Actor has stopped")
    }
    override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
      log.warning("Actor is restarting...")
    }

    override def receive: Receive = {
      case ReadFile(filename) =>
        if(dataSource == null)
          dataSource = Source.fromFile(new File(s"src/main/resources/testfiles/$filename"))

        log.info("Reading of file finished: " + dataSource.getLines().toList)
    }
  }

  import scala.concurrent.duration._

  private def runExample1 = {
    val actorSystem = ActorSystem("as")

    val backoffSupervisorProps = BackoffSupervisor.props(
      BackoffOpts.onFailure(
        childProps = Props[TestActor],
        childName = "backoffActor",
        minBackoff = 3.seconds,
        maxBackoff = 30.seconds,
        randomFactor = 0.2,
      )
    )

    // Using above props will create backoff supervisor actor and child actor to be supervised (TestActor):
    val backoffSupervisor = actorSystem.actorOf(backoffSupervisorProps, "supervisor")
    // filenotfound exception will cause a restart of an actor:
    backoffSupervisor ! ReadFile("nonexistingfile.txt")
  }

  private def runExample2 = {
    val actorSystem = ActorSystem("as")
    val stopSupervisorProps = BackoffSupervisor.props(
      BackoffOpts.onStop(
        childProps = Props[TestActor],
        childName = "stopBackoffActor",
        minBackoff = 3.seconds,
        maxBackoff = 30.seconds,
        randomFactor = 0.2
        ).withSupervisorStrategy(OneForOneStrategy() { _ => SupervisorStrategy.Stop })
      )
    val stopSupervisorActor = actorSystem.actorOf(stopSupervisorProps)
    stopSupervisorActor ! ReadFile("anothernotexistingfile.txt")
  }

  private def runExample3 = {
    class ActorThatFailsOnInit extends TestActor {
      override def preStart(): Unit = {
        log.info("preStart(); trying to read file...")
        Source.fromFile(new File("nonexistingfile.path"))
      }
    }
    val as = ActorSystem("as")
    val repeatedSupervisorProps = BackoffSupervisor.props(
      BackoffOpts.onStop(
        childProps = Props[ActorThatFailsOnInit],
        childName = "initFailActor",
        minBackoff = 1.seconds,
        maxBackoff = 30.seconds,
        randomFactor = 0.1 // will guarantee that not all actors will connect at the same time
      )
    )

    /* Below creation of actor will:
     * - spawn supervisor backoff actor
     * - spawn initFailActor
     * - throw ActorInitializationException and stop initFailActor
     * - backoff supervisor will try to create again initFailActor in 1s, 2s, 4s, 8s, ... 30s (will apply exponential delays between tries)
     */
    val repeatedSupervisor = as.actorOf(repeatedSupervisorProps, "supervisor")
  }

  // runExample1
  // runExample2
  runExample3
}
