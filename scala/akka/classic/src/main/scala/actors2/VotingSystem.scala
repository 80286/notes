package actors2

import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.ActorRef
import VotingSystem.Votes

case class Vote(candidate: String)
case object VoteStatusRequest
case class VoteStatusReply(candidate: Option[String])
class Citizen extends Actor {
  def receive: Receive = {
    case Vote(candidate) =>
      println(s"$this voted for $candidate")
      context.become(receiveForVoted(currentCandidate = candidate))

    case VoteStatusRequest =>
      sender() ! VoteStatusReply(None)
  }

  private def receiveForVoted(currentCandidate: String): Receive = {
    case VoteStatusRequest =>
      sender() ! VoteStatusReply(Some(currentCandidate))
  }
}

case class AggregateVotes(voters: Set[ActorRef], replyTo: ActorRef)
case class VoteResult(votes: Votes)
class VotingAggregator extends Actor {

  def receive: Receive = receiveAggregationRequest

  private def receiveAggregationRequest: Receive = {
    case AggregateVotes(voters, replyTo) =>
      voters.foreach { voter => voter ! VoteStatusRequest }
      context.become(receiveFor(Map.empty, stillWaiting = voters, replyTo))

    case msg =>
      println(s"Received unknown message: $msg")
  }

  private def receiveForFinishedVote: Receive = {
    case VoteResult(votes) =>
      println(s"Received VoteResult: $votes")

    case msg =>
      println(s"Received unknown message: $msg")
  }

  private def receiveFor(currentVotes: Votes, stillWaiting: Set[ActorRef], replyTo: ActorRef): Receive = {
    case VoteStatusReply(Some(candidate)) =>
      println(s"Received VoteStatusReply ($candidate) from ${sender()}")

      val updatedNewVotes = currentVotes + (candidate -> (currentVotes.getOrElse(candidate, 0) + 1))
      val updatedWaitingVoters = stillWaiting - sender()

      if(updatedWaitingVoters.isEmpty) {
        println(s"Vote has finished. Status: $updatedNewVotes")
        replyTo ! VoteResult(updatedNewVotes)
        context.become(receiveForFinishedVote)
      } else {
        println(s"Vote hasn't finished yet. Vote is still in progress, waiting for ${stillWaiting.size} votes...")
        context.become(receiveFor(updatedNewVotes, updatedWaitingVoters, replyTo))
      }

    case VoteStatusReply(None) =>
      sender() ! VoteStatusRequest

    case msg =>
      println(s"Received unknown msg: $msg")
  }
}

object VotingSystem extends App {
  type Votes = Map[String, Int]

  def run = {
    val actorSystem = ActorSystem("votingSystem")

    def getCitizenActor(name: String) = actorSystem.actorOf(Props[Citizen], name)
    val a1 = getCitizenActor("a1")
    val a2 = getCitizenActor("a2")
    val a3 = getCitizenActor("a3")
    val a4 = getCitizenActor("a4")

    a1 ! Vote("x")
    a2 ! Vote("y")
    a3 ! Vote("x")
    a4 ! Vote("z")

    val aggregatorActor = actorSystem.actorOf(Props[VotingAggregator])
    aggregatorActor ! AggregateVotes(Set(a1, a2, a3, a4), replyTo = aggregatorActor)
  }

  run
}
