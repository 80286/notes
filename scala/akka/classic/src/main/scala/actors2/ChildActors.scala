package actors2

import akka.actor.Actor
import akka.actor.Props
import akka.actor.ActorRef
import akka.actor.ActorSystem

object ParentActor {
  case class AddChild(name: String)
  case class TellChild(msg: String)
}

class ParentActor extends Actor {
  import ParentActor._

  private def receiveWithChildRef(childRef: ActorRef): Receive = {
    case TellChild(msg) =>
      childRef forward msg
  }

  override def receive: Receive = {
    case AddChild(name) =>
      println(s"${self.path}: creating child '$name'...")
      context.become(receiveWithChildRef(context.actorOf(Props[ChildActor], name)))
  }
}

class ChildActor extends Actor {
  override def receive: Receive = {
    case msg =>
      println(s"${self.path}: child received msg: $msg")
  }
}

object ChildActors extends App {
  def run: Unit = {
    val actorSystem = ActorSystem("childActors")

    val parentActorRef = actorSystem.actorOf(Props[ParentActor], "parent")

    parentActorRef ! ParentActor.AddChild("child")
    parentActorRef ! ParentActor.TellChild("msg forwarded through parent")

    actorSystem.actorSelection("/user/parent/child") ! "msg sent from outside"
  }

  run
}
