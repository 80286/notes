package actors2

import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.ActorLogging

case object ActivateTaskHandler
case object DeactivateTaskHandler
class TaskHandler extends Actor with ActorLogging {
  def receive: Receive = receiveOnSleep

  private def receiveOnSleep: Receive = {
    case ActivateTaskHandler =>
      context.become(receiveOnStandBy)
      log.info("TaskHandler activated.")
    case msg =>
      log.info(s"Ignoring message '$msg', handler is not active.")
  }

  private def receiveOnStandBy: Receive = {
    case DeactivateTaskHandler =>
      context.become(receiveOnSleep)
      log.info("TaskHandler deactivated.")
    case msg =>
      log.info("Processing message '$msg'...")
  }
}

case object PushState
case object PopState
class StackedTaskHandler extends Actor with ActorLogging {
  def receive: Receive = defaultReceive

  private def defaultReceive: Receive = {
    case PushState =>
      context.become(specialReceive, discardOld = false)
      log.info("Received PushState: specialReceive behavior pushed to the top.")
    case msg =>
      log.info(s"Received '$msg' in default state.")
  }

  private def specialReceive: Receive = {
    case PopState =>
      log.info("Received PopState: taking the top behavior from the top of the stack...")
      context.unbecome
    case msg =>
      log.info(s"Received '$msg' in special state.")
  }
}

object ChangingBehaviors extends App {
  private val actorSystem = ActorSystem("ChangingBehaviorTest")

  private def runTaskHandler = {
    val taskHandler = actorSystem.actorOf(Props[TaskHandler])

    taskHandler ! "???"
    taskHandler ! ActivateTaskHandler
    taskHandler ! "???"
    taskHandler ! DeactivateTaskHandler
    taskHandler ! "!!!"
  }

  private def runStackedTaskHandler = {
    val stackedTaskHandler = actorSystem.actorOf(Props[StackedTaskHandler])

    stackedTaskHandler ! "test1"
    stackedTaskHandler ! PushState
    stackedTaskHandler ! "test2"
    stackedTaskHandler ! PushState
    stackedTaskHandler ! "test3"
    stackedTaskHandler ! PopState
    stackedTaskHandler ! "test4"
    stackedTaskHandler ! PopState
    stackedTaskHandler ! "test5"
  }

  def run = {
    // runTaskHandler
    runStackedTaskHandler
  }

  run
}
