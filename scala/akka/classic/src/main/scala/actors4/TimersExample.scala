package actors4

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorSystem
import akka.actor.Props
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import akka.actor.Cancellable
import akka.actor.PoisonPill
import akka.actor.Timers

private object TimerExample {
  case object StartMsg
  case object ReminderMsg
  case object StopMsg

  class TimerActor extends Actor with ActorLogging with Timers {
    case object TimerKey
    timers.startSingleTimer(key = TimerKey, msg = StartMsg, timeout = 500.millis)

    def receive: Receive = {
      case StartMsg =>
        log.info("StartMsg")
        timers.startPeriodicTimer(key = TimerKey, msg = ReminderMsg, interval = 1.second)

      case ReminderMsg =>
        log.info("Reminder")

      case StopMsg => 
        log.info("StopMsg")
        context.stop(self)
    }
  }

  def run = {
    val as = ActorSystem("as")
    val actor = as.actorOf(Props[TimerActor])

    as.scheduler.scheduleOnce(delay = 5.seconds) {
      actor ! StopMsg
    }(as.dispatcher)
  }
}

object TimersExample extends App {

  private class TestActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case msg => log.info(s"Received $msg")
    }
  }

  private def showSingleAndRoutineScheduling = {// {{{
    val as = ActorSystem("as")
    val actor = as.actorOf(Props[TestActor])

    as.log.info("Scheduling operation for actor...")
    implicit val ec: ExecutionContext = as.dispatcher

    as.scheduler.scheduleOnce(delay = 1.second) {
      actor ! "sth"
    }

    as.log.info("Scheduling routine for actor...")
    val cancellableRoutine = as.scheduler.schedule(initialDelay = 1.second, interval = 2.seconds) {
      actor ! "request-from-routine"
    }

    as.scheduler.scheduleOnce(delay = 5.seconds) {
      as.log.info("Cancelling routine after 5 seconds...")
      cancellableRoutine.cancel()
      as.log.info("Routine cancelled.")
    }
  }// }}}

  private def showExample1 = {// {{{
    class SelfStoppingActor extends Actor with ActorLogging {
      import context.dispatcher

      override def postStop(): Unit = {
        log.info("Actor stopped.")
      }

      private def receiveWhenWaitingForNewMsg(routineOpt: Option[Cancellable]): Receive = {
        case msg =>
          routineOpt.foreach(_.cancel())
          log.info(s"Received $msg - waiting 1 second for new messages...")

          val newRoutine = context.system.scheduler.scheduleOnce(delay = 1.second) {
            log.info("1 second passed, I'm no longer waiting for messages - stopping myself...")
            context.stop(self)
          }

          context.become(receiveWhenWaitingForNewMsg(Some(newRoutine)))
      }

      override def receive: Receive = receiveWhenWaitingForNewMsg(routineOpt = None)
    }

    val as = ActorSystem("as")
    val actor = as.actorOf(Props[SelfStoppingActor])

    actor ! "test"

    import as.dispatcher
    as.scheduler.scheduleOnce(delay = 3.seconds) {
      actor ! "dead letter"
    }
  }// }}}

  // showSingleAndRoutineScheduling
  // showExample1
  TimerExample.run
}
