package actors4

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorSystem
import akka.actor.Props
import com.typesafe.config.Config
import akka.dispatch.UnboundedPriorityMailbox
import akka.dispatch.PriorityGenerator
import com.typesafe.config.ConfigFactory
import akka.actor.PoisonPill
import akka.dispatch.ControlMessage

object Mailboxes extends App {
  /* Mailboxes stores incoming messages in every actor */

  private val conf = """
  mailboxesDemo = {
    support-ticket-dispatcher {
      mailbox-type = "actors4.Mailboxes$CustomPrioritizedMailbox"
    }

    control-mailbox {
      mailbox-type = "akka.dispatch.UnboundedControlAwareMailbox"
    }
  }
  """

  class TestActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case msg =>
        log.info(s"Received $msg")
    }
  }

  class CustomPrioritizedMailbox(settings: ActorSystem.Settings, config: Config)
    extends UnboundedPriorityMailbox(PriorityGenerator {
        case msg: String if msg.startsWith("A") => 0
        case msg: String if msg.startsWith("B") => 1
        case msg: String if msg.startsWith("C") => 2
        case _ => 3
      }
    )

  private def getCfg = ConfigFactory.parseString(conf).getConfig("mailboxesDemo")

  private def showCustomMailboxExample = {
    val as = ActorSystem("as", getCfg)
    val actor = as.actorOf(Props[TestActor].withDispatcher("support-ticket-dispatcher"))

    actor ! PoisonPill // even poison pill will be postponed by our custom mailbox
    actor ! "A test1"
    actor ! "B test2"
    actor ! "C test3"
    actor ! "A test4"
  }

  private def showControlAwareMailboxExample = {

    case object TestMsg extends ControlMessage

    val as = ActorSystem("as", getCfg)
    val controlAwareActor = as.actorOf(Props[TestActor].withMailbox("control-mailbox"))

    controlAwareActor ! "Test1"
    controlAwareActor ! "Test2"
    controlAwareActor ! TestMsg // this control msg will be processed first by actor
  }

  // showCustomMailboxExample
  showControlAwareMailboxExample
}
