package actors4

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.Props
import akka.routing.Router
import akka.routing.RoundRobinRoutingLogic
import akka.routing.ActorRefRoutee
import akka.actor.Terminated
import akka.actor.ActorSystem
import akka.actor.typed.scaladsl.PoolRouter
import akka.routing.RoundRobinPool
import com.typesafe.config.Config
import akka.routing.FromConfig
import com.typesafe.config.ConfigFactory
import akka.routing.RoundRobinGroup
import akka.routing.Broadcast

class SubActor extends Actor with ActorLogging {
  override def receive: Receive = {
    case msg =>
      log.info(s"$msg")
  }
}

private object ManualRouterExample {
  class Main extends Actor {
    private def createNewSubActor(idx: Int) = {
      val subActor = context.actorOf(Props[SubActor], s"subActor$idx")
      context.watch(subActor)
      subActor
    }

    private val subRouteActors = (1 to 5).map { idx =>
      ActorRefRoutee(createNewSubActor(idx))
    }

    override def receive = {
      /* Available logics:
       * - RoundRobinRoutingLogic
       * - RandomRoutingLogic
       * - SmallestMailboxRoutingLogic
       * - BroadcastRoutingLogic
       * - ScatterGatherFirstRoutingLogic
       * - TailChoppingRoutingLogic
       * - ConsistentHashingRoutingLogic
       */
      receiveForRouter(Router(logic = RoundRobinRoutingLogic(), routees = subRouteActors))
    }

    private def receiveForRouter(router: Router): Receive = {
      case Terminated(ref) =>
        val newRouter = router.removeRoutee(ref)

        newRouter.addRoutee(createNewSubActor(1))

        context.become(receiveForRouter(newRouter))

      case msg =>
        router.route(msg, sender())
    }
  }

  def run = {
    val as = ActorSystem("as")
    val router = as.actorOf(Props[Main])

    (1 to 10).foreach { i =>
      router ! s"msg_$i"
    }
  }
}

private object PoolExample {
  private val conf = """
  routersDemo = {
    akka {
      actor.deployment = {
        /poolRouter2 {
          router = round-robin-pool
          nr-of-instances = 5
        }
      }
    }
  }
  """

  // Almost same impl as ManualExample router impl:
  def run = {
    val as = ActorSystem("as", ConfigFactory.parseString(conf).getConfig("routersDemo"))
    val poolRouter = as.actorOf(RoundRobinPool(5).props(Props[SubActor]), "poolRouter")

    (1 to 10).map { idx =>
      poolRouter ! s"msg_$idx"
    }

    // Creating router based on config:
    val poolRouterFromConfig = as.actorOf(FromConfig.props(Props[SubActor]), "poolRouter2")
    (1 to 10).map { idx =>
      poolRouterFromConfig ! s"msg_cfg_$idx"
    }
  }
}

object GroupRouterExample {
  private val conf = """
  groupDemo = {
    akka {
      actor.deployment = {
        /groupRouter {
          router = round-robin-group
          routees.paths = ["/user/a1", "/user/a2", "/user/a3", "/user/a4", "/user/a5"]
        }
      }
    }
  }
  """

  def run = {
    val as = ActorSystem("as", ConfigFactory.parseString(conf).getConfig("groupDemo"))
    val routeSubActors = (1 to 5).map(idx => as.actorOf(Props[SubActor], name = ("a" + idx.toString)))
    val paths = routeSubActors.map(_.path.toString)

    // Manual:
    val grpRouter = as.actorOf(RoundRobinGroup(paths).props(), "manualGroupRouter")
    (1 to 10).map { idx =>
      grpRouter ! s"msg_cfg_$idx"
    }

    // From config:
    val grpRouterFromCfg = as.actorOf(FromConfig.props(), "groupRouter2")
    (1 to 10).map { idx =>
      grpRouterFromCfg ! s"msg_cfg_$idx"
    }

    // Broadcasting messages (PoisonPill/Kill are not routed):
    grpRouterFromCfg ! Broadcast("sthToBeBroadcastedToEverySubActor")
  }
}

object Routers extends App {
  // ManualRouterExample.run
  // PoolExample.run
  GroupRouterExample.run
}
