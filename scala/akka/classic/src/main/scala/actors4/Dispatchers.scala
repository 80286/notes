package actors4

import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.Actor
import akka.actor.ActorLogging
import com.typesafe.config.ConfigFactory
import scala.util.Random
import scala.concurrent.Future
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.ExecutionContext

object Dispatchers extends App {
  /*
   * Dispatcher types:
   * - Dispatcher
   * - PinnedDispatcher (binds exactly one actor to one thread pool)
   * - CallingThreadDispatcher
   */
  private val cfg = """
  dispatchersDemo = {
    test-dispatcher = {
      type = Dispatcher
      executor = "thread-pool-executor"
      thread-pool-executor {
        fixed-pool-size = 1
      }
      throughput = 30
    }

    akka.actor.deployment = {
      /testactor {
        dispatcher = test-dispatcher
      }
    }
  }
  """

  class Counter extends Actor with ActorLogging {
    private def receiveForCounter(counter: Int): Receive = {
      case msg =>
        log.info(s"Actor ${self.path} received $msg (counter: $counter)")
        context.become(receiveForCounter(counter + 1))
    }

    override def receive: Receive =
      receiveForCounter(0)
  }

  private def run = {
    val as = ActorSystem("as", ConfigFactory.parseString(cfg).getConfig("dispatchersDemo"))
    val counters = (1 to 10).map { idx =>
      as.actorOf(Props[Counter].withDispatcher("test-dispatcher"), "counter" + idx.toString())
    }

    val random = new Random()
    (1 to 1000).map { idx =>
      counters(random.nextInt(counters.size)) ! idx
    }
    // According to custom dispatcher config (cfg):
    // - only 3 actors will be delegated to execute actions at one time
    // - 30 messages are processed for any actor before thread will move to process messages from another actor

    // Using exclusive config for actor in akka.actor.deployment section:
    val testActor = as.actorOf(Props[Counter], "testactor")


    class DbActor extends Actor with ActorLogging {
      private implicit val ec: ExecutionContext = context.dispatcher

      override def receive: Receive = {
        case msg => Future {
          Thread.sleep(5000)
          log.info(s"Done: $msg")
        }
      }
    }

    val dbActor = as.actorOf(Props[DbActor], "dbActor")
    val nonBlockingActor = as.actorOf(Props[Counter])

    (1 to 100).map { idx =>
      dbActor ! idx
      nonBlockingActor ! idx
    }
  }

  run
}
