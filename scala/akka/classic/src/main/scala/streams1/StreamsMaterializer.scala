package streams1

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Source, Sink}
import akka.NotUsed
import scala.concurrent.Future
import akka.stream.scaladsl.RunnableGraph
import akka.Done
import scala.util.Success
import scala.util.Failure
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Keep

case object StreamsMaterializer extends App {
  private implicit val actorSystem = ActorSystem("actorSystem")

  // Materializer is an object that takes care of allocation of right resources to the running stream
  private implicit val materializer = ActorMaterializer()

  import actorSystem.dispatcher

  def runFirstExample = {
    val source: Source[Int,NotUsed]           = Source(1 to 3)

    val printingSink: Sink[Any, Future[Done]] = Sink.foreach(println)
    val graph: RunnableGraph[NotUsed]         = source.to(printingSink)
    // Materialize graph by running the graph, implicit materializer from the scope will be used:
    val notUsedMaterializedValue: NotUsed     = graph.run()

    // Below sink will expose materialized value after sink has finished receiving elements:
    val reducingSink: Sink[Int, Future[Int]]  = Sink.reduce[Int]((a, b) => a + b)
    val sumFuture: Future[Int]                = source.runWith(reducingSink)
    sumFuture.onComplete {
      case Success(value) => println(s"Materialized value: $value")
      case Failure(exception) => println(exception)
    }

    // When the graph is constructed using via() & to() methods when we connect flow and sink
    // by default the leftmost materialized value is kept
    // for example:
    //              Source(1 to 3)                      // A: Source[Int, NotUsed]
    //                  .via(Flow[Int].map(x => x + 1)) // B: Source[Int, NotUsed]
    //                  .to(printingSink)               // C: RunnableGraph[NotUsed]
    // run() method will return materialized value used from (A) stage
  }

  def runChoosingMaterializedValueExample = {
    val source: Source[Int,NotUsed]  = Source(1 to 3)
    val flow: Flow[Int,Int,NotUsed]  = Flow[Int].map(x => x * 2)
    val sink: Sink[Int,Future[Done]] = Sink.foreach[Int](println)

    source.viaMat(flow)((sourceMat, flowMat) => flowMat)
    // above call is synonym for:
    source.viaMat(flow)(Keep.right) // Allowed Keep functions: None, Both, Left, Right

    // below graph will use materialized value used by sink used in the last toMat() method (sink):
    val runnableGraph: RunnableGraph[Future[Done]] = source.viaMat(flow)(Keep.right).toMat(sink)(Keep.right)

    val graph = source
      .viaMat(flow){
        (sourceMat, flowMat) =>
          println(s"1: viaMat(sourceMat=$sourceMat, flowMat=$flowMat) -> Forwarding materialized value from flow")
          flowMat
      }.toMat(sink){
        (sourceMat, sinkMat) =>
          println(s"2: toMat(sourceMat=$sourceMat, sinkMat=$sinkMat) -> Forwarding materialized value from sink")
          sinkMat
      }

    graph.run().onComplete {
      case Success(value) => println(s"Future materialized by this stream finished with Success: $value")
      case Failure(exception) => println(exception)
    }

    // Syntatic sugars:
    val sum: Future[Int]  = Source(1 to 3).runWith(Sink.reduce[Int](_ + _)) // Source(1 to 3).to(Sink.reduce)(Keep.right)
    val sum2: Future[Int] = Source(1 to 3).runReduce[Int](_ + _)

    // Another ways of running stream:
    val f: NotUsed                 = Sink.foreach[Int](println).runWith(Source(1 to 3))
    val g: (NotUsed, Future[Done]) = Flow[Int].map(x => x + 1).runWith(source, sink)
  }

  def runLastItemExtractor = {
    val sentences = Seq("a b c", "c d", "e")
    val source = Source(sentences)

    source.toMat(Sink.last)(Keep.right).run().map { lastItem => println(s"Last: $lastItem") }
    // or:
    source.runWith(Sink.head).map { firstItem => println(s"First: $firstItem") }
  }

  def runWordCounter = {
    val splitToWords: Flow[String, Seq[String], NotUsed] = Flow[String].map { sentenceStr =>
      val result = sentenceStr.split(" ").toSeq
      println(s"splitToWords('$sentenceStr') -> $result")
      result
    }

    val countItems: Flow[Seq[String], Int, NotUsed]      = Flow[Seq[String]].map { items =>
      val result = items.size
      println(s"countItems($items) -> $result")
      result
    }

    def runImpl1(source: Source[String, NotUsed]): Future[Int] = {
      println("runImpl1()")

      source
        .via(splitToWords)
        .via(countItems)
        .runReduce[Int](_ + _)
    }

    def runImpl2(source: Source[String, NotUsed]): Future[Int] = {
      println("runImpl2()")

      source
        .via(splitToWords)
        .via(countItems)
        .toMat(Sink.reduce[Int](_ + _))((countItemsMat, sinkReduceMat) => sinkReduceMat) // or simply use: Keep.right
        .run()
    }

    def runImpl3(source: Source[String, NotUsed]): Future[Int] = {
      println("runImpl3()")
      val wordCounterFunc = (wordCounter: Int, string: String) => wordCounter + string.split(" ").size

      val sink: Sink[String,Future[Int]] = Sink.fold[Int, String](0)(wordCounterFunc)

      source.toMat(sink)(Keep.right).run()
      // or:
      source.runWith(sink)
      // or:
      source.runFold(0)(wordCounterFunc)
    }

    val sentences = Seq("a b c", "c d", "e")
    val source = Source(sentences)

    runImpl1(source).map { result => println(s"Impl1(source = $sentences): $result") }
    runImpl2(source).map { result => println(s"Impl2(source = $sentences): $result") }
    runImpl3(source).map { result => println(s"Impl3(source = $sentences): $result") }

    source.toMat(Sink.last)(Keep.right).run().map { lastItem => println(s"Last: $lastItem") }
  }

  // runFirstExample
  // runChoosingMaterializedValueExample
  // runLastItemExtractor
  runWordCounter
}
