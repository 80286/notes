package streams1

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Source, Flow, Sink}
import akka.actor.Actor
import akka.actor.Props
import scala.concurrent.Await
import scala.concurrent.duration.Duration

object OperatorFusion extends App {
  implicit val actorSystem = ActorSystem("actorSystem")
  implicit val actorMaterializer = ActorMaterializer()

  private def logThreadMsg(prefix: String) = {
    println(s"$prefix: Thread: ${Thread.currentThread().getName()}")
  }

  private def runSimpleCase = {
    println("### 1: Simple case")
    val source = Source(1 to 10)
    val flowAdd = Flow[Int].map(x => x + 1)
    val flowMul = Flow[Int].map(x => x * 2)
    val sink = Sink.foreach[Int](x => logThreadMsg(s"1. sink: $x"))

    // By default below stream will be processed by the same single actor!
    // Akka stream components are fused (they run on the same actor)
    source.via(flowAdd).via(flowMul).to(sink).run()

    // and it will do sth close to the below code:
    class TestActor extends Actor {
      override def receive: Receive = {
        case x: Int =>
          val f = x + 1
          val g = f * 2
          logThreadMsg(s"TestActor.receive: $x -> $g")
      }
    }
    val testActor = actorSystem.actorOf(Props[TestActor])
    (1 to 10).foreach(testActor ! _)
  }

  private def runComplexCase = {
    println("### 2: Complex case")

    val flow1 = Flow[Int].map { x =>
      val result = x + 1
      logThreadMsg(s"2. flow1: $x -> $result")
      Thread.sleep(1000)
      result
    }
    val flow2 = Flow[Int].map { x => 
      val result = x * 2
      logThreadMsg(s"2. flow2: $x -> $result")
      Thread.sleep(1000)
      result
    }
    val source = Source(1 to 5)
    val sink = Sink.foreach[Int](x => logThreadMsg(s"2. sink: $x"))

    // source.via(flow1).via(flow2).to(sink).run()
    // Thread.sleep(2 * 5 * 1000)

    // Increasing throughput by using async on components:
    source
      .via(flow1).async
      .via(flow2).async
      .run()

    Thread.sleep(5000)
  }

  private def runOrderingExample = {
    // Below code will print: 1A 1B 1C 2A 2B 2C
    // Source(1 to 2)
    //   .map { x => println(s"${x}A"); x }
    //   .map { x => println(s"${x}B"); x }
    //   .map { x => println(s"${x}C"); x }
    //   .to(Sink.ignore)
    //   .run()

    // In below async version order will be preserved for every single item emitted to the stream:
    Source(1 to 2)
      .map { x => println(s"${x}A"); x }.async
      .map { x => println(s"${x}B"); x }.async
      .map { x => println(s"${x}C"); x }.async
      .to(Sink.ignore)
      .run()
  }

  // runSimpleCase
  // runComplexCase
  runOrderingExample
}
