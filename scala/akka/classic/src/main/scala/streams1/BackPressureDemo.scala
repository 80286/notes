package streams1

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Source, Sink, Flow}
import akka.stream.OverflowStrategy
import scala.concurrent.duration._

object BackPressureDemo extends App {
  implicit val actorSystem = ActorSystem("actorSystem")
  implicit val materializer = ActorMaterializer()

  // Elements flow as response to demand from source
  // (Consumers trigger the flow)

  private val slowSink = Sink.foreach[Int]{ x => 
    Thread.sleep(1000)
    println(s"slowSink: $x")
  }

  private def showNotBackpressureDemo = {
    val fastSource = Source(1 to 10)

    // That's not backpressure due to the fusing (processing components by the same actor)
    fastSource.to(slowSink).run()

    // Here is the case where backpressure occurs:
    fastSource.async.to(slowSink).run()
  }

  private def showBackpressureDemo = {
    val fastSource = Source(1 to 30)
    val flow = Flow[Int].map { x => 
      println(s"Incoming flow item: $x")
      x
    }

    // fastSource
    //   .via(flow).async
    //   .to(slowSink)
    //   .run()
    //
    // Thread.sleep(30000)

    // After running above commented code, at the beginning you will notice 16 messages of 'Incoming flow item: ...'
    // that's the default value used by akka stream buffer
    
    // Allowed overflow strategies: dropHead, dropNew, dropTail, dropBuffer, backpressure, fail
    val bufferedFlow = flow.buffer(5, OverflowStrategy.dropHead)
    fastSource.async
      .via(bufferedFlow).async
      .to(slowSink)
      .run()

    Thread.sleep(30000)
    // For above stream run()
    // first 16 sink print calls will come from fastSource buffer
    // next sink print calls from the middle will be dropped
    // except last 5 buffered by bufferedFlow

    // Throttling - allow to pass max 3 items in 3 seconds timespan:
    fastSource.throttle(3, 3.seconds)
  }

  // showNotBackpressureDemo
  showBackpressureDemo
}
