package streams1

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Source, Sink}
import akka.NotUsed
import akka.Done
import scala.concurrent.Future
import akka.stream.scaladsl.RunnableGraph
import scala.concurrent.Await
import scala.concurrent.duration.Duration

object StreamsDemo1 extends App {
  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val actorSystem = ActorSystem("actorSystem")
  implicit val materializer = ActorMaterializer()

  def basics = {
    val source: Source[Int,NotUsed]   = Source(1 to 5)
    val sink: Sink[Int,Future[Done]]  = Sink.foreach[Int](println)
    val graph: RunnableGraph[NotUsed] = source.to(sink)

    // will use implicit materializer:
    graph.run()

    val flow: Flow[Int,Int,NotUsed]          = Flow[Int].map(x => x + 1)
    // source.via(flow) creates another source:
    val sourceWithFlow: Source[Int, NotUsed] = source.via(flow)
    val flowWithSink = flow.to(sink)

    // 3 same ways of running streams:
    sourceWithFlow.to(sink).run()
    source.to(flowWithSink).run()
    source.via(flow).to(sink).run()

    // will throw exception, null cannot be passed to sources:
    // Source.single[String](null).to(Sink.foreach(println)).run()

    // Sources:
    val s1 = Source.single(0)
    val s2 = Source(Seq("a", "b"))
    val emptySource = Source.empty[String]
    // Infinite source:
    val infiniteSource = Source(Stream.from(1)) // notice: class Stream used here doesn't correspond to akka streams
    val s3 = Source.future(Future(10))

    // Sinks:
    val sink1 = Sink.ignore
    val sink2 = Sink.foreach[String](println)
    val sink3 = Sink.head[String] // takes head element and closes stream
    val sink4 = Sink.fold[Int, Int](0)((x, y) => x + y)

    // Flows:
    val flow1 = Flow[Int].map(x => x - 1)
    val flow2 = Flow[Int].take(5) // takes 5 items and then closes stream

    val mapSource1 = Source(1 to 3).map(x => x + 1)
    // is equal to:
    val mapSource2 = Source(1 to 3).via(Flow[Int].map(x => x + 1))
    mapSource1.runForeach(println) // is equal to: mapSource.to(Sink.foreach[Int](println)).run()
  }

  def basicExample = {
    val flowFilterEvenNumbers = Flow[Int].filter(x => x % 2 == 0)
    val flowTakeTwoNumbers = Flow[Int].take(2)
    val sink = Sink.foreach(println)

    Source(1 to 10)
      .via(flowFilterEvenNumbers)
      .via(flowTakeTwoNumbers)
      .to(sink)
      .run
  }

  basicExample
}
