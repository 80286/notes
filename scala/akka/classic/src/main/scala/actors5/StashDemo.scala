package actors5

import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.Stash

object StashDemo extends App {
  case object Open
  case object Close
  case object Read
  case class Write(data: String)

  class ResourceActor extends Actor with ActorLogging with Stash {
    private var innerData = ""

    override def receive: Receive = closed

    private def closed: Receive = {
      case Open =>
        log.info("Openning resource...")
        unstashAll()
        context.become(open)

      case other =>
        log.info(s"Stashing msg: $other (closed state doesn't handle this type of msg)")
        stash() // this msg will be handled on unstashAll() call
    }

    private def open: Receive = {
      case Read =>
        log.info(s"Reading... result: $innerData")

      case Write(data) =>
        log.info(s"Writing data '$data'...")
        innerData = data

      case Close =>
        log.info("Closing resource...")
        unstashAll()
        context.become(closed)

      case other =>
        log.info(s"Stashing msg: $other (closed state doesn't handle this type of msg)")
        stash()
    }
  }

  private def showStashExample = {
    val actorSystem = ActorSystem("as")
    val resource = actorSystem.actorOf(Props[ResourceActor])

    resource ! Read       // stash Read msg
    resource ! Open       // open, unstash and handle Read msg
    resource ! Open       // stash Open msg
    resource ! Write("x") // log sth
    resource ! Close      // close, unstash and handle Open msg
    resource ! Read       // log sth
  }

  showStashExample
}
