package streams2

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.GraphDSL
import akka.NotUsed
import akka.stream.scaladsl.Broadcast
import akka.stream.scaladsl.Zip
import akka.stream.ClosedShape
import akka.stream.scaladsl.Merge
import akka.stream.scaladsl.Balance
import scala.concurrent.duration._

object GraphsIntro extends App {
  implicit val actorSystem = ActorSystem("GraphsIntro")
  implicit val materializer = ActorMaterializer()

  private def runFirstExample = {
    val source = Source(1 to 10)

    val incrementer = Flow[Int].map(x => x + 1)
    val multiplier = Flow[Int].map(x => x * 2)

    val sink1 = Sink.foreach[(Int, Int)](t => println(s"Sink1: $t"))
    val sink2 = Sink.foreach[(Int, Int)](t => println(s"Sink2: $t"))

    /*
     *             ______________                    __________       __________________
     *            |             -|> -> [ flow ] -> -|>         |     |                -|> -> sink1
     * source -> -|> fan-out     |                  |  fan-in -|> -> | fan-out         |
     *            | (broadcast) -|> -> [ flow ] -> -|>  (zip)  |     |(sinkBroadcast) -|> -> sink2
     *            |______________|                  |__________|     |_________________|
     */
    val graph = RunnableGraph.fromGraph(
      GraphDSL.create() {
        // builder defined here is mutable structure until return of ClosedShape, from that moment it becomes immutable
        implicit builder: GraphDSL.Builder[NotUsed] =>
          import GraphDSL.Implicits._

          // fan-out operator used for items from source:
          // takes on input single Int and returns 2 values:
          val sourceBroadcast = builder.add(Broadcast[Int](2))

          // fan-out operator used for items forwarded to the output sink:
          // takes on input single tuple (int, int) and returns 2 values:
          val sinkBroadcast = builder.add(Broadcast[(Int, Int)](2))

          // fan-in operator:
          val zip = builder.add(Zip[Int, Int])

          source ~> sourceBroadcast

          sourceBroadcast.out(0) ~> incrementer ~> zip.in0
          sourceBroadcast.out(1) ~> multiplier ~> zip.in1

          zip.out ~> sinkBroadcast

          // Same tuple value will be forwarded to 2 different sinks:
          sinkBroadcast.outlet ~> sink1
          sinkBroadcast.outlet ~> sink2

          // Return shape & freeze builder:
          ClosedShape
      }
    )

    graph.run()
  }

  private def runSecondExample = {
    val fastSource = Source[Int](1 to 100).throttle(5, 1.seconds)
    val slowSource = Source[Int](100 to 200).throttle(2, 1.seconds)

    val sink1 = Sink.fold[Int, Int](0){ (count, _) => 
      println(s"Sink1: num of items: $count")
      count + 1
    }
    val sink2 = Sink.fold[Int, Int](0){ (count, _) =>
      println(s"Sink2: num of items: $count")
      count + 1
    }

    /*
     *                 ________        __________
     * fastSource -> -|>      -|> -> -|>        -|> -> sink1
     *                | merge  |      | balance  |
     * slowSource -> -|>      -|> -> -|>        -|> -> sink2
     *                |________|      |__________|
     */
    val graph = RunnableGraph.fromGraph(GraphDSL.create() {
      implicit builder =>
        import GraphDSL.Implicits._

        val merge = builder.add(Merge[Int](2))
        val balance = builder.add(Balance[Int](2))

        fastSource ~> merge ~> balance ~> sink1
        slowSource ~> merge;   balance ~> sink2

        ClosedShape
    })

    graph.run()
  }

  // runFirstExample
  runSecondExample
}
