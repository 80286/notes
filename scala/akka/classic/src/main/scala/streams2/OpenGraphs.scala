package streams2

import akka.stream.scaladsl.{Source, Flow, Sink, RunnableGraph, GraphDSL, Broadcast, Zip, Merge, Balance, Concat}
import akka.stream.{ActorMaterializer, FlowShape, SourceShape, SinkShape, ClosedShape}
import akka.actor.ActorSystem
import akka.NotUsed
import scala.concurrent.duration._
import akka.stream.FlowShape

object OpenGraphs extends App {
  implicit val actorSystem = ActorSystem("GraphsIntro")
  implicit val materializer = ActorMaterializer()

  private def runComplexSourceGraphEx = {
    val source1 = Source(1 to 10)
    val source2 = Source(20 to 40)

    // Create complex source (graph):
    val sourceGraph = Source.fromGraph(
      GraphDSL.create() { implicit builder =>
        import GraphDSL.Implicits._

        // Declaring graph components:
        val concat = builder.add(Concat[Int](2))

        // Linking sources with graph component:
        source1 ~> concat
        source2 ~> concat

        SourceShape(concat.out)
      }
    )

    sourceGraph.to(Sink.foreach(println)).run()
  }

  private def runComplexSinkGraphEx = {
    val sink1 = Sink.foreach[Int](x => println(s"Sink1: $x"))
    val sink2 = Sink.foreach[Int](x => println(s"Sink2: $x"))

    // Complex sink that forwards items to two sinks:
    val sinkGraph = Sink.fromGraph(
      GraphDSL.create() { implicit builder => 
        import GraphDSL.Implicits._

        // Define 2 outputs for broadcast component:
        val broadcast = builder.add(Broadcast[Int](2))

        broadcast ~> sink1
        broadcast ~> sink2

        SinkShape(broadcast.in)
      }
    )

    Source(1 to 10).to(sinkGraph).run()
  }

  private def runComplexFlowGraphEx = {

    val flow1 = Flow[Int].map(x => x + 1)
    val flow2 = Flow[Int].map(x => x * 2)

    val flowGraph = Flow.fromGraph(
      GraphDSL.create() { implicit builder => 
        import GraphDSL.Implicits._

        val flow1Shape = builder.add(flow1)
        val flow2Shape = builder.add(flow2)

        flow1Shape ~> flow2Shape

        FlowShape(flow1Shape.in, flow2Shape.out)
      }
    )

    Source(1 to 5).via(flowGraph).to(Sink.foreach(println)).run()
  }

  // runComplexSourceGraphEx
  // runComplexSinkGraphEx
  runComplexFlowGraphEx
}
