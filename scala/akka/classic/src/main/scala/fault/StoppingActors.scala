package fault

import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.PoisonPill
import akka.actor.Kill
import akka.actor.Terminated

object StoppingActors extends App {
  private implicit val actorSystem = ActorSystem("as")

  object Parent {
    case class StartSubActor(name: String)
    case class StopSubActor(name: String)
    case object Stop
  }
  class Parent extends Actor with ActorLogging {
    import Parent._

    override def receive: Receive = withSubActors(Map.empty)

    private def withSubActors(subActors: Map[String, ActorRef]): Receive = {
      case StartSubActor(name) =>
        val subActor = context.actorOf(Props[SubActor], name)
        context.become(withSubActors(subActors + (name -> subActor)))
        log.info(s"Started child actor '$name' ($subActor)")

      case StopSubActor(name) =>
        log.warning(s"Trying to stop subActor '$name'...")
        subActors.get(name).foreach { subActorToStop =>
          context.stop(subActorToStop)
          log.info(s"SubActor '$name' stopped.")
        }

      case Stop =>
        log.info("Stopping.")
        context.stop(self) // will stop all child actor first and then will finally stop self actor

      case msg =>
        log.info(s"Received other msg: ${msg.toString}")
    }
  }

  class SubActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case msg => log.info(msg.toString)
    }
  }

  private def runAndShowStoppingUsingContextStop = {
    import Parent._

    val parent = actorSystem.actorOf(Props[Parent], "parent")
    parent ! StartSubActor("a")

    val subActor1 = actorSystem.actorSelection("/user/parent/a")
    subActor1 ! "echo"

    parent ! StopSubActor("a")
    // If you try to send here for example 20 messages to the stopped actor ref
    // you will notice that few first messages will be still processed correctly

    parent ! StartSubActor("b")
    val subActor2 = actorSystem.actorSelection("/user/parent/b")

    parent ! Stop
  }

  private def runAndShowStoppingUsingMsgRequest = {
    import Parent._
    val parent = actorSystem.actorOf(Props[Parent], "parent")
    parent ! StartSubActor("a")
    parent ! PoisonPill
    parent ! "hello ???"

    val actor = actorSystem.actorOf(Props[SubActor])
    actor ! Kill
    actor ! "are u ok??"
  }

  private def runAndShowDeathWatchExample = {
    import Parent._

    class WatcherActor extends Actor with ActorLogging {
      def receive: Receive = {
        case StartSubActor(name) =>
          val subActor = context.actorOf(Props[SubActor], name)
          context.watch(subActor)

        case Terminated(terminatedActorRef) =>
          log.info(s"Received terminated msg: $terminatedActorRef")
      }
    }

    val watcher = actorSystem.actorOf(Props[WatcherActor], "watcher")
    watcher ! StartSubActor("1")

    val watchedActorRef = actorSystem.actorSelection("/user/watcher/1")

    Thread.sleep(500)
    watchedActorRef ! PoisonPill
  }

  // runAndShowStoppingUsingContextStop
  // runAndShowStoppingUsingMsgRequest
  runAndShowDeathWatchExample
}
