package actors1

import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.ActorLogging

case class PrintInternalVar(s: String)
case class IncrementInternalVar(step: Int)
case object GetValueOfInternalVar
case class GetValueOfInternalVarResponse(value: Int)
case class Forward(msg: String)

class TestActor extends Actor with ActorLogging {
  private var value = 0
  private def printMsg(msg: String) = {
    log.info(s"$self: $msg")
  }

  // 'Receive' is alias to: PartialFunction[Any, Unit]
  override def receive: Receive = {
    case msg: PrintInternalVar =>
      printMsg(s"Received $msg from: {$sender()}")
      printMsg(s"Actual value: $msg")

    case msg: IncrementInternalVar =>
      val step = msg.step
      printMsg(s"Received $msg from: {$sender()}")
      printMsg(s"Incrementing internal value: $value + $step -> ${value + step}")
      value += step

    case GetValueOfInternalVar =>
      printMsg(s"Received ${GetValueOfInternalVar} from: {$sender()}")
      sender() ! GetValueOfInternalVarResponse(value)

    case msg: Forward =>
      printMsg(s"Received $msg from ${sender()} - forwarding msg with original sender...")
      sender() forward msg

    case msg =>
      printMsg(s"Received unknown message: $msg from ${sender()}")
  }
}

object TestActor {
  def apply(name: String)(implicit actorSystem: ActorSystem): ActorRef = {
    actorSystem.actorOf(Props[TestActor], name)
  }
}

class TestActor2(a: ActorRef) extends Actor with ActorLogging {
  log.info(s"${self}: TestActor2 created. Sending ${GetValueOfInternalVar} to $a...")
  a ! GetValueOfInternalVar
  a ! Forward("test")

  override def receive: Receive = {
    case msg: GetValueOfInternalVarResponse =>
      log.info(s"${self}: Received: $msg from ${sender()}") 

    case msg =>
      log.info(s"${self}: Received unknown message $msg from ${sender}")
  }
}

object Actors1 {
  def run = {
    implicit val actorSystem = ActorSystem("akkaClassicTest")

    val actor1 = TestActor("actor1")

    actor1 ! PrintInternalVar("test-msg")
    actor1 ! IncrementInternalVar(1)
    actor1 ! "test"

    // Valid way of passing parameters into actor:
    val actor2 = actorSystem.actorOf(Props(new TestActor2(actor1)), "actor2")
  }
}
