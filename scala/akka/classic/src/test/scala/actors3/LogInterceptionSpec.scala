package actors3

import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike
import scala.util.Random
import scala.concurrent.duration._
import akka.actor.{Actor, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import akka.testkit.EventFilter
import akka.actor.ActorLogging
import com.typesafe.config.ConfigFactory

class LogInterceptionSpec
  extends TestKit(ActorSystem("as", ConfigFactory.parseString(LogInterceptionSpec.configStr).getConfig("interceptorLog")))
  with AnyWordSpecLike
  with ImplicitSender
  with BeforeAndAfterAll {
    override protected def afterAll(): Unit = {
      TestKit.shutdownActorSystem(system)
    }

    "LogInterceptionTestActor" must {
      val actor = system.actorOf(Props[LogInterceptionSpec.LogInterceptionTestActor])

      "catch log message defined by regex pattern" in {
        EventFilter.info(pattern = "FirstActor returned response", occurrences = 1).intercept {
          actor ! "init"
        }
      }

      "catch exception" in {
        EventFilter[RuntimeException](occurrences = 1).intercept {
          actor ! "amen"
        }
      }
    }
}

object LogInterceptionSpec {
  private val configStr = """
  interceptorLog.akka.loggers = ["akka.testkit.TestEventListener"]
  """

  class LogInterceptionTestActor extends Actor {
    private val firstActor = context.system.actorOf(Props[FirstActor])

    override def receive: Receive = {
      case "init" =>
        firstActor ! FirstActorDoAction("test")

      case "amen" =>
        throw new RuntimeException("kaszel")

      case FirstActorActionResponse =>
        ()
    }
  }

  private case class FirstActorDoAction(msg: String)
  private case object FirstActorActionResponse
  private class FirstActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case FirstActorDoAction(msg) =>
        log.info("FirstActor returned response")
        sender() ! FirstActorActionResponse
    }
  }
}
