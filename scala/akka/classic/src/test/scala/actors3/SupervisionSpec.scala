package actors3

import akka.testkit.TestKit
import akka.actor.ActorSystem
import akka.testkit.ImplicitSender
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike
import akka.actor.Actor
import akka.actor.Props
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy
import akka.testkit.TestProbe
import akka.actor.ActorRef
import akka.actor.Terminated
import akka.actor.AllForOneStrategy
import akka.testkit.EventFilter

/*
 * Supervision
 *
 * Default strategy when actor throws exception is to: restart actor.
 *
 * We can set custom strategy by overriding supervisorStrategy method in actor.
 */

class SupervisionSpec extends TestKit(ActorSystem("as"))
  with ImplicitSender
  with AnyWordSpecLike
  with BeforeAndAfterAll {

  import SupervisionSpec._

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  private def getSupervisorAndStrProcActor = {
    val supervisor = system.actorOf(Props[Supervisor])

    supervisor ! Props[StrProcessor]
    val strProcActor = expectMsgType[ActorRef]

    (supervisor, strProcActor)
  }

  "Supervisor" should {
    "resume its child in case of runtimeException" in {
      val (supervisor, strProcActor) = getSupervisorAndStrProcActor

      strProcActor ! "Polmo"
      strProcActor ! GetReport
      expectMsg(1)

      val tooLongStr = "x" * 11
      strProcActor ! tooLongStr

      strProcActor ! GetReport
      // Report should return previously returned value because state has been kept
      // after RuntimeException due to the resume strategy defined for this type of exception:
      expectMsg(1)
    }

    "restart its child in case of NPE" in {
      val (supervisor, strProcActor) = getSupervisorAndStrProcActor

      strProcActor ! "Polmo"
      strProcActor ! GetReport
      expectMsg(1)

      // will throw npe and restart actor:
      strProcActor ! ""

      strProcActor ! GetReport
      // and should restart state of actor:
      expectMsg(0)
    }

    "stop its child in case of IllegalArgumentException" in {
      val (supervisor, strProcActor) = getSupervisorAndStrProcActor

      watch(strProcActor)

      strProcActor ! "P"
      strProcActor ! GetReport
      expectMsg(1)

      strProcActor ! "polmo"
      assert(expectMsgType[Terminated].actor == strProcActor)
    }

    "escalate in case of Exception" in {
      val (supervisor, strProcActor) = getSupervisorAndStrProcActor

      watch(strProcActor)

      strProcActor ! 1
      assert(expectMsgType[Terminated].actor == strProcActor)
    }
  }

  "NoDeathOnRestartSupervisor" should {
    "not kill child actors in case of escalation" in {
      val supervisor = system.actorOf(Props[NoDeathOnRestartSupervisor], "supervisor")

      supervisor ! Props[StrProcessor]
      val strProcActor = expectMsgType[ActorRef]

      strProcActor ! "Test"
      strProcActor ! GetReport
      expectMsg(1)

      // Trigger escalation:
      strProcActor ! 1
      strProcActor ! GetReport
      expectMsg(0)
    }
  }

  "AllForOneSupervisor" should {
    "use all for one strategy" in {
      val supervisor = system.actorOf(Props[AllForOneSupervisor], "a41supervisor")

      supervisor ! Props[StrProcessor]
      val strProcActor1 = expectMsgType[ActorRef]

      supervisor ! Props[StrProcessor]
      val strProcActor2 = expectMsgType[ActorRef]

      strProcActor2 ! "Test"
      strProcActor2 ! GetReport
      expectMsg(1)

      EventFilter[NullPointerException]().intercept {
        strProcActor1 ! ""
      }

      Thread.sleep(1000)

      strProcActor2 ! GetReport
      expectMsg(0)
    }
  }
}

object SupervisionSpec {

  class Supervisor extends Actor {
    protected val decider: SupervisorStrategy.Decider = {
      case _: NullPointerException     => SupervisorStrategy.Restart
      case _: IllegalArgumentException => SupervisorStrategy.Stop
      case _: RuntimeException         => SupervisorStrategy.Resume
      case _: Exception                => SupervisorStrategy.Escalate
    }

    override def supervisorStrategy: SupervisorStrategy =
      OneForOneStrategy()(decider)

    override def receive: Receive = {
      case props: Props =>
        sender() ! context.actorOf(props)
    }
  }

  class NoDeathOnRestartSupervisor extends Supervisor {
    // override non empty default impl that stops every child:
    override def preRestart(reason: Throwable, message: Option[Any]): Unit = {}
  }

  class AllForOneSupervisor extends Supervisor {
    override def supervisorStrategy: SupervisorStrategy =
      AllForOneStrategy()(decider)
  }

  object GetReport

  class StrProcessor extends Actor {
    private var validStrings = 0

    override def receive: Receive = {
      case GetReport =>
        sender() ! validStrings

      case "" =>
        throw new NullPointerException("Received empty str")

      case str: String =>
        if(str.length > 10)
          throw new RuntimeException("Received too long str")
        else if(!str.charAt(0).isUpper)
          throw new IllegalArgumentException("Must start with uppercase letter")
        else
          validStrings += 1

      case v =>
        throw new Exception(s"Unexpected msg: $v")
    }
  }
}
