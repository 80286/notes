package actors3

import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike
import scala.util.Random
import scala.concurrent.duration._
import akka.actor.{Actor, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}

class TimeAssertionsSpec
  extends TestKit(ActorSystem("as"))
  with AnyWordSpecLike
  with ImplicitSender
  with BeforeAndAfterAll {
    override protected def afterAll(): Unit = {
      TestKit.shutdownActorSystem(system)
    }

    "DelayedActor" must {
      val actor = system.actorOf(Props[TimeAssertionsSpec.DelayedActor])

      "pass timebox test for 'start' request" in {
        within(500.millis, 1.second) {
          actor ! "start"
          expectMsg("started")
        }
      }

      "pass start2 test" in {
        actor ! "start2"

        val results: Seq[String] = receiveWhile[String](max = 2.seconds, idle = 500.millis, messages = 5) {
          case response: String => response
        }

        assert(results.size == 5)
      }
    }
}

object TimeAssertionsSpec {
  class DelayedActor extends Actor {
    private val random = new Random()

    override def receive: Receive = {
      case "start" =>
        Thread.sleep(500)
        sender() ! "started"

      case "start2" =>
        (1 to 5).foreach { idx =>
          Thread.sleep(random.nextInt(20))
          sender() ! s"msg-$idx"
        }
    }
  }
}
