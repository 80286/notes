package actors3

import akka.actor.ActorSystem
import akka.testkit.TestKit
import akka.testkit.ImplicitSender
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.BeforeAndAfterAll
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.testkit.TestProbe

class TestProbeSpec extends TestKit(ActorSystem("as")) with ImplicitSender with AnyWordSpecLike with BeforeAndAfterAll {
  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  import TestProbeSpec._
  "MainActor" should {
    val mainActor = system.actorOf(Props[MainActor])

    "work" in {
      val subActor = TestProbe("subActor")

      mainActor ! RegisterSubActor(subActor.ref)
      expectMsg(RegisterSubActorDone)

      mainActor ! DoWork("a")
      mainActor ! DoWork("b")

      subActor.receiveWhile() {
        case SubActorDoWork(msg, replyTo) =>
          subActor.reply(SubActorWorkFinished(2, replyTo))
      }

      expectMsg(ReportFinishedSubActorWork(2))
      expectMsg(ReportFinishedSubActorWork(4))
    }
  }
}

object TestProbeSpec {
  case class RegisterSubActor(actorRef: ActorRef)
  case object RegisterSubActorDone

  case class DoWork(msg: String)
  case class SubActorDoWork(msg: String, replyTo: ActorRef)
  case class SubActorWorkFinished(counter: Int, replyTo: ActorRef)
  case class ReportFinishedSubActorWork(result: Int)

  class MainActor extends Actor {
    override def receive: Receive = {
      case RegisterSubActor(actorRef) =>
        sender() ! RegisterSubActorDone
        context.become(receiveForRegisteredSubActor(actorRef, 0))
    }

    private def receiveForRegisteredSubActor(subActor: ActorRef, total: Int): Receive = {
      case DoWork(msg) =>
        subActor ! SubActorDoWork(msg, replyTo = sender())

      case SubActorWorkFinished(counter, replyTo) =>
        val sum = total + counter
        replyTo ! ReportFinishedSubActorWork(sum)
        context.become(receiveForRegisteredSubActor(subActor, sum))
    }
  }
}
