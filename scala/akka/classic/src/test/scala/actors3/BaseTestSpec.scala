package actors3

import akka.testkit.TestKit
import akka.testkit.ImplicitSender
import akka.actor.ActorSystem
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.BeforeAndAfterAll
import akka.actor.Actor
import akka.actor.Props
import scala.concurrent.duration._
import scala.util.Random

class BaseTestSpec
  extends TestKit(ActorSystem("testActorSystem"))
  with ImplicitSender
  with AnyWordSpecLike
  with BeforeAndAfterAll {
    override protected def afterAll(): Unit = {
      TestKit.shutdownActorSystem(system)
    }

    import BaseTestSpec._
    "TestActor" should {
      "respond with same msg" in {
        val actor = system.actorOf(Props[TestActor])
        val msg = "test"

        // Responses here are sent to member testActor (because it's defined as sender in ImplicitSender)
        actor ! msg
        expectMsg(msg)
      }
    }

    "DevNullActor" should {
      "not respond with any message" in {
        val actor = system.actorOf(Props[DevNullActor])

        actor ! "test"
        expectNoMessage(1.second)
      }
    }

    "VariousActionsActor" should {
      val actor = system.actorOf(Props[VariousActionsActor])

      "convert input string to uppercase" in {
        actor ! "abc"

        val response = expectMsgType[String]
        assert(response == "ABC")
      }

      "respond a or b" in {
        actor ! "getAorB"
        expectMsgAnyOf("a", "b")
      }

      "respond a then b" in {
        actor ! "get2msgs"
        expectMsgAllOf("a", "b")

        // or:
        actor ! "get2msgs"
        val messages = receiveN(2)

        // or:
        actor ! "get2msgs"
        expectMsgPF() {
          case "a" => 
          case "b" =>
        }
      }
    }
}

object BaseTestSpec {
  class TestActor extends Actor {
    override def receive: Receive = {
      case msg => sender() ! msg
    }
  }

  class DevNullActor extends Actor {
    override def receive: Receive = Actor.emptyBehavior
  }

  class VariousActionsActor extends Actor {
    private val random = new Random()
    override def receive: Receive = {
      case "getAorB" =>
        sender() ! (if(random.nextBoolean()) "a" else "b")

      case "get2msgs" =>
        sender() ! "a"
        sender() ! "b"

      case msg: String =>
        sender() ! msg.toUpperCase()
    }
  }
}
