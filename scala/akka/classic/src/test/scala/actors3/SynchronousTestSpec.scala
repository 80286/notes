package actors3

import org.scalatest.BeforeAndAfterAll
import akka.actor.ActorSystem
import org.scalatest.wordspec.AnyWordSpecLike
import akka.actor.Actor
import akka.testkit.TestActorRef
import akka.actor.Props
import akka.testkit.CallingThreadDispatcher
import akka.testkit.TestProbe
import scala.concurrent.duration.Duration

class SynchronousTestSpec extends AnyWordSpecLike with BeforeAndAfterAll {
  implicit val actorSystem = ActorSystem("as")

  override def afterAll() = {
    actorSystem.terminate()
  }

  import SynchronousTestSpec._

  "CounterActor" should {
    "increase counter without parallel execution" in {
      // TestActorRef guarantees execution on single thread:
      val counter = TestActorRef[CounterActor](Props[CounterActor])

      counter ! Inc
      assert(counter.underlyingActor.v == 1)

      counter.receive(Inc)
      assert(counter.underlyingActor.v == 2)
    }

    "return correct initial value of counter" in {
      // Below test will fail without pointing CallingThreadDispatcher (becuse it will be launched asynchronously with default dispatcher)
      val actor = actorSystem.actorOf(Props[CounterActor].withDispatcher(CallingThreadDispatcher.Id))
      val probe = TestProbe()

      probe.send(actor, Get)
      probe.expectMsg(max = Duration.Zero, 0) // probe already received Get request
    }
  }
}

object SynchronousTestSpec {
  case object Inc
  case object Get
  class CounterActor extends Actor {
    var v = 0

    override def receive: Receive = {
      case Inc => v += 1
      case Get => sender() ! v
    }
  }
}
