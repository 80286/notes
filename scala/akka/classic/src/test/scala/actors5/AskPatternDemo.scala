package actors5

import org.scalatest.BeforeAndAfterAll
import akka.testkit.TestKit
import akka.actor.ActorSystem
import akka.testkit.ImplicitSender
import org.scalatest.wordspec.AnyWordSpecLike
import akka.actor.ActorLogging
import akka.actor.Actor
import akka.actor.Props
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import scala.concurrent.duration._
import scala.util.Success
import scala.util.Failure
import scala.reflect.ClassTag

class AskPatternDemo extends TestKit(ActorSystem("as"))
  with ImplicitSender
  with AnyWordSpecLike
  with BeforeAndAfterAll {
    import AskPatternDemo._

    override protected def afterAll(): Unit = {
      TestKit.shutdownActorSystem(system)
    }

    private def doAuthMgrTest(props: Props) = {
      "fail" when {
        "trying to authenticate non defined user" in {
          val authMgr = system.actorOf(props)

          authMgr ! Authenticate("unknownUser", "password")
          expectMsg(AuthFailure(s"Unable to find user: unknownUser"))
        }

        "wrong password is used" in {
          val authMgr = system.actorOf(props)

          authMgr ! RegisterUser("u", "password")

          authMgr ! Authenticate("u", "wrongpassword")
          expectMsg(AuthFailure("Password incorrect"))
        }
      }
    }

    "AuthManager" should {
      doAuthMgrTest(Props[AuthManager])
    }

    "PipedAuthManager" should {
      doAuthMgrTest(Props[PipedAuthManager])
    }
}

object AskPatternDemo {
  case class Read(key: String)
  case class Write(key: String, value: String)
  class KeyValueActor extends Actor with ActorLogging {
    override def receive: Receive =
      online(Map.empty)

    private def online(data: Map[String, String]): Receive = {
      case Read(key) =>
        log.info(s"Reading key: $key")
        sender() ! data.get(key)

      case Write(key, value) =>
        log.info(s"Writing $value for $key")
        context.become(online(data + (key -> value)))
    }
  }

  case class Authenticate(user: String, pass: String)
  case class RegisterUser(user: String, pass: String)

  case class AuthFailure(msg: String)
  case object AuthSuccess

  class AuthManager extends Actor with ActorLogging {
    protected val authDb = context.actorOf(Props[KeyValueActor])
    protected implicit val timeout: Timeout = Timeout(1.second)
    protected implicit val ec = context.dispatcher

    protected def doAuthentication(user: String, pass: String) = {
      val future = authDb ? Read(user)

      // Important: you should NOT use actor methods (sender()) inside Future.onComplete
      // because these are called on different thread and may cause race conditions
      // (avoid closing over actor instance or mutable state)
      val originalSender = sender()

      future.onComplete {
        case Success(Some(userPassword)) =>
          if(userPassword == pass)
            originalSender ! AuthSuccess
          else
            originalSender ! AuthFailure(s"Password incorrect")

        case Success(None) =>
          originalSender ! AuthFailure(s"Unable to find user: $user")

        case _ =>
          originalSender ! AuthFailure(s"unknown error")
      }
    }

    override def receive: Receive = {
      case RegisterUser(user, pass) =>
        authDb ! Write(user, pass)

      case Authenticate(user, pass) =>
        doAuthentication(user, pass)
    }
  }

  class PipedAuthManager extends AuthManager {
    override protected def doAuthentication(user: String, pass: String): Unit = {
      val future = authDb ? Read(user)
      val passFuture = future.mapTo[Option[String]]
      val responseFuture = passFuture.map {
        case None =>
          AuthFailure(s"Unable to find user: $user")

        case Some(dbPassword) =>
          if(pass == dbPassword)
            AuthSuccess
          else
            AuthFailure("Password incorrect")
      }

      // when the future completes, send response to sender()
      responseFuture.pipeTo(sender())
    }
  }
}
