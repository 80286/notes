package actors5

import akka.testkit.TestKit
import akka.actor.ActorSystem
import akka.testkit.ImplicitSender
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.BeforeAndAfterAll
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.Cancellable
import akka.actor.ActorRef
import scala.concurrent.duration._
import akka.actor.Props
import akka.actor.FSM
import org.scalatest.OneInstancePerTest

class FsmSpec extends TestKit(ActorSystem("as"))
  with ImplicitSender
  with AnyWordSpecLike
  with BeforeAndAfterAll
  with OneInstancePerTest { // without this trait tests will be failing for both implementations of VM !
  import FsmSpec._

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  private def runTests(getVm: => ActorRef) = {
    "fail when it's not initialized" in {
      val vm = getVm

      vm ! RequestProduct("ocetix")
      expectMsg(VendingError("Not initialized"))
    }

    "fail when product is not present in the store" in {
      val vm = getVm

      vm ! Initialize(Map("ocet" -> 3), Map("ocet" -> 1))
      vm ! RequestProduct("poznanskie")
      expectMsg(VendingError("Product not found"))
    }

    "throw timeout if enough money were not inserted" in {
      val vm = getVm

      vm ! Initialize(Map("ocet" -> 3), Map("ocet" -> 1))
      vm ! RequestProduct("ocet")
      expectMsg(Instruction("Please insert 1 dollars"))

      within((1.5).seconds) {
        expectMsg(VendingError("Timeout"))
      }
    }

    "handle partial money case" in {
      val vm = getVm

      vm ! Initialize(Map("ocet" -> 6), Map("ocet"-> 3))
      vm ! RequestProduct("ocet")
      expectMsg(Instruction("Please insert 3 dollars"))

      vm ! ReceiveMoney(1)
      expectMsg(Instruction("Insert remaining 2"))

      within((1.5).seconds) {
        expectMsg(VendingError("Timeout"))
        expectMsg(GiveBackChange(1))
      }
    }

    "deliver product if enough money inserted" in {
      val vm = getVm

      vm ! Initialize(Map("ocet" -> 6), Map("ocet"-> 3))
      vm ! RequestProduct("ocet")
      expectMsg(Instruction("Please insert 3 dollars"))

      vm ! ReceiveMoney(3)
      expectMsg(Deliver("ocet"))
    }

    "give back change and be able to get next product" in {
      val vm = getVm

      vm ! Initialize(Map("ocet" -> 6), Map("ocet"-> 3))
      vm ! RequestProduct("ocet")
      expectMsg(Instruction("Please insert 3 dollars"))

      vm ! ReceiveMoney(4)
      expectMsg(Deliver("ocet"))
      expectMsg(GiveBackChange(1))

      vm ! RequestProduct("ocet")
      expectMsg(Instruction("Please insert 3 dollars"))
    }
  }

  "VendingMachine" should {
    runTests(system.actorOf(Props[VendingMachine]))
  }

  "VendingMachineFSM" should {
    runTests(system.actorOf(Props[VendingMachineFsm]))
  }
}

object FsmSpec {

  case class Initialize(
    inventory: Map[String, Int],
    prices: Map[String, Int]
  )
  case class RequestProduct(product: String)
  case class Instruction(instruction: String)
  case class ReceiveMoney(amount: Int)
  case class Deliver(product: String)
  case class GiveBackChange(amount: Int)
  case class VendingError(reason: String)
  case object ReceiveMoneyTimeout

  class VendingMachine extends Actor with ActorLogging {// {{{
    private implicit val ec = context.dispatcher

    override def receive: Receive = idle

    private def idle: Receive = {
      case Initialize(inventory, prices) =>
        context.become(operational(inventory, prices))

      case _ =>
        sender() ! VendingError("Not initialized")
    }

    private def operational(inventory: Map[String, Int], prices: Map[String, Int]): Receive = {
      case RequestProduct(product) =>
        inventory.get(product) match {
          case None | Some(0) =>
            sender() ! VendingError("Product not found")

          case Some(value) =>
            val price = prices(product)
            sender() ! Instruction(s"Please insert $price dollars")
            context.become(waitingForMoney(inventory, prices, product, 0, startMoneyTimeoutScheduler, sender()))
        }
    }

    private def startMoneyTimeoutScheduler =
      context.system.scheduler.scheduleOnce(1.second) {
        self ! ReceiveMoneyTimeout
      }

    private def waitingForMoney(
      inventory: Map[String, Int],
      prices: Map[String, Int],
      product: String,
      moneyReceivedSoFar: Int,
      moneyTimeoutScheduler: Cancellable,
      replyTo: ActorRef): Receive = {

      case ReceiveMoneyTimeout =>
        replyTo ! VendingError("Timeout")

        if(moneyReceivedSoFar > 0)
          replyTo ! GiveBackChange(moneyReceivedSoFar)

        context.become(operational(inventory, prices))

      case ReceiveMoney(amount) =>
        moneyTimeoutScheduler.cancel()

        val price = prices(product)

        if(moneyReceivedSoFar + amount >= price) {
          replyTo ! Deliver(product)

          if(moneyReceivedSoFar + amount - price > 0)
            replyTo ! GiveBackChange(moneyReceivedSoFar + amount - price)

          val newInventoryAmount = inventory(product) - 1
          val newInventory = inventory + (product -> newInventoryAmount)

          context.become(operational(newInventory, prices))
        } else {
          val remaining = price - moneyReceivedSoFar - amount
          replyTo ! Instruction(s"Insert remaining $remaining")
          context.become(waitingForMoney(inventory, prices, product, moneyReceivedSoFar + amount, startMoneyTimeoutScheduler, replyTo))
        }
    }
  }// }}}

  // ----- FSM version:
  trait VendingState
  case object Idle extends VendingState
  case object Operational extends VendingState
  case object WaitingForMoney extends VendingState

  trait VendingData
  case object Uninitialized extends VendingData
  case class Initialized(inventory: Map[String, Int], prices: Map[String, Int]) extends VendingData
  case class WaitingForMoneyData(
      inventory: Map[String, Int],
      prices: Map[String, Int],
      product: String,
      moneyReceivedSoFar: Int,
      replyTo: ActorRef
  ) extends VendingData

  class VendingMachineFsm extends FSM[VendingState, VendingData] {
    // there is no receive handler
    // when FSM receives msg => it triggers an event with (message, data)
    
    startWith(Idle, Uninitialized)

    when(Idle) {
      case Event(Initialize(inventory, prices), Uninitialized) =>
        goto(Operational) using Initialized(inventory, prices)

      case _ =>
        sender() ! VendingError("Not initialized")
        stay()
    }

    when(Operational) {
      case Event(RequestProduct(product), Initialized(inventory, prices)) =>
        inventory.get(product) match {
          case None | Some(0) =>
            sender() ! VendingError("Product not found")
            stay()

          case Some(value) =>
            val price = prices(product)
            sender() ! Instruction(s"Please insert $price dollars")
            goto(WaitingForMoney) using WaitingForMoneyData(inventory, prices, product, 0, sender())
        }
    }

    when(WaitingForMoney, stateTimeout = 1.second) {
      case Event(StateTimeout, WaitingForMoneyData(inventory, prices, product, moneyReceivedSoFar, replyTo)) =>
        replyTo ! VendingError("Timeout")

        if(moneyReceivedSoFar > 0)
          replyTo ! GiveBackChange(moneyReceivedSoFar)

        goto(Operational) using Initialized(inventory, prices)

      case Event(ReceiveMoney(amount), WaitingForMoneyData(inventory, prices, product, moneyReceivedSoFar, replyTo)) =>
        val price = prices(product)

        if(moneyReceivedSoFar + amount >= price) {
          replyTo ! Deliver(product)

          if(moneyReceivedSoFar + amount - price > 0)
            replyTo ! GiveBackChange(moneyReceivedSoFar + amount - price)

          val newInventoryAmount = inventory(product) - 1
          val newInventory = inventory + (product -> newInventoryAmount)

          goto(Operational) using Initialized(newInventory, prices)
        } else {
          val remaining = price - moneyReceivedSoFar - amount
          replyTo ! Instruction(s"Insert remaining $remaining")

          stay() using WaitingForMoneyData(inventory, prices, product, moneyReceivedSoFar + amount, replyTo)
        }
    }

    whenUnhandled {
      case Event(_, _) =>
        sender() ! VendingError("???")
        stay()
    }

    onTransition {
      case stateA -> stateB =>
        log.info(s"Transitioning from $stateA to $stateB")
    }

    // FSM actor will not start without initialize() call !
    initialize()
  }
}
