scalaVersion := "2.13.8"
name := "classic"
organization := "ch.epfl.scala"
version := "1.0"

val akkaVersion = "2.6.19"
val slf4jVersion = "1.7.5"
val scalaTestVersion = "3.2.12"

libraryDependencies += "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion
libraryDependencies += "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test
libraryDependencies ++= Seq("org.slf4j" % "slf4j-api" % slf4jVersion,
                            "org.slf4j" % "slf4j-simple" % slf4jVersion)
libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.12"
libraryDependencies += "org.scalatest" %% "scalatest" % scalaTestVersion % "test"

// Streams
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "org.scalatest" %% "scalatest" % scalaTestVersion
)
