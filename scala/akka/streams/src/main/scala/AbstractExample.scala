import akka.actor.ActorSystem
import akka.stream._


abstract class AbstractExample(actorSystemName: String) {
  protected val system = ActorSystem(s"$actorSystemName-ActorSystem")
  protected implicit val materializer = ActorMaterializer.create(system)

  protected def printMsg(msg: String) = {
    println(s"[thread: ${Thread.currentThread().getName}]: $msg")
  }

  def run(): Unit
}
