import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl._
import akka.NotUsed

// https://akka.io/blog/article/2016/07/06/threading-and-concurrency-in-akka-streams-explained
object Main extends App {
  private def main = {
    Seq(
      // new Example1,
      // new Example2,
      new Example3,
    ).foreach(_.run())
  }

  main
}