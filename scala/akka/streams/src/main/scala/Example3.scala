import akka.stream._
import akka.stream.scaladsl._
import akka.NotUsed
import scala.concurrent.Future
import akka.Done

class Example3 extends AbstractExample("E3") {
  implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

  private def processingStageFlow(name: String): Flow[String, String, NotUsed] = {
    Flow[String].map {
      item => {
        val msg = s"processingStageFlow[$name]; Processing '$item': Flow thread"
        printMsg(s"$msg started")
        Thread.sleep(100)
        printMsg(s"$msg finished")

        s"[$item]"
      }
    }
  }

  override def run() = {
    val prefix = "Example3"

    printMsg(s"$prefix: start")

    val source = Source(List("item1", "item2", "item3"))

    val streamFutureObj: Future[Done] = source
      .via(processingStageFlow("A")).async
      .via(processingStageFlow("B")).async
      .via(processingStageFlow("C")).async
      .runWith(Sink.foreach(obj => printMsg(s"$prefix: Sink.foreach: output: obj=$obj")))

    streamFutureObj.onComplete(_ => {
      printMsg(s"$prefix: runWith() Future obj has finished.")
      printMsg(s"$prefix: terminating ActorSystem...")
      system.terminate()
    })

    printMsg(s"~run()")
    Thread.sleep(1000)
  }
}
