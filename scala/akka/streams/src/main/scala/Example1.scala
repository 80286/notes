import akka.stream._
import akka.stream.scaladsl._
import akka.NotUsed

class Example1 extends AbstractExample("E1") {
  private def processSource[T](source: Source[T, NotUsed]) = {
    source
      .map(obj => {
        val result = s"[$obj]"
        printMsg(s"map() 1: '$obj' -> '$result'")
        
        result
      })
      .map(obj => {
        val result = s"[$obj]"
        printMsg(s"map() 2: '$obj' -> '$result'")

        result
      })
      .to(Sink.foreach(s => {
        printMsg(s"Sink.foreach: to(s=$s)")
      }))
      .run()
  }

  override def run() = {
    val prefix = "Example1"

    printMsg(s"$prefix: start")

    processSource(Source.single("test"))

    val delayMs = 1000
    printMsg(s"$prefix: running... Sleeping for $delayMs ms")
    // Put a pause before terminate() call:
    Thread.sleep(delayMs)

    printMsg("$prefix: Terminating ActorSystem...")
    system.terminate()
  }
}
