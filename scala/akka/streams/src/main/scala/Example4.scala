// import akka.stream.CompletionStrategy
// import akka.stream.scaladsl.Sink
// import akka.stream.typed.scaladsl.ActorSource
// import akka.actor.typed.ActorRef

// case object Emitted
// sealed trait Event

// case class Element(content: String) extends Event
// case object ReachedEnd extends Event
// case class FailureOccured(ex: Exception) extends Event

// object StreamFeeder {
//   def apply(): Behavior[Emitted.type] = {
//     Behavior.setup {
//       context => {
//         val streamActor = runStream(context.self)(context.system)
//       }
//     }
//   }

//   private def runStream(ackReceiver: ActorRef[Emitted.type])(implicit system: ActorSystem[_]): ActorRef[Event] = {
//   }
// }

// class Example4 extends AbstractExample("E4") {
//   implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

//   override def run() = {

//   }
// }


import akka.actor.typed.ActorRef
import akka.stream.CompletionStrategy
import akka.stream.scaladsl.Sink
import akka.stream.typed.scaladsl.ActorSource

object StreamFeeder {

  /** Signals that the latest element is emitted into the stream */
  case object Emitted

  sealed trait Event
  case class Element(content: String) extends Event
  case object ReachedEnd extends Event
  case class FailureOccured(ex: Exception) extends Event

  def apply(): Behavior[Emitted.type] =
    Behaviors.setup { context =>
      val streamActor = runStream(context.self)(context.system)
      streamActor ! Element("first")
      sender(streamActor, 0)
    }

  private def runStream(ackReceiver: ActorRef[Emitted.type])(implicit system: ActorSystem[_]): ActorRef[Event] = {
    val source =
      ActorSource.actorRefWithBackpressure[Event, Emitted.type](
        // get demand signalled to this actor receiving Ack
        ackTo = ackReceiver,
        ackMessage = Emitted,
        // complete when we send ReachedEnd
        completionMatcher = {
          case ReachedEnd => CompletionStrategy.draining
        },
        failureMatcher = {
          case FailureOccured(ex) => ex
        })

    val streamActor: ActorRef[Event] = source
      .collect {
        case Element(msg) => msg
      }
      .to(Sink.foreach(println))
      .run()

    streamActor
  }

  private def sender(streamSource: ActorRef[Event], counter: Int): Behavior[Emitted.type] =
    Behaviors.receiveMessage {
      case Emitted if counter < 5 =>
        streamSource ! Element(counter.toString)
        sender(streamSource, counter + 1)
      case _ =>
        streamSource ! ReachedEnd
        Behaviors.stopped
    }
}

// ActorSystem(StreamFeeder(), "stream-feeder")
