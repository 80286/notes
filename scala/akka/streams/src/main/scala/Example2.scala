import akka.stream._
import akka.stream.scaladsl._
import akka.NotUsed
import scala.concurrent.Future
import akka.Done

class Example2 extends AbstractExample("E2") {
  implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

  override def run() = {
    val prefix = "Example2"

    printMsg(s"$prefix: start")

    val source = Source.single("a")

    val streamFutureObj: Future[Done] = source
      .map(obj => {
        val result = s"[$obj]"
        printMsg(s"map() 1: '$obj' -> '$result'")
        
        result
      })
      .map(obj => {
        val result = s"[$obj]"
        printMsg(s"map() 2: '$obj' -> '$result'")

        result
      })
      .runWith(Sink.foreach(obj => printMsg(s"runWith: Sink.foreach: $obj")))

    streamFutureObj.onComplete(_ => {
      printMsg(s"$prefix: runWith() Future obj has finished.")
      printMsg(s"$prefix: terminating ActorSystem...")
      system.terminate()
    })

    printMsg(s"~run()")
  }
}
