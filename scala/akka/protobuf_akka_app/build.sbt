scalaVersion := "2.13.16"

// val AkkaHttpVersion = "10.7.0"
// val AkkaVersion = "2.10.0"

val library = new {
  object Version {
    val akkaHttp = "10.2.9"
    val akka     = "2.6.19"
    val akkaHttpVersion = "10.7.0"
    val json4s = "4.0.6"
    val jsoniterScala = "2.17.9"
    val scalaPbJson4s = "0.11.1"
  }

  val akka = "com.typesafe.akka" %% "akka-actor" % Version.akka
  val akkaStream = "com.typesafe.akka" %% "akka-stream" % Version.akka
  val akkaHttp = "com.typesafe.akka" %% "akka-http" % Version.akkaHttp

  val json4sCore = "org.json4s" %% "json4s-core" % Version.json4s
  val json4sJackson = "org.json4s" %% "json4s-jackson" % Version.json4s
  val json4sNative = "org.json4s" %% "json4s-native" % Version.json4s
  val jsoniterScalaCore = "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-core" % Version.jsoniterScala
  val jsoniterScalaMacros = "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-macros" % Version.jsoniterScala

  val scalaPbJson4s = "com.thesamet.scalapb" %% "scalapb-json4s" % Version.scalaPbJson4s
}

lazy val protobuf = project
  .in(file("modules/protobuf"))
  .settings(
    name         := "protobuf",
    organization := "org.zx",
    Compile / PB.targets := Seq(
      scalapb.gen() -> (Compile / sourceManaged).value
    ),
  )

lazy val app1 = project.in(file("modules/app1"))
  .dependsOn(protobuf)
  .settings(
    name         := "app1",
    organization := "org.zx",
    libraryDependencies ++= Seq(
      library.akka,
      library.akkaStream,
      library.akkaHttp,
      library.json4sCore,
      library.json4sNative,
      library.jsoniterScalaCore,
      library.jsoniterScalaMacros,
      library.scalaPbJson4s,
    )
  )

lazy val app2 = project.in(file("modules/app2"))
  .dependsOn(protobuf)
  .settings(
    name         := "app2",
    organization := "org.zx"
  )

lazy val root = project.in(file("."))
  .aggregate(protobuf, app1, app2)
  .settings(
    name         := "protobuf-akka-app",
    organization := "org.zx",
  )
