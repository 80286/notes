package org.zx

import org.zx.proto.test.{Device, DeviceSpec}
import scalapb.json4s.JsonFormat

import scala.io.StdIn
import scala.concurrent.{ExecutionContextExecutor, Future}
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route.seal
import akka.http.scaladsl.coding.Coders
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.{Directives, Route}
import akka.http.scaladsl.unmarshalling.Unmarshal
import scalapb.{GeneratedMessage, GeneratedMessageCompanion}
import scalapb.json4s.JsonFormat

import scala.io.StdIn
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller
import com.github.plokhotnyuk.jsoniter_scala.macros._
import com.github.plokhotnyuk.jsoniter_scala.core._
import org.json4s.JsonAST._
import org.zx.de.heikoseeberger.akkahttpjsoniterscala.JsoniterScalaSupport

trait DeviceCodecs {
  private def getCodec[T <: GeneratedMessage: GeneratedMessageCompanion] = new JsonValueCodec[T] {
    override def decodeValue(in: JsonReader, default: T): T = {
      val j = in.readString("")
      val out = JsonFormat.fromJsonString(j)
      println(s"Decode: $j -> $out")
      out
    }

    override def encodeValue(obj: T, out: JsonWriter): Unit = {
      val s = JsonFormat.toJsonString(obj)
      val o = out.writeVal(s)
      println(s"Encode: $s -> $o")
      o
    }

    override def nullValue: T = null.asInstanceOf[T]
  }

  implicit val deviceCodec: JsonValueCodec[Device] = getCodec[Device]
  implicit val deviceSpecCodec: JsonValueCodec[DeviceSpec] = getCodec[DeviceSpec]
}

trait DeviceFacade {
  def getDevice(id: String): Future[Device] =
    Future.successful(Device(id, Some("test")))

  def getDevices: Future[Seq[Device]] =
    Future.successful(Seq.empty)

  def upsertDevice(id: String, spec: DeviceSpec): Future[Unit] =
    Future.unit
}

object Main extends App with DeviceFacade with DeviceCodecs {
  private implicit val actorSystem: ActorSystem = ActorSystem("system")
  private implicit val executionCtx: ExecutionContextExecutor = actorSystem.dispatcher

  import Directives._
  import JsoniterScalaSupport._
  import com.github.plokhotnyuk.jsoniter_scala.core._
  import com.github.plokhotnyuk.jsoniter_scala.macros._

  private val route =
    pathPrefix("test" / Segment) { id =>
      complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, s"<h1>akka-http: id: $id</h1>"))
    } ~ pathPrefix("devices" / Segment) { deviceId => 
      get {
        complete(getDevice(deviceId))
      } ~ put {
        entity(as[DeviceSpec]) { deviceSpec =>
          complete(upsertDevice(deviceId, deviceSpec).map { _ => StatusCodes.OK })
        }
      }
    }

  private def startHttpServer(): Unit = {
    val (host, port) = ("localhost", 8080)

    println(s"Starting HTTP server on $host:$port...")
    val serverBinding: Future[Http.ServerBinding] = Http()
      .newServerAt("localhost", 8080)
      .bind(route)

    try {
      println("Type <Ctrl-C> to terminate")
      StdIn.readLine()
    } catch {
      case e: Throwable => ()
    }

    println("Terminating HTTP server....")
    serverBinding
      .flatMap(_.unbind())
      .onComplete(_ => actorSystem.terminate())
  }

  startHttpServer()
}
