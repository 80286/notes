package persistence2

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory

object LocalStores extends App {
  private def run = {
    // Goto application.conf to see how to bind configs for any persistent-actor-id:
    val as = ActorSystem("LocalStoresDemo", ConfigFactory.load().getConfig("localStoreDemo"))
    val actor = as.actorOf(Props[TestPersistentActor])

    (1 to 10).foreach { idx =>
      actor ! s"msg-$idx"
    }

    actor ! "print"
    actor ! "snap"

    (11 to 20).foreach { idx =>
      actor ! s"msg-$idx"
    }
  }

  run
}
