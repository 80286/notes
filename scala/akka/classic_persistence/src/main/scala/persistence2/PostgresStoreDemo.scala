package persistence2

import akka.actor.{ActorLogging, ActorSystem, Props}
import akka.persistence.{PersistentActor, SnapshotOffer, SaveSnapshotSuccess, SaveSnapshotFailure, RecoveryCompleted}
import com.typesafe.config.ConfigFactory

object PostgresStoreDemo extends App {
  private def run = {
    // Goto application.conf to see how to bind configs for any persistent-actor-id:
    val as = ActorSystem("postgresStoreDemo", ConfigFactory.load().getConfig("postgresStoreDemo"))
    val actor = as.actorOf(Props[TestPersistentActor])

    (1 to 10).foreach { idx =>
      actor ! s"msg-$idx"
    }

    actor ! "print"
    actor ! "snap"

    (11 to 20).foreach { idx =>
      actor ! s"msg-$idx"
    }

    /* After calling run() use:
     * > ./psql.sh
     *
     * to see contents of db tables:
     *
     * > select * from public.journal;
     * > select * from public.snapshot;
     */
  }

  run
}
