package persistence2

import akka.actor.{ActorLogging, ActorSystem, Props}
import akka.persistence.{PersistentActor, SnapshotOffer, SaveSnapshotSuccess, SaveSnapshotFailure, RecoveryCompleted}
import com.typesafe.config.ConfigFactory

class TestPersistentActor extends PersistentActor with ActorLogging {
  private var num = 0

  override def persistenceId: String = "test-persistent-actor-id"

  override def receiveRecover: PartialFunction[Any,Unit] = {
    case SnapshotOffer(metadata, data: Int) =>
      log.info(s"Recovered SNAPSHOT: $data")
      num = data

    case msg: SaveSnapshotSuccess =>
      log.info("Received: $msg")

    case msg: SaveSnapshotFailure =>
      log.info("Received: $msg")

    case RecoveryCompleted =>
      log.info("Recovery completed.")

    case msg =>
      log.info(s"Recovered EVENT: $msg")
      num += 1
  }

  override def receiveCommand: PartialFunction[Any,Unit] = {
    case "print" =>
      log.info(s"Num of received commands: $num")

    case "snap" =>
      saveSnapshot(num)

    case msg =>
      persist(msg) { ev =>
        log.info(s"Persisted: $ev")
        num += 1
      }
  }
}
