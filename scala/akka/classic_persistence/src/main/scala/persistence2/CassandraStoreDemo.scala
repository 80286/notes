package persistence2

import akka.actor.{ActorLogging, ActorSystem, Props}
import akka.persistence.{PersistentActor, SnapshotOffer, SaveSnapshotSuccess, SaveSnapshotFailure, RecoveryCompleted}
import com.typesafe.config.ConfigFactory

object CassandraStoreDemo extends App {
  private def run = {
    // Goto application.conf to see how to bind configs for any persistent-actor-id:
    val as = ActorSystem("cassandraStoreDemo", ConfigFactory.load().getConfig("cassandraStoreDemo"))
    val actor = as.actorOf(Props[TestPersistentActor])

    (1 to 10).foreach { idx =>
      actor ! s"msg-$idx"
    }

    actor ! "print"
    actor ! "snap"

    (11 to 20).foreach { idx =>
      actor ! s"msg-$idx"
    }

    /* Cassandra plugin will automaticly create all required tables.
     * There is no need to call init script like in postgresql case.
     *
     * After calling run() use:
     * $ ./csql.sh
     *
     * to check data saved in cassandra db:
     *
     * > select * from akka.messages;
     * > select * from akka_snapshot.snapshots;
     */
  }

  run
}
