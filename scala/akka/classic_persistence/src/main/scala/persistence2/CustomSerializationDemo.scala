package persistence2

import akka.actor.ActorLogging
import akka.persistence.PersistentActor
import akka.serialization.Serializer
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.actor.Props


case class UserSpec(email: String, login: String)
case class RegisterUser(userSpec: UserSpec)

case class UserRegisteredEvent(id: Int, userSpec: UserSpec)

class UserRegistrationSerializer extends Serializer {
  override def identifier: Int = 1001
  override def includeManifest: Boolean = false

  override def toBinary(obj: AnyRef): Array[Byte] = {
    obj match {
      case event @ UserRegisteredEvent(id, userSpec) =>
        println(s"Serializing event: $event")
        s"[$id/${userSpec.email}/${userSpec.login}]".getBytes()

      case unknown =>
        throw new IllegalArgumentException(s"Unknown type of msg: $unknown")
    }
  }

  override def fromBinary(bytes: Array[Byte], manifest: Option[Class[_]]): AnyRef = {
    val str = new String(bytes)
    val values = str.substring(1, str.length - 1).split("/")

    val (id, email, login) = (values(0).toInt, values(1), values(2))
    val deserializedEvent = UserRegisteredEvent(id, UserSpec(email, login))

    println(s"Deserialized '$str' -> $deserializedEvent")
    deserializedEvent
  }


}

class UserRegisterActor extends PersistentActor with ActorLogging {
  private var lastId = 0

  override def persistenceId: String = "user-register-actor-id"

  override def receiveRecover: PartialFunction[Any,Unit] = {
    case event @ UserRegisteredEvent(id, spec) =>
      log.info(s"Recovered $event")
      lastId = lastId
  }

  override def receiveCommand: PartialFunction[Any,Unit] = {
    case RegisterUser(spec) =>
      persist(UserRegisteredEvent(lastId, spec)) { ev =>
        log.info(s"Persisted $ev")
        lastId += 1
      }
  }
}

object CustomSerializationDemo extends App {
  private def run = {
    // 1. Actor calls persist() on persisted event
    // 2. event is serialized using serializer
    // 3. Journal saves data using raw bytes produced by serializer
    //
    // Deserialization is called on recovery

    val as = ActorSystem("CustomSerializationDemo", ConfigFactory.load().getConfig("customSerializerDemo"))
    val actor = as.actorOf(Props[UserRegisterActor])

    (1 to 10).foreach { idx =>
      actor ! RegisterUser(UserSpec(s"user_$idx@polmo.net", s"user_$idx"))
    }
  }

  run
}
