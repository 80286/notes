package persistence3

import akka.actor.ActorSystem
import akka.persistence.PersistentActor
import akka.actor.ActorLogging
import scala.collection.mutable
import akka.actor.Props
import com.typesafe.config.ConfigFactory
import akka.persistence.journal.ReadEventAdapter
import akka.persistence.journal.EventSeq

object EventAdapters extends App {
  /* Demo shows how to :
   * deal with schema progression of events already saved in recovery store.
   * */

  // First, initial of object:
  case class SomeObject(id: String)
  // Second, new format of object:
  case class SomeObjectV2(id: String, objType: String)

  // Commands:
  case class AddObjects(user: SomeObjectV2, num: Int)

  // Events:
  case class ObjectAdded(user: SomeObject, num: Int)
  case class ObjectAddedV2(user: SomeObjectV2, num: Int)

  class ObjectEventAdapter extends ReadEventAdapter {
   /* Flow: journal (bytes) -> serializer (obj) -> read event adapter (objv2) -> actor */

   def fromJournal(event: Any, manifest: String): EventSeq = {
     event match {
       case ObjectAdded(obj, num) =>
         EventSeq.single(ObjectAddedV2(SomeObjectV2(obj.id, objType = "old-format"), num))

       case other =>
         EventSeq.single(other)
     }
   }
  }

  class InventoryManager extends PersistentActor with ActorLogging {
    private val objInventory = new mutable.HashMap[SomeObjectV2, Int];

    override def persistenceId: String = "user-manager-id"

    override def receiveCommand: Receive = {
      case AddObjects(obj, numOfObjects) =>
        persist(ObjectAddedV2(obj, numOfObjects)) { ev =>
          updateInventory(obj, numOfObjects)
          log.info(s"Added $numOfObjects of $obj")
        }

      case "print" =>
        objInventory.foreach { case (k, v) =>
          log.info(s"$k -> $v")
        }
    }

    override def receiveRecover: Receive = {
      // Below code is not needed while our custom EventAdapter is correctly configured in application.conf
      // case ObjectAdded(obj, numOfObjects) => // recover events from first version for compatibility with old version:
      //   updateInventory(SomeObjectV2(obj.id, "old-format"), numOfObjects)
      //   log.info(s"Recovered $numOfObjects of $obj")

      case ObjectAddedV2(obj, numOfObjects) => // recover events from current version:
        updateInventory(obj, numOfObjects)
        log.info(s"Recovered $numOfObjects of $obj")
    }

    private def updateInventory(obj: SomeObjectV2, numOfObjects: Int) = 
      objInventory.put(obj, objInventory.getOrElse(obj, 0) + numOfObjects)
  }

  private def main = {
    val as = ActorSystem("EventAdapters", ConfigFactory.load().getConfig("eventAdapters"))
    val mgr = as.actorOf(Props[InventoryManager])

    (1 to 10).foreach { idx => 
      mgr ! AddObjects(SomeObjectV2(s"obj-$idx", "new"), 1)
    }

    mgr ! "print"
  }

  main
}
