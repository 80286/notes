package persistence3

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.persistence.query.PersistenceQuery
import akka.persistence.cassandra.query.scaladsl.CassandraReadJournal
import akka.stream.Materializer
import akka.stream.ActorMaterializer
import akka.NotUsed
import akka.stream.scaladsl.Source
import akka.persistence.PersistentActor
import akka.actor.ActorLogging
import akka.actor.Props
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import akka.persistence.journal.WriteEventAdapter
import akka.persistence.journal.Tagged
import akka.persistence.query.Offset

object PersistenceQueryDemo extends App {
  private def run = {
    val as = ActorSystem("PersistenceQueryDemo", ConfigFactory.load().getConfig("persistenceQueryDemo"))

    val readJournal = PersistenceQuery(as).readJournalFor[CassandraReadJournal](CassandraReadJournal.Identifier)

    // 1) Getting all ids of all persistent actors stored so far in cassandra journal:
    val persistenceIds: Source[String, NotUsed] = readJournal.persistenceIds()

    implicit val mat: Materializer = ActorMaterializer()(as)
    persistenceIds.runForeach { persistenceId =>
      println(s"PersistenceId: $persistenceId")
    }

    sendSthToNewPersistentActor(as)
    // After that above runForeach() callback will print id of new persistent actor (because it uses infinite stream)

    // 2) Selecting events by persistence id:
    val persistentActorId = "simple-persistent-actor-for-query-testing-1"
    val events = readJournal.eventsByPersistenceId(persistentActorId, 0, Long.MaxValue)
    events.runForeach { event =>
      println(s"$persistentActorId event found in journal: $event")
    }

    // 3) Querying events by tags:
    createPersistentActorWithTags(as)

    val eventsForGenre1 = readJournal.eventsByTag("genre1", Offset.noOffset)
    eventsForGenre1.runForeach { event =>
      println(s"Found events for genre1 in journal: $event")
    }
  }

  private def sendSthToNewPersistentActor(as: ActorSystem) = {
    val spActor = as.actorOf(Props[SimplePersistentActor])

    import as.dispatcher
    as.scheduler.scheduleOnce(Duration(5, TimeUnit.SECONDS)) {
      spActor ! "force-persisting-this-msg-for-new-actor"
    }
  }

  private def createPersistentActorWithTags(as: ActorSystem) = {
    import MusicStore._

    val store = as.actorOf(Props[MusicStore], "musicStoreActor")

    store ! BuySongs(Seq(Song(0, "genre1"), Song(1, "genre2")))
  }

  run
}

class SimplePersistentActor extends PersistentActor with ActorLogging {
  override def persistenceId: String = "simple-persistent-actor-for-query-testing-1"
  override def receiveCommand: PartialFunction[Any,Unit] = {
    case command =>
      persist(command) { _ =>
        log.info(s"Command: $command")
      }
  }
  override def receiveRecover: PartialFunction[Any,Unit] = {
    case event => log.info(s"Recovered: $event")
  }
}

object MusicStore {
  val genres = Set("genre1", "genre2")

  case class Song(id: Int, genre: String)
  // MusicStore Commands:
  case class BuySongs(songs: Seq[Song])
  // MusicStore Events:
  case class SongsPurchased(id: Int, songs: Seq[Song])
}

class MusicStore extends PersistentActor with ActorLogging {
  import MusicStore._

  private var lastPurchaseId = 0
  override def persistenceId: String = "music-store"

  override def receiveCommand: PartialFunction[Any, Unit] = {
    case BuySongs(songs) =>
      persist(SongsPurchased(lastPurchaseId, songs)) { event =>
        log.info(s"Songs purchased: $event")
        lastPurchaseId += 1
      }
  }

  override def receiveRecover: PartialFunction[Any, Unit] = {
    case event @ SongsPurchased(id, _) =>
      log.info(s"Recovered: $event")
      lastPurchaseId = id
  }
}

class MusicStoreEventAdapter extends WriteEventAdapter {
  import MusicStore._

  override def manifest(event: Any): String = "musicStore"
  override def toJournal(event: Any): Any = event match {
    case SongsPurchased(id, songs) =>
      val genres = songs.map(_.genre).toSet

      // Below wrapper will set tags in cassandra record for this event:
      Tagged(event, tags = genres)

    case event => event
  }
}
