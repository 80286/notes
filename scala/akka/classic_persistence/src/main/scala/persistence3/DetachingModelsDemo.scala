package persistence3

import akka.persistence.PersistentActor
import akka.actor.ActorLogging
import akka.actor.ActorSystem
import akka.actor.Props
import scala.collection.mutable
import akka.persistence.journal.EventAdapter
import akka.persistence.journal.EventSeq
import com.typesafe.config.ConfigFactory

object DomainModel {
  case class User(id: String, email: String, userNameNewField: String)
  case class Coupon(code: String, promoAmount: Int)

  // Commands:
  case class ApplyCoupon(coupon: Coupon, user: User)

  // Events:
  case class CouponApplied(code: String, user: User)
}

object DataModel {
  case class WrittenCouponApplied(code: String, userId: String, userEmail: String)
  case class WrittenCouponAppliedV2(code: String, userId: String, userEmail: String, userNameNewField: String)
}

/* Thanks to below adapter we can define which data model will be stored
 * in our journal (WrittenCouponApplied)
 *
 * Purpose: separation between data & domain model helps in evolution of schema
 */
class ModelAdapter extends EventAdapter {
  import DomainModel._
  import DataModel._

  override def manifest(event: Any): String = ""

  /* Flow: journal -> serializer -> fromJournal -> persistent actor (receiveRecover) */
  override def fromJournal(event: Any, manifest: String): EventSeq = {
    event match {
      case WrittenCouponApplied(code, userId, userEmail) => // old format found in journal
        println(s"[ModelAdapter.fromJournal] Converting $event to DOMAIN model (WrittenCouponApplied -> CouponApplied)")
        EventSeq.single(CouponApplied(code, User(userId, userEmail, "")))

      case WrittenCouponAppliedV2(code, userId, userEmail, userNameNewField) => // newer format found in journal - remember to append this class in config !
        println(s"[ModelAdapter.fromJournal] Converting $event to DOMAIN model (WrittenCouponAppliedV2 -> CouponApplied)")
        EventSeq.single(CouponApplied(code, User(userId, userEmail, userNameNewField)))

      case other =>
        EventSeq.single(other)
    }
  }

  /* Flow: persistent actor (persist) -> toJournal -> serializer -> journal */
  override def toJournal(event: Any): Any = {
    event match {
      case ev @ CouponApplied(code, user) =>
        println(s"[ModelAdapter.toJournal] Converting $event to DATA model (CouponApplied -> Wri)")

        WrittenCouponAppliedV2(code, user.id, user.email, user.userNameNewField)
    }
  }
}

object DetachingModelsDemo extends App {
  import DomainModel._

  class CouponManager extends PersistentActor with ActorLogging {
    private val coupons: mutable.Map[String, User] = new mutable.HashMap[String, User]()

    override def persistenceId: String = "coupon-mgr-id"

    override def receiveCommand: PartialFunction[Any,Unit] = {
      case ApplyCoupon(coupon, user) =>
        if(!coupons.contains(coupon.code)) {
          persist(CouponApplied(coupon.code, user)) { ev =>
            log.info(s"Persisted $ev")
            coupons.put(coupon.code, user)
          }
        }
    }

    override def receiveRecover: PartialFunction[Any,Unit] = {
      case ca @ CouponApplied(code, user) =>
        coupons.put(code, user)
        log.info(s"Recovered $ca")
    }
  }

  private def run = {
    import DomainModel._

    val as = ActorSystem("DetachingModelsDemo", ConfigFactory.load().getConfig("detachingModels"))
    val couponMgr = as.actorOf(Props[CouponManager], "couponMgr")

    (1 to 5).foreach { idx =>
      val coupon = Coupon(s"c_$idx", 10)
      val user = User(idx.toString, s"user_$idx@domain.com", s"name_$idx")

      couponMgr ! ApplyCoupon(coupon, user)
    }
  }

  run
}
