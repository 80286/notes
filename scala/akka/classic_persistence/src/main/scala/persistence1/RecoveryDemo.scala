package persistence1

import java.util.Date
import akka.persistence.{PersistentActor, SnapshotOffer, SaveSnapshotSuccess, SaveSnapshotFailure}
import akka.actor.{ActorLogging, ActorSystem, Props}
import scala.collection.mutable
import akka.persistence.Recovery
import akka.persistence.SnapshotSelectionCriteria
import akka.persistence.RecoveryCompleted

object RecoveryDemo extends App {
  /* Demo shows:
   * - stashing awaiting commands while recovery
   * - failure while recovering (overriding onRecoveringFailure)
   * - overriding recovery settings (overriding recovery)
   * - recovery status (in progress/done)
   * - changing receive() impl by calling context.become while recovery
   */

  case class Command(contents: String)
  case class Event(id: Int, contents: String)

  class RecoveryActor extends PersistentActor with ActorLogging {
    override def persistenceId: String = "recovery-actor-id"

    override def receiveCommand: Receive = online(lastEventId = 0)

    private def online(lastEventId: Int): Receive = {
      case Command(contents) =>
        persist(Event(lastEventId, contents)) { ev =>
          log.info(s"Event persisted ($ev) - recovery isFinished: ${recoveryFinished}")
        }
    }

    override def receiveRecover: Receive = {
      case Event(id, contents) =>
        if(contents == "50") throw new RuntimeException("Test failure while recovery")

        log.info(s"Event recovered ($contents) - recovery isFinished: ${recoveryFinished}")

        context.become(online(lastEventId = id))
        // Notice: there is no way of changing receiveRecover implementation on the fly
        // After recovery the final receive handler will be the result of all the stacking of context.become calls

      case RecoveryCompleted =>
        log.info("Recovery completed.")
        // Do some initialization of actor here
    }

    override protected def onRecoveryFailure(cause: Throwable, event: Option[Any]): Unit = {
      log.error(s"Recovery of event $event failed with: $cause - actor will be stopped (otherwise its state would be unconsistent)")
      super.onRecoveryFailure(cause, event)
    }

    override def recovery: Recovery =
      Recovery(
        fromSnapshot = SnapshotSelectionCriteria.Latest,
        toSequenceNr = 49 // only first 49 events will be recovered on start
      )

    // override def recovery: Recovery = Recovery.none // will completely turn off recovery on start
  }

  private def run = {
    val as = ActorSystem("RecoveryDemo")
    val actor = as.actorOf(Props[RecoveryActor], "recoveryActor")

    (1 to 100).foreach { idx =>
      actor ! Command(idx.toString)
    }

    // On second run you can notice that:
    // all commands sent during recovery are stashed !!!
  }

  run
}
