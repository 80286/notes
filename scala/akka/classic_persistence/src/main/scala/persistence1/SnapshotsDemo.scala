package persistence1

import java.util.Date
import akka.persistence.{PersistentActor, SnapshotOffer, SaveSnapshotSuccess, SaveSnapshotFailure}
import akka.actor.{ActorLogging, ActorSystem, Props}
import scala.collection.mutable

object ShapshotsDemo extends App {
  case class MsgSpec(from: String, to: String, msg: String)
  case class ChatMsg(spec: MsgSpec)

  case class ChatMsgReceived(spec: MsgSpec)

  /*
   * Snapshots are used to store internal state from some point of time
   * instead of replaying all events.
   *
   * - receiveRecover:    implements SnapshotOffer(metadata, contents) to restore snapshot
   * - saveSnapshot(...): saves internal state to the journal
   */

  class ChatSystem extends PersistentActor with ActorLogging {
    private val lastMessages = new mutable.Queue[MsgSpec]()
    private val numOfLastStoredMessages = 10
    private var messagesWithoutSnapshot = 0

    private def saveSnapshotIfNeeded(): Unit = {
      messagesWithoutSnapshot += 1

      if(messagesWithoutSnapshot >= 10) {
        log.info(s"Saving snapshot of last $numOfLastStoredMessages messages...")
        messagesWithoutSnapshot = 0
        saveSnapshot(lastMessages)
      }
    }

    private def storeMsg(spec: MsgSpec): Unit = {
      if(lastMessages.size >= numOfLastStoredMessages)
        lastMessages.dequeue()

      lastMessages.enqueue(spec)
    }

    override def persistenceId: String = "chat-system-id"

    override def receiveCommand: Receive = {
      case ChatMsg(msgSpec) =>
        persist(ChatMsgReceived(msgSpec)) { ev =>
          log.info(s"Chat msg $msgSpec persisted.")
          storeMsg(msgSpec)
          saveSnapshotIfNeeded()
        }

      case msg: SaveSnapshotSuccess =>
        log.info(s"Received $msg")

      case msg: SaveSnapshotFailure =>
        log.info(s"Received $msg")

      case "print" =>
        log.info(s"Last messages:")
        lastMessages.foreach(msg => log.info(msg.toString))

      case "stop" =>
        context.stop(self)
    }

    override def receiveRecover: Receive = {
      case msg: ChatMsgReceived =>
        log.info(s"Recovered EVENT: $msg")
        storeMsg(msg.spec)

      case SnapshotOffer(metadata, contents) =>
        val recoveredMessages = contents.asInstanceOf[mutable.Queue[MsgSpec]]
        recoveredMessages.foreach(lastMessages.enqueue(_))
        log.info(s"Recovered SNAPSHOT: ${recoveredMessages.size} messages: $recoveredMessages")
    }
  }

  private def run = {
    val as = ActorSystem("Snapshots")
    val chat = as.actorOf(Props(new ChatSystem()))

    // (A)
    (1 to 100).foreach { idx =>
      chat ! ChatMsg(MsgSpec(from = "x", to = "y", msg = s"msg-$idx"))
    }

    chat ! "print"
    // chat ! "stop"

    // To test snapshot recovery:
    // 0. Call: rm -rf target/{journal,snapshots}
    // 1. Call (A) section for one time
    // 2. Call: rm -rf target/journal/LOCK
    // 3. Comment out (A), leave only "print" request, run this app again
    // 4. snapshot should restore last 10 messages without recovering all past events available in journal
    // (and eventually all events persisted after last saveSnapshot() call)
  }

  run
}
