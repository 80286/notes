package persistence1

import java.util.Date
import akka.persistence.{PersistentActor, SnapshotOffer, SaveSnapshotSuccess, SaveSnapshotFailure}
import akka.actor.{ActorLogging, ActorSystem, Props}
import scala.collection.mutable
import akka.actor.Actor
import akka.actor.ActorRef

object PersistAsyncDemo extends App {
  case class Command(data: String)
  case class Event(data: String)

  /* Use cases for persistAsync:
   * - improving performance of processing commands
   * - ordering of commands is not relevant
   * 
   * Guarantees:
   * - persist call happen in order
   * - persist callbacks are also called in order
   * - new messages can be handled between persist() and persist-callback time gap
   */

  class TestActor(eventAggregator: ActorRef) extends PersistentActor with ActorLogging {
    override def persistenceId: String = "persist-async-demo-actor-id"

    override def receiveCommand: Receive = {
      case Command(data) =>
        eventAggregator ! s"Processing $data"

        persistAsync(Event(data)) { ev =>
          eventAggregator ! ev
        }

        persistAsync(Event(s"${data}_processed")) { ev =>
          eventAggregator ! ev
        }
    }

    override def receiveRecover: Receive = {
      case Event(data) =>
        log.info(s"Recovered: $data")
    }
  }

  class EventAggregator extends Actor with ActorLogging {
    override def receive: Receive = {
      case msg => log.info(s"Aggregating $msg")
    }
  }

  private def run = {
    val as = ActorSystem("PersistAsyncDemo")
    val eventAggregator = as.actorOf(Props[EventAggregator])
    val streamProcessor = as.actorOf(Props(new TestActor(eventAggregator)), "streamProcessor")

    streamProcessor ! Command("c1")
    streamProcessor ! Command("c2")
  }

  run
}
