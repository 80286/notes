package persistence1

import java.util.Date
import akka.persistence.PersistentActor
import akka.actor.{ActorLogging, ActorSystem, Props}

// Intro to Event Sourcing:
//
// Persistent store: a.k.a. journal

// Persistent actor:
// - has PersistentId (unique for every actor)
// - can send events to persistent store (has action: persist)
// - is able to recover by replaying events from persistent store which are associated for its PersistentId (has action: recover)

// When an actor handles a request (we'll use a 'command' term for this in this case)
// - it can persist event to the store
// - after event is persisted, then: it changes its internal state

// When persistent actor starts up or its restarted:
// - then replays all events associated for its PersistentId using data from persistent storage

object EventSourcingIntro extends App {
  /*
   * Scenario: an accountant keeps track of our invoices
   */

  // Commands:
  case class Invoice(recipient: String, date: Date, amount: Int)
  case class InvoiceBulk(invoices: List[Invoice])
  case object Shutdown

  // Events:
  case class InvoiceRecorded(id: Int, recipient: String, date: Date, amount: Int)

  class Accountant() extends PersistentActor with ActorLogging {
    private var latestInvoiceId: Int = 0
    private var totalAmount: Int = 0

    override def persistenceId: String = "acountant-id" // it'd be good to generate unique id here

    // Regular handler, same as receive:
    override def receiveCommand: Receive = {
      /*
       * When Persistent Actor receives a command, then:
       * - create event to persist in journal
       * - perist that event and pass it in a callback that will be called when event is written
       * - update actor state when the event has persisted
       */
      case Invoice(recipient, date, amount) =>
        log.info(s"Received invoice for amount: $amount")

        // All messages received between persist() call and its callback will be stashed !
        persist(InvoiceRecorded(latestInvoiceId, recipient, date, amount)) { event =>
          // It's safe to access mutable state here
          // Akka persistence guarantees that other threads will not call this section at the same time
          updateActorState(amount = event.amount)

          // despite we're inside future callback, call of sender() in this case will correclty point to the sender of Invoice(...) msg
          log.info(s"Persisted: $event; totalAmount: $totalAmount")
        }

      // Dealing with multiple events:
      case InvoiceBulk(invoices) =>
        /*
         * - create events
         * - persist all events
         * - update actor state when every event is persisted
         */
        val invoiceIds: Seq[Int] = latestInvoiceId to (latestInvoiceId + invoices.size)
        val events = invoices.zip(invoiceIds).map { case (Invoice(recipient, date, amount), invoiceId) =>
          InvoiceRecorded(invoiceId, recipient, date, amount)
        }

        persistAll(events) { ev => 
          updateActorState(amount = ev.amount)
          log.info(s"Persisted (persistAll): $ev; totalAmount: $totalAmount")
        }

      case Shutdown =>
        // It's better to define own message that will stop persistent actor (instead of sending PoisonPill which is handled by separate mailbox)
        // Such message will be processed after all invoices, it will not interrupt application of them
        context.stop(self)
    }

    private def updateActorState(amount: Int) = {
      latestInvoiceId += 1
      totalAmount += amount
    }

    // Handler called on recovery:
    override def receiveRecover: Receive = {
      // It should follow the steps of persisted events from receiveCommand handler
      case ir @ InvoiceRecorded(id, _, _, amount) =>
        latestInvoiceId = id
        totalAmount += amount
        log.info(s"Recovered invoice: $id ($ir)")
    }

    // Called on persist() fail.
    // Actor will be stopped, regardless to the supervision strategy
    // (if we're not able to make complete recover, then actor will end up in undefined broken state)
    // It's better to create actor one more time after a while (use BackoffSupervisor)
    override def onPersistFailure(cause: Throwable, event: Any, seqNr: Long): Unit = {
      log.error(s"Fail to persist $event ($cause)")
      super.onPersistFailure(cause, event, seqNr)
    }

    // Called when journal has problem with persisting the event.
    // The actor is resumed.
    override def onPersistRejected(cause: Throwable, event: Any, seqNr: Long): Unit = {
      log.error(s"Persist rejected for $event ($cause)")
      super.onPersistRejected(cause, event, seqNr)
    }
  }

  private def runSingle = {
    val as = ActorSystem("PersistentActorsDemo")
    val accountant = as.actorOf(Props(new Accountant()))

    (1 to 10).foreach { idx =>
      accountant ! Invoice("POLMO", new Date, idx)
    }
    // If above loop will be commented, then next call of run() will
    // try to recreate (recover) actor state using events from journal according to its persitence id of actor (which is fixed in this case)
    // remove directory pointed in application.conf to clear journal state

    accountant ! Shutdown

    // Following persitance failures can occur:
    // - persist() throws an error            -> check: onPersistFailure
    // - journal impl. can't persist a event  -> check: onPersistRejected

    // !! persist()/persistAll() should NOT be called from Future(...)
    // persist calls should not be called from different threads
  }

  private def runMultiple = {
    val as = ActorSystem("PersistentActorsDemo")
    val accountant = as.actorOf(Props(new Accountant()))

    accountant ! InvoiceBulk((1 to 10).map(Invoice("POLMO", new Date, _)).toList)

    accountant ! Shutdown
  }

  runSingle
}
