package persistence1

import java.util.Date
import akka.persistence.PersistentActor
import akka.actor.{ActorLogging, ActorSystem, Actor, ActorRef, Props}

object MultipleEventsPersist extends App {
  case object Event1
  case object Event2

  class MultiPersistActor(receivingActor: ActorRef) extends PersistentActor with ActorLogging {
    override def persistenceId: String = "multi-perist-actor-id"

    override def receiveCommand: Receive = {
      case "init" =>
        log.info(s"Init called...")

        persist(Event1) { ev =>
          receivingActor ! "Event1 persisted"

          persist("event1-declaration") { decl =>
            receivingActor ! "Event1-declaration persisted"
          }
        }

        persist(Event2) { ev =>
          receivingActor ! "Event2 persisted"

          persist("event2-declaration") { decl =>
            receivingActor ! "Event2-declaration persisted"
          }
        }

        /*
         * Message ordering is guaranteed because impl. of persist() use message passing
         * while sending persist requests to the journal.
         * In this case receiving actor will always get responses in following order:
         *
         * Received: Event1 persisted
         * Received: Event2 persisted
         * Received: Event1-declaration persisted
         * Received: Event2-declaration persisted
         * 
         */

      case "stop" =>
        context.stop(self)
    }

    override def receiveRecover: Receive = {
      case msg =>
        log.info(s"Recovered: $msg")
    }
  }

  class ReceivingActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case msg =>
        log.info(s"Received: $msg")
    }
  }

  private def run = {
    val as = ActorSystem("MultipleEventsPersist")
    val receivingActor = as.actorOf(Props[ReceivingActor])
    val testActor = as.actorOf(Props(new MultiPersistActor(receivingActor)))

    testActor ! "init"
    testActor ! "stop"
  }

  run
}
