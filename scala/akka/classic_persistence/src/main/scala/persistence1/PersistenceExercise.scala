package persistence1

import akka.persistence.PersistentActor
import akka.actor.{ActorLogging, ActorSystem, Props}

object PersistenceExercise extends App {
  case class Vote(citizenId: String, candidate: String)
  case object StopVote

  case class CitizenVoted(citizenId: String, candidate: String)

  class VotingStation extends PersistentActor with ActorLogging {
    override def persistenceId: String = "voting-station"

    private def readyToVote(actualVotes: Map[String, Int], alreadyVoted: Set[String]): Receive = {
      case Vote(citizenId, candidate) =>
        persist(CitizenVoted(citizenId, candidate)) { ev =>
          val updatedVotes = actualVotes + (candidate -> (actualVotes.getOrElse(candidate, 0) + 1))
          val updatedAlreadyVoted = alreadyVoted ++ Set(citizenId)
          context.become(readyToVote(updatedVotes, updatedAlreadyVoted))
        }

      case "print" =>
        log.info(s"Actual votes:  $actualVotes")
        log.info(s"Already voted: $alreadyVoted")

      case StopVote =>
        context.stop(self)
        context.system.terminate()
    }

    private def updateVotes(votes: Map[String, Int], candidate: String): Map[String, Int] = {
      votes + (candidate -> (votes.getOrElse(candidate, 0) + 1))
    }

    override def receiveCommand: Receive = readyToVote(Map.empty, Set.empty)

    override def receiveRecover: Receive = {
      // Unfortunately, haven't found any way to call context.become for receiveRecover
      // that's why I had to use vars here:
      var votes = Map.empty[String, Int];
      var alreadyVoted = Set.empty[String];
      {
        case CitizenVoted(citizenId, candidate) =>
          votes = updateVotes(votes, candidate)
          alreadyVoted = alreadyVoted ++ Set(citizenId)
          context.become(readyToVote(votes, alreadyVoted))
      }
    }
  }

  private def testVote = {
    val as = ActorSystem("PersistentActorsDemo")
    val votingStation = as.actorOf(Props(new VotingStation()))

    // votingStation ! Vote(citizenId = "0", candidate = "oleksy")
    // votingStation ! Vote(citizenId = "1", candidate = "miller")
    // votingStation ! Vote(citizenId = "2", candidate = "czarzasty")
    votingStation ! "print"
    votingStation ! StopVote
  }

  testVote
}
