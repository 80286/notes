[https://github.com/rockthejvm/udemy-akka-persistence-starter]

Remember to do journal cleanup after running different apps (`rm -rf target/journal` or `rm -rf target/snapshots`)
otherwise you'll get exceptions like `IO error: lock` (when using local store)
