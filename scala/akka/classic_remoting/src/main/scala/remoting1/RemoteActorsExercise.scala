package remoting1

import akka.actor.Actor
import akka.actor.ActorLogging
import scala.concurrent.Future
import akka.actor.ActorRef
import akka.actor.ActorIdentity
import akka.actor.Identify
import akka.actor.Props
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.actor.PoisonPill
import akka.routing.FromConfig

/* 
 * Apps:
 * - ClientApp
 * - WorkerApp
 *  
 * Testing:
 * 1. Launch 'sbt run' for WorkerApp & ClientApp at the same time
 * 2. Track console output of both apps and wait for final WordCountResult msg
 *
 * Description:
 * - WorkerApp creates 5 worker actors on localhost:2552
 * - ClientApp (localhost:2551) creates WordCountMaster actor and identifies 5 worker actors from localhost:2552
 * - ClientApp passes some text to WordCountMaster actor
 * - WordCountMaster actor splits received text into sentences and forwards to first found worker
 * - WordCountMaster waits for WordCountResult rensposes from workers
 *
 * Notice:
 * - code marked by (1) was used in 1st exercise (remote actors)
 * - code marked by (2) was used in 2nd exercise (remote deployment)
 * uncommented version uses code for (1)
 * */

object WordCountDomain {
  case class Initialize(remoteActorPath: String, numOfWorkers: Int)
  case class WordCountTask(text: String)
  case class WordCountResult(count: Int)
  case object EndWordCount
}

class WordCountWorker extends Actor with ActorLogging {
  import WordCountDomain._

  def receive: Receive = {
    case msg @ WordCountTask(text) =>
      log.info(s"WordCountWorker received $msg")
      sender() ! WordCountResult(count = text.split(" ").length)
  }
}

class WordCountMaster extends Actor with ActorLogging {
  import WordCountDomain._

  // Start (2) ==========================================
  val workerRouter = context.actorOf(FromConfig.props(Props[WordCountWorker]), "workerRouter")

  // Same impl. as online() but uses router:
  private def onlineUsingRouter(remainingTasks: Int, totalCount: Int): Receive = {
    case text: String =>
      log.info(s"Received text [$text]")
      val sentences = text.split("\\. ")

      sentences.foreach(sentence => workerRouter ! WordCountTask(sentence))
      context.become(onlineUsingRouter(remainingTasks = remainingTasks + sentences.length, totalCount = totalCount))

    case msg @ WordCountResult(count) =>
      if(remainingTasks == 1) {
        log.info(s"Received final $msg - total: ${totalCount + count}, sending PoisonPills to worker actors")

        context.stop(self)
      } else {
        log.info(s"Received partial $msg  (total: $totalCount)")
        context.become(onlineUsingRouter(remainingTasks - 1, totalCount + count))
      }
  }
  // End (2) ==========================================

  def receive: Receive = {
    case Initialize(remoteActorsPathFmt, numOfWorkers) =>
      identifyWorkers(remoteActorsPathFmt, numOfWorkers) // From (1)
      // deployWorkersOnRemoteJvm(remoteActorsPathFmt, numOfWorkers) // From (2)
  }

  // From (2)
  private def deployWorkersOnRemoteJvm(remoteActorsPathFmt: String, numOfWorkers: Int) = {
    val workers = (1 to numOfWorkers).map { idx => 
      context.actorOf(Props[WordCountWorker], remoteActorsPathFmt.format(idx))
    }

    context.become(online(workers, 0, 0))
  }

  // From (1)
  private def identifyWorkers(remoteActorsPathFmt: String, numOfWorkers: Int) = {
    log.info(s"Trying to identify ${numOfWorkers} workers... (using path: $remoteActorsPathFmt)")

    (1 to numOfWorkers).map { idx =>
      val remoteWorkerPath = remoteActorsPathFmt.format(idx)

      log.info(s"Sending Identify request to $remoteWorkerPath...")
      context.actorSelection(remoteWorkerPath) ! Identify(0)
    }

    context.become(initializing(Seq.empty, numOfWorkers))
  }

  private def initializing(workersIdentifiedSoFar: Seq[ActorRef], remainingWorkers: Int): Receive = {
    case ActorIdentity(_, Some(workerActorRef)) =>
      log.info(s"Worker identified: $workerActorRef (identifiedSoFar: ${workersIdentifiedSoFar.size}, remainingWorkers: $remainingWorkers)")

      val updatedWorkersIndeitifiedSoFar = Seq(workerActorRef) ++ workersIdentifiedSoFar

      if(remainingWorkers == 1) {
        log.info("All workers collected, switching to online state.")

        context.become(online(
          workers = updatedWorkersIndeitifiedSoFar,
          remainingTasks = 0,
          totalCount = 0
        ))
      } else
        context.become(initializing(
          workersIdentifiedSoFar = updatedWorkersIndeitifiedSoFar,
          remainingWorkers = remainingWorkers - 1
        ))
  }

  private def online(workers: Seq[ActorRef], remainingTasks: Int, totalCount: Int): Receive = {
    case text: String =>
      log.info(s"Received text [$text]")
      val sentences = text.split("\\. ")

      Iterator.continually(workers).flatten.zip(sentences.iterator).foreach {
        case (worker, sentence) =>
          log.info(s"Sending [$sentence] to ${worker.path}...")
          worker ! WordCountTask(text = sentence)
      }

      context.become(online(workers, remainingTasks = remainingTasks + sentences.length, totalCount = totalCount))

    case msg @ WordCountResult(count) =>
      if(remainingTasks == 1) {
        log.info(s"Received final $msg - total: ${totalCount + count}, sending PoisonPills to worker actors")

        workers.foreach(_ ! PoisonPill)
        context.stop(self)
      } else {
        log.info(s"Received partial $msg  (total: $totalCount)")
        context.become(online(workers, remainingTasks - 1, totalCount + count))
      }
  }
}

object ClientApp extends App {
  val clientActorSystem = ActorSystem("ClientAppActorSystem", ConfigFactory.parseString(Configs.get(port = 2551)))
  println(s"Starting WorkerApp system (port=2551: $clientActorSystem)")

  val wordCountMaster = clientActorSystem.actorOf(Props[WordCountMaster], "wordCountMaster")
  val remoteWorkerActorPath = "akka://WorkerAppActorSystem@localhost:2552/user/wordCountWorker-%d"

  wordCountMaster ! WordCountDomain.Initialize(remoteWorkerActorPath, numOfWorkers = 5)
  println("Waiting for initialization...")
  Thread.sleep(5000)

  Seq("abc def", "111 2222", "333 444 555").foreach { str =>
    println(s"Sending [$str] to ${wordCountMaster.path}...")
    wordCountMaster ! str
  }
}

object WorkerApp extends App {
  val clientActorSystem = ActorSystem("WorkerAppActorSystem", ConfigFactory.parseString(Configs.get(port = 2552)))
  println(s"Starting WorkerApp (port=2552: $clientActorSystem)")

  (1 to 5).foreach { idx =>
    val workerRef = clientActorSystem.actorOf(Props[WordCountWorker], s"wordCountWorker-$idx")
    println(s"Worker $idx started (${workerRef.path})")
  }

  Thread.sleep(100000)
}

private object Configs {
  def get(port: Int) = s"""
    akka {
      actor {
        provider = remote

        deployment = { // extra config from (2)
          "/wordCountMaster/*" {
            remote = "akka://WorkerAppActorSystem@localhost:2552"
          }

          /wordCountMaster/workerRouter { // From (2)
            router = round-robin-pool
            nr-of-instances = 10
            target.nodes = ["akka://WorkerAppActorSystem@localhost:2552"]
          }
        }
      }
      remote {
        artery {
          enabled = on
          transport = aeron-udp
          canonical.hostname = "localhost"
          canonical.port = $port
          advanced {
            outbound-message-queue-size = 1000000
          }
        }
      }
    }
  """.stripMargin
}
