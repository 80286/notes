package remoting1

import com.typesafe.config.ConfigFactory
import akka.actor.ActorSystem
import akka.actor.Props
import akka.util.Timeout
import java.util.concurrent.TimeUnit
import scala.util.Success
import scala.util.Failure
import akka.actor.ActorLogging
import akka.actor.Actor
import akka.actor.Identify
import akka.actor.ActorIdentity

// object RemoteActorsDemo1 extends App {
//   private def run = {
//     val configPath = "remoteActorsDemo/remoteActorsDemo.conf"
//
//     // Local system exposes 2551 port:
//     val localSystem = ActorSystem("LocalSystem", ConfigFactory.load(configPath))
//
//     // Remote system exposes 2552 port:
//     val remoteSystem = ActorSystem("RemoteSystem", ConfigFactory.load(configPath).getConfig("remoteSystem"))
//
//     val localSimpleActor = localSystem.actorOf(Props[TestActor], "localTestActor")
//     val remoteSimpleActor = remoteSystem.actorOf(Props[TestActor], "remoteTestActor")
//
//     // Both below requests will log address of each actor system
//     localSimpleActor ! "msg-to-local-actor"
//     remoteSimpleActor ! "msg-to-remote-actor"
//     // If you'll remove 'remote' section from ActorSystem config, then
//     // info about host & port will disappear from logs:
//     // With remote config:    [akka://LocalSystem@localhost:2551/user/localTestActor]
//     // Without remote config: [akka://LocalSystem/user/localTestActor]
//   }
//
//   run
// }

object RemoteActorsDemo2a extends App {
  val configPath = "remoteActorsDemo/remoteActorsDemo.conf"
  val localSystem = ActorSystem("LocalSystem2", ConfigFactory.load(configPath))
  val localSimpleActor = localSystem.actorOf(Props[TestActor], "localTestActor2")

  val remoteActorPath = "akka://RemoteSystem2@localhost:2552/user/remoteTestActor2"

  // 1. Find from other app via ActorSelection (2b):
  val remoteActorSelection = localSystem.actorSelection(remoteActorPath)
  remoteActorSelection ! "msg-from-LocalSystem2"

  // 2. Resolve ActorSelection to ActorRef
  {
    import localSystem.dispatcher
    implicit val timeout: Timeout = Timeout(3, TimeUnit.SECONDS)
    val remoteActorRefFuture = remoteActorSelection.resolveOne()
    remoteActorRefFuture.onComplete {
      case Success(actorRef) => actorRef ! "Resolved via Future"
      case Failure(exception) => println(s"Unable to resolve: ${exception.getMessage()}")
    }
  }

  // 3. Actor identification:
  // - resolver will ask for an ActorSelection from the local ActorSystem
  // - resolver will send a Identify(id)
  // - remote actor will respond automatically with ActorIdentity(id, actorRef)
  // - resolver will be able to use actorRef
  class ActorResolver extends Actor with ActorLogging {
    override def preStart(): Unit = {
      val selection = context.actorSelection(remoteActorPath)
      selection ! Identify(12)
    }

    override def receive: Receive = {
      case ActorIdentity(id, Some(actorRef)) =>
        actorRef ! "ACK Received actorRef"
    }
  }

  localSystem.actorOf(Props[ActorResolver], "localActorResolver")
}

/* Below app should be started from IJ, in parallel to be ready for requests from 2a App.
 *
 * For sbt:
 * - open 2 terminals inside build.sbt dir
 * - make sure that 2b app calls sleep at the end
 * - goto terminal (2), call 'sbt run', select 2b app
 * - goto terminal (1), call 'sbt run', select 2a app
 * - you should observe how 2a app sent messages to the 2b app using 3 different methods
 * */
object RemoteActorsDemo2b extends App {
  val configPath = "remoteActorsDemo/remoteActorsDemo.conf"
  val remoteSystem = ActorSystem("RemoteSystem2", ConfigFactory.load(configPath).getConfig("remoteSystem"))
  val remoteSimpleActor = remoteSystem.actorOf(Props[TestActor], "remoteTestActor2")
  remoteSimpleActor ! "msg-from-RemoteSystem2"

  println("Calling sleep....")
  Thread.sleep(10000)
}
