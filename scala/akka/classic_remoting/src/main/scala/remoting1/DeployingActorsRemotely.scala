package remoting1

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.actor.Props
import akka.actor.Address
import akka.actor.AddressFromURIString
import akka.actor.Deploy
import akka.remote.RemoteScope
import akka.routing.FromConfig
import akka.actor.ActorLogging
import akka.actor.Actor
import akka.actor.Terminated
import akka.actor.PoisonPill

object DeployingActorsRemotely_LocalApp extends App {
  val as = ActorSystem("LocalActorSystem", ConfigFactory.load("remoting1/deployingActorsRemotely.conf").getConfig("localApp"))

  // 1: Any new actor with name 'remoteActor' according to used config file
  // will be >deployed< on ActorSystem pointed by name in config - in this case it will be deployed on remote app.
  // Used Props object will be passed to remote ActorSystem (Props object needs to be serializable !! avoid passing lambdas)
  //
  // If there's no deployment configured, then actor will be created locally.
  val remoteActorRef = as.actorOf(Props[TestActor], "remoteActor")
  remoteActorRef ! "Hello remote actor (1)"

  println(remoteActorRef)
  // Will print:
  // Actor[akka://RemoteActorSystem@localhost:2552/remote/akka/LocalActorSystem@localhost:2551/user/remoteActor#-2018181471]
  // Notice: actor path contains a name of ActorSystem that requested creation of actor (LocalActorSystem)
  
  // 2: Remote deployment using api:
  val remoteSystemAddress: Address = AddressFromURIString("akka://RemoteActorSystem@localhost:2552")
  val remotelyDeployedActor = as.actorOf(Props[TestActor].withDeploy(Deploy(scope = RemoteScope(remoteSystemAddress))))

  remotelyDeployedActor ! "Hello remote actor (2)"

  // 3: Routers with routees deployed remotely between 2 JVMs (check target.nodes in config):
  val poolRouter = as.actorOf(FromConfig.props(Props[TestActor]), "routerWithRemoteChildren")
  (1 to 5).map { idx =>
    poolRouter ! s"message $idx"
  }
  // After lauching this you should see that messages were distributed between both pointed actor systems

  // 4: Watching remote actors
  class ParentActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case "create" =>
        log.info(s"Creating remote child actor...")
        val child = context.actorOf(Props[TestActor], "remoteChild")

        // Local actor starts to subscribe remote actor:
        context.watch(child)

      case msg @ Terminated(ref) =>
        // This message will be also received when RemoteActorSystem will be terminated for some reason
        //
        //
        // From the point of connecting two ActorSystems, these systems start to
        // send heartbeat messages to each other to validate stability of connection
        log.info(s"Received $msg")
    }
  }
  val parentActor = as.actorOf(Props[ParentActor], "watcher")
  parentActor ! "create"
  Thread.sleep(1000)

  val remoteChildSelection =
    as.actorSelection("akka://RemoteActorSystem@localhost:2552/remote/akka/LocalActorSystem@localhost:2551/user/watcher/remoteChild")

  remoteChildSelection ! PoisonPill
}

object DeployingActorsRemotely_RemoteApp extends App {
  val as = ActorSystem("RemoteActorSystem", ConfigFactory.load("remoting1/deployingActorsRemotely.conf").getConfig("remoteApp"))

  Thread.sleep(10000)
}
