package remoting1

import akka.actor.Actor
import akka.actor.ActorLogging

class TestActor extends Actor with ActorLogging {
  def receive: Receive = {
    case msg =>
      log.info(s"Received $msg from ${sender()}")
  }
}
