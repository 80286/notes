package remoting2

import com.typesafe.config.ConfigFactory
import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.cluster.Cluster
import akka.cluster.ClusterEvent
import akka.actor.Props
import akka.actor.Address

class ClusterSubscriber extends Actor with ActorLogging {
  private val cluster = Cluster(context.system)

  override def receive: Receive = {
    case ClusterEvent.MemberJoined(member) =>
      log.info(s"ClusterEvent.MemberJoined: ${member.address}")

    case ClusterEvent.MemberUp(member) if member.hasRole("role1") =>
      log.info(s"!!! ClusterEvent.MemberUp: ${member.address} - member with role1 detected")

    case ClusterEvent.MemberUp(member) =>
      log.info(s"ClusterEvent.MemberUp: ${member.address}")

    case ClusterEvent.MemberRemoved(member, previousState) =>
      log.info(s"ClusterEvent.MemberRemoved: ${member.address} (prevState: $previousState)")

    case ClusterEvent.UnreachableMember(member) =>
      log.info(s"ClusterEvent.UnrechableMember: ${member.address}")

    case other =>
      log.info(other.toString)
  }

  override def preStart(): Unit = {
    cluster.subscribe(
      self,
      initialStateMode = ClusterEvent.InitialStateAsEvents,
      classOf[ClusterEvent.MemberEvent],
      classOf[ClusterEvent.UnreachableMember]
      )
  }

  override def postStop(): Unit = {
    cluster.unsubscribe(self)
  }
}

object ClusteringDemo extends App {
  def startCluster(ports: List[Int]): Unit = {
    ports.foreach { port =>
      val config = ConfigFactory
        .parseString("akka.remote.artery.canonical.port = $port")
        .withFallback(ConfigFactory.load("remoting2/clusteringDemo.conf"))

      // All ActorSystems in the cluster must use same name:
      val as = ActorSystem("DemoCluster")

      // Create subscriber actor:
      as.actorOf(Props[ClusterSubscriber])
    }
  }

  // Will start cluster consisting of 3 ActorSystems that listen on different ports:
  startCluster(ports = List(2551, 2552, 2553)) // Using port 0 will allocate random port
}

object ClusteringDemoManualRegistration extends App {
  // ActorSystem with 2555 port
  val as = ActorSystem("DemoCluster", ConfigFactory.load("remoting2/clusteringDemo.conf").getConfig("manualRegistration"))

  val cluster = Cluster(as)

  def joinExistingClusterFromClusteringDemoAppCluster = {
    // Manual join call instead of setting cluster.seed-nodes
    // ClusteringDemo must run in background to use this:
    cluster.joinSeedNodes(List(
      Address("akka", as.name, "localhost", 2551),
      Address("akka", as.name, "localhost", 2552),
    ))
  }

  def joinExistingNodeFromClusteringDemoAppCluster = 
    cluster.join(Address("akka", as.name, "localhost", 2553))

  def joinMyself =
    cluster.join(Address("akka", as.name, "localhost", 2555))
}
