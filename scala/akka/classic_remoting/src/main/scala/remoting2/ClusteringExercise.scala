package remoting2

import akka.actor.{Actor, ActorRef, ActorLogging, ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import akka.cluster.{Cluster, ClusterEvent}
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import akka.cluster.Member

object ChatDomain {
  case class ChatMessage(userId: String, msg: String)
  case class UserMessage(msg: String)
  case class EnterRoom(fullAddress: String, userId: String)
}

class ChatActor(userId: String, port: Int) extends Actor with ActorLogging {
  import ClusterEvent.{MemberUp, MemberRemoved, UnreachableMember}
  import ChatDomain._

  private implicit val timeout = Timeout(3.seconds)
  private implicit val ec: ExecutionContext = context.dispatcher

  private val cluster = Cluster(context.system)

  private def findAnotherChatActorRef(memberAddress: String) = {
    val fullAddress = s"${memberAddress}/user/chatActor"
    val remoteActorSelection = context.actorSelection(fullAddress)

    remoteActorSelection
  }

  override def receive: Receive =
    online(chatMembers = Set.empty)

  def online(chatMembers: Set[String]): Receive = {
    case MemberUp(member) =>
      log.info(s"Member ${member.address} joined. Sending hello message...")

      findAnotherChatActorRef(member.address.toString) ! ChatDomain.EnterRoom(
        fullAddress = s"${self.path.address}@localhost:$port",
        userId = userId
      )

    case UnreachableMember(member) =>
      log.info(s"Member unreachable: ${member.address}")

    case MemberRemoved(member, prevState) =>
      val remoteAddr = member.address.toString
      val remoteUserId = chatMembers(remoteAddr)

      log.info(s"User $remoteUserId left.")
      context.become(online(chatMembers - remoteAddr))

    case UserMessage(msg) =>
      chatMembers.foreach {
        remoteAddrAsString => findAnotherChatActorRef(remoteAddrAsString) ! ChatMessage(userId, msg)
      }

    case EnterRoom(remoteFullAddress, remoteUserId) =>
      if(remoteUserId != userId) {
        log.info(s"User $remoteUserId joined.")
        context.become(online(chatMembers + remoteFullAddress))
      }

    case ChatMessage(userId, msg) =>
      log.info(s"[$userId]: $msg")
  }

  override def preStart(): Unit =
    cluster.subscribe(self,
      initialStateMode = ClusterEvent.InitialStateAsEvents,
      classOf[ClusterEvent.MemberEvent],
      classOf[ClusterEvent.UnreachableMember]
    )

  override def postStop(): Unit =
    cluster.unsubscribe(self)
}

class ClusteringExerciseApp(userId: String, port: Int) extends App {
  val config = ConfigFactory
    .parseString(s"akka.remote.artery.canonical.port = $port")
    .withFallback(ConfigFactory.load("remoting2/clusteringExercise.conf"))

  val as = ActorSystem("ClusteringExercise", config)
  val chatActor = as.actorOf(Props(new ChatActor(userId, port)), "chatActor")

  scala.io.Source.stdin.getLines().foreach { line =>
    chatActor ! ChatDomain.UserMessage(line)
  }
}

object User1App extends ClusteringExerciseApp(userId = "user1", port = 2551)
object User2App extends ClusteringExerciseApp(userId = "user2", port = 2552)
object User3App extends ClusteringExerciseApp(userId = "user3", port = 2553)

/* 
 * > Initialization:
 *
 * ActorSystem 2551 (A) starts -> gets MemberUp for A, B, C -> each MemberUp req. forwards EnterRoom(memberA_addr@localhost:2551) to A, B, C -> each AS updates list of members
 * ActorSystem 2552 (B) starts -> gets MemberUp for A, B, C -> each MemberUp req. forwards EnterRoom(memberB_addr@localhost:2552) to A, B, C -> each AS updates list of members
 * ActorSystem 2553 (C) starts -> gets MemberUp for A, B, C -> each MemberUp req. forwards EnterRoom(memberC_addr@localhost:2553) to A, B, C -> each AS updates list of members
 *
 * > Sending message:
 * A gets UserMessage(line) -> A propagates ChatMessage(A_userId, line) to each chat member -> rest of members receives ChatMessage(userId, msg) and logs in the console
 * */
