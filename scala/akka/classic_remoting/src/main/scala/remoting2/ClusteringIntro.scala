package remoting2

object ClusteringInfo {
  /*
   * Clustering is based on Remoting, in most cases should be used instead of Remoting.
   *
   * Cluster: is composed of member nodes
   * Node: host + port + UID
   * Nodes may be deployed on:
   *   a) the same JVM
   *   b) multiple JVMS on the same machine
   *   c) on a set of machines of any scale
   *
   * =======================
   * Cluster membership:
   * - detected with: convergent gossip protocol
   * - Phi accural failure detector (same as Remoting)
   * - there is no leader election, leader is chosen deterministically
   *
   * Joining a cluster:
   * 1. New node has to contact seed nodes (initial contact points) in order, they are defined in config
   *   a) if I am the first seed node, then I'll join myself
   *   b) send Join command to the seed node that responds first
   *
   * 2. Node is in <Joining> state
   *   a) WAIT for gossip to converge
   *   b) ALL nodes in the cluster must acknowledge the new node
   *   c) Leader will set the state of the new node to <Up>
   *
   * After convergence all nodes in the cluster have to be in sync to know the state of incoming node.
   * =======================
   * Leaving cluster:
   *
   * 1. Safe and quiet option:
   * - node switches state to <Leaving>
   * - gossip converges at some point in the future
   * - leader sets the state to <Exiting>
   * - leader marks its state as <Removed>
   *
   * 2. Hard way option:
   * - node become unreachable
   * - gossip convergence and leader actions are not possible
   * - cluster must be removed manually
   * - can be automaticly downed by the leader (not preferred in production env)
   *
   *
   * =======================
   * When to use clustering?
   * - in distributed app
   * - tightly coupled
   * - single artifacts
   * - all the benefits of clustering: availability, fault tolerance etc.
   *
   * When NOT to use clustering?
   * - for internal communication inside one microservice
   * - services are separated, different artifacts
   * - distributed monolith
   */
}
