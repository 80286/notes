package remoting2

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Address, Props}
import akka.cluster.{Cluster, ClusterEvent}
import akka.pattern.pipe
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import com.typesafe.config.ConfigFactory
import scala.util.Random
import akka.actor.ReceiveTimeout
import com.typesafe.config.Config
import akka.dispatch.UnboundedPriorityMailbox
import akka.dispatch.PriorityGenerator

object ClusteringExampleDomain {
  case class ProcessFile(filename: String)
  case class ProcessLine(line: String, aggregator: ActorRef)
  case class ProcessLineResult(countOfWords: Int)
}

class ClusterWordCountPriorityMailbox(settings: ActorSystem.Settings, config: Config)
extends UnboundedPriorityMailbox(
  PriorityGenerator {
    // Handle cluster events first:
    case _: ClusterEvent.MemberEvent => 0
    case other => 1
  })

class Master extends Actor with ActorLogging {
  import ClusteringExampleDomain._

  private val cluster = Cluster(context.system)

  private var workers: Map[Address, ActorRef] = Map.empty
  private var pendingRemoval: Map[Address, ActorRef] = Map.empty

  private implicit val ec: ExecutionContext = context.dispatcher

  override def preStart(): Unit = {
    log.info("Subscribring cluster events...")
    cluster.subscribe(
      self,
      initialStateMode = ClusterEvent.InitialStateAsEvents,
      classOf[ClusterEvent.MemberEvent],
      classOf[ClusterEvent.UnreachableMember]
      )
  }

  override def postStop(): Unit =
    cluster.unsubscribe(self)

  override def receive: Receive =
    handleClusterEvents
      .orElse(handleWorkerRegistration)
      .orElse(handleJob)

  private def handleClusterEvents: Receive = {
    case ClusterEvent.MemberUp(member) if member.hasRole("worker") =>
      log.info(s"Member (worker) is up: ${member.address}")

      if (pendingRemoval.contains(member.address)) {
        pendingRemoval = pendingRemoval - member.address
      } else {
        implicit val timeout = Timeout(3 seconds)

        context
          .actorSelection(s"${member.address}/user/workerActor")
          .resolveOne()
          .map(ref => (member.address -> ref))
          .recoverWith {
            case ex: Exception =>
              log.info(ex.getMessage())
              Future.failed(ex)
          }
          .pipeTo(self)
      }

    case ClusterEvent.UnreachableMember(member) =>
      log.info(s"Member is unreachable: ${member.address}")

      workers.get(member.address).foreach { ref => 
        pendingRemoval = pendingRemoval + (member.address -> ref)
      }

    case ClusterEvent.MemberRemoved(member, prevStatus) =>
      log.info(s"Member removed: ${member.address} after previous status: $prevStatus")

    case otherEvent: ClusterEvent.MemberEvent =>
      log.info(s"Member event received: $otherEvent")
  }

  private def handleWorkerRegistration: Receive = {
    case pair: (Address, ActorRef) =>
      log.info(s"Registering worker: $pair")
      workers = workers + pair
  }

  private def handleJob: Receive = {
    case ProcessFile(filename) =>
      val aggregator = context.actorOf(Props[Aggregator], "aggregator")

      scala.io.Source.fromFile(filename).getLines().foreach { line =>
        self ! ProcessLine(line, aggregator)
        // We've moved line processing to separate Receive section
        // because it allows actor to handle also new incoming cluster events
        // work can be dispatched to new cluster members that appeared in meantime
      }

    case ProcessLine(line, aggregator) =>
      val readyWorkers = (workers -- pendingRemoval.keys)
      val workerIdx = Random.nextInt(readyWorkers.size)
      val worker = readyWorkers.values.toSeq(workerIdx)

      worker ! ProcessLine(line, aggregator)
      Thread.sleep(10)
  }
}

class Aggregator extends Actor with ActorLogging {
  import ClusteringExampleDomain._

  // If actor doesn't receive any messages for 3 seconds, then ActorSystem will send Timeout msg to it:
  context.setReceiveTimeout(3.seconds)

  override def receive: Receive =
    online(totalCount = 0)

  private def online(totalCount: Int): Receive = {
    case ProcessLineResult(count) =>
      context.become(online(totalCount = totalCount + count))

    case ReceiveTimeout =>
      log.info(s"Total count: $totalCount")

      // Disable receive timeout set on start:
      context.setReceiveTimeout(Duration.Undefined)
  }
}

class Worker extends Actor with ActorLogging {
  import ClusteringExampleDomain._

  override def receive: Receive = {
    case ProcessLine(line, aggregator) =>
      log.info(s"Worker is processing [$line]")
      aggregator ! ProcessLineResult(line.split(" ").length)
  }
}

object ClusteringExampleSeedNodes extends App {
  import ClusteringExampleDomain._

  def createNode(port: Int, role: String, props: Props, actorName: String): ActorRef = {
    val config = ConfigFactory.parseString(
      s"""
        akka.cluster.roles = ["${role}"]
        akka.remote.artery.canonical.port = ${port}
       """.stripMargin
      ).withFallback(ConfigFactory.load("remoting2/clusteringExample.conf"))

    val as = ActorSystem("clusteringExample", config)

    as.actorOf(props, actorName)
  }

  val master = createNode(port = 2551, role = "master", Props[Master], "masterActor")
  createNode(port = 2552, role = "worker", Props[Worker], "workerActor")
  createNode(port = 2553, role = "worker", Props[Worker], "workerActor")

  Thread.sleep(10000)
  master ! ProcessFile("src/main/scala/remoting2/ClusteringExample.scala")

  Thread.sleep(10000)
}

// If: you will run ExtraWorker app while first app is processing input file
// Then: you should see in logs that new cluster worker is engaged into processing file
object ClusteringExampleExtraWorker extends App {
  import ClusteringExampleSeedNodes.createNode

  createNode(port = 2554, role = "worker", Props[Worker], "extraWorker")
}
