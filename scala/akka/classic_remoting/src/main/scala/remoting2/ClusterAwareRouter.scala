package remoting2

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import _root_.akka.routing.FromConfig
import com.typesafe.config.ConfigFactory

case class Task(content: String)
case object StartWork

class MasterWithRouter extends Actor with ActorLogging {
  private val router =
    context.actorOf(FromConfig.props(Props[SimpleRoutee]), "clusterAwareRouter")

  override def receive: Receive = {
    case StartWork =>
      log.info("Starting work...")

      (1 to 100).foreach { id =>
        router ! Task(s"task-$id")
      }
  }
}

class SimpleRoutee extends Actor with ActorLogging {
  override def receive: Receive = {
    case Task(content) =>
      log.info(s"Processing Task($content)")
  }
}

object ClusterAwareRoutersApp extends App {
  def startRouteeNode(port: Int) = {
    val config = ConfigFactory
      .parseString(s"akka.remote.artery.canonical.port = $port")
      .withFallback(ConfigFactory.load("remoting2/clusterAwareRouters.conf"))

    val as = ActorSystem("ClusterAwareRoutersSystem", config)
    // Intentionally - there is no creation of actors here
  }

  startRouteeNode(port = 2551)
  startRouteeNode(port = 2552)
}

object ClusterAwareRoutersMasterApp extends App {
  val mainConfig = ConfigFactory.load("remoting2/clusterAwareRouters.conf")
  val config = mainConfig.getConfig("masterWithRouterApp").withFallback(mainConfig)

  val as = ActorSystem("ClusterAwareRoutersSystem", config)
  val masterActor = as.actorOf(Props[MasterWithRouter], "master")

  Thread.sleep(10000)
  masterActor ! StartWork
}

object ClusterAwareGroupRoutersApp extends App {
  def startRouteeNode(port: Int) = {
    val config = ConfigFactory
      .parseString(s"akka.remote.artery.canonical.port = $port")
      .withFallback(ConfigFactory.load("remoting2/clusterAwareRouters.conf"))

    val as = ActorSystem("ClusterAwareRoutersSystem", config)
    as.actorOf(Props[SimpleRoutee], "worker")
  }

  startRouteeNode(port = 2551)
  startRouteeNode(port = 2552)
}
