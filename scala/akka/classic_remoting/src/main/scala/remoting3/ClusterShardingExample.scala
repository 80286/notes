package remoting3

import java.util.Date
import akka.actor.{Actor, ActorRef, ActorLogging, ActorSystem, Props}
import akka.cluster.sharding.ShardRegion
import com.typesafe.config.ConfigFactory
import akka.cluster.sharding.ClusterSharding
import akka.cluster.sharding.ClusterShardingSettings
import scala.util.Random
import java.util.UUID
import scala.concurrent.duration._
import akka.actor.ReceiveTimeout

case class EntryCard(id: String, amount: Int)

case class EntryAttempt(card: EntryCard, date: Date)
case object EntryAccepted
case class EntryRejected(reason: String)

// Passivation msg:
case object TerminateValidator

class Turnstile(validator: ActorRef) extends Actor with ActorLogging {
  override def receive: Receive = {
    case card: EntryCard =>
      validator ! EntryAttempt(card, new Date())

    case EntryAccepted =>
      log.info(s"Entry accepted, please pass.")

    case EntryRejected(reason) =>
      log.info(s"Entry rejected, reason: $reason")
  }
}

object Turnstile {
  def props(validator: ActorRef): Props =
    Props(new Turnstile(validator))
}

class CardValidator extends Actor with ActorLogging {
  /* Assume that validator stores enormous amount of data here */

  override def preStart(): Unit = {
    super.preStart()
    log.info("Starting validator...")

    context.setReceiveTimeout(10 seconds)
  }

  override def receive: Receive = {
    case EntryAttempt(card @ EntryCard(cardId, amount), _) =>
      log.info(s"Validating card $card...")

      if(amount > 2)
        sender() ! EntryAccepted
      else
        sender() ! EntryRejected(reason = s"[${cardId}] Not enough founds")

    case ReceiveTimeout =>
      // After 10s timeout we're sending Passivate msg to our parent (Shard Region)
      // From this moment no more messages will be received by this actor
      context.parent ! ShardRegion.Passivate(stopMessage = TerminateValidator)

    case TerminateValidator =>
      // We received stop msg, we won't get any messages - it'd be good to stop the actor:
      context.stop(self)
  }
}

// Cluster Sharding settings:
object TurnstileSettings {
  val numberOfShards = 10
  val numberOfEntities = 100

  /* Remember: there must be no situation when two different messages m1, m2 for which:
   * extractEntityId(m1) == extractEntityId(m2) && extractShardId(m1) == extractShardId(m2)
   */

  val extractEntityId: ShardRegion.ExtractEntityId = { // ExtractEntityId expands to: PartialFunction[Msg, (EntityId, Msg)]
    case attempt @ EntryAttempt(EntryCard(cardId, _), _) =>
      val entityId = cardId.hashCode.abs % numberOfEntities

      (entityId.toString, attempt)
  }

  val extractShardId: ShardRegion.ExtractShardId = { // ExtractShardId expands to: Msg ⇒ ShardId
    case EntryAttempt(EntryCard(cardId, _), _) =>
      (cardId.hashCode.abs % numberOfShards).toString

    // Used only when ClusterShardingSettings enables withRememberEntities(true):
    case ShardRegion.StartEntity(entityId) =>
      // This message will be sent to the ShardRegion when it takes rsp for new shard
      // which was previously moved
      (entityId.toLong % numberOfShards).toString
  }
}

// Cluster nodes
class TubeStationApp(port: Int, numberOfTurnstiles: Int) extends App {
  val config = ConfigFactory
    .parseString(s"akka.remote.artery.canonical.port = $port")
    .withFallback(ConfigFactory.load("remoting3/clusterShardingExample.conf"))

  val as = ActorSystem("ClusterShardingExampleCluster", config)

  // Cluster Sharding setup:
  val validatorShardRegion = ClusterSharding(as).start(
    typeName = "CardValidator",
    entityProps = Props[CardValidator],
    settings = ClusterShardingSettings(as),
    extractEntityId = TurnstileSettings.extractEntityId,
    extractShardId = TurnstileSettings.extractShardId
  )

  val turnstiles = (1 to numberOfTurnstiles).map(_ => as.actorOf(Turnstile.props(validatorShardRegion)))

  // Take some time for cluster initialization, sleep 10s:
  Thread.sleep(10000)

  // Send random card requests to random turnstiles:
  (1 to 1000).foreach { _ =>
    val randomTurnstile = turnstiles(Random.nextInt(numberOfTurnstiles))

    randomTurnstile ! EntryCard(id = UUID.randomUUID().toString, amount = Random.nextInt(10))
    Thread.sleep(200)
  }
}

object TubeStation1 extends TubeStationApp(port = 2551, numberOfTurnstiles = 10)
object TubeStation2 extends TubeStationApp(port = 2561, numberOfTurnstiles = 4)
object TubeStation3 extends TubeStationApp(port = 2571, numberOfTurnstiles = 20)

/* 
 * - Exercise 1 (test): start all 3 station apps at the same time
 * - Exercise 2: start 3 apps, then stop one of them, observe logs:
 *   requests will be balanced between remaining nodes, app is still able to correctly handle card requests
 *
 *   Same situtation takes place when one of the nodes crashes.
 *   When a node crashes then:
 *   - Shard Coordinator will distribute dead shard regions into working nodes
 *   - new entities will be created if needed
 *   - messages to dead shards are buffered
 *
 *   Q: what happens when crash happens on the node with Shard Coordinator?
 *   A: it's simply moved to another node since it's Cluster Singleton, all SC messages are buffered (akka.cluster.sharding.buffer-size = 100000)
 *
 */
