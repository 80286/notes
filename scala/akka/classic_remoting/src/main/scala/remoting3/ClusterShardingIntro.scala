package remoting3

/*
 * Cluster Sharding
 *
 * Goals:
 * - split big actor into smaller actors of same type in the cluster
 *
 * How it works:
 * - assume splitting an actor of type A into many smaller actors of same type (called 'entities')
 * - potentially many instances can work on the same node
 * - an instance of actor (entity) has an unique ID
 *
 * <Every node in the cluster> starts <Cluster Sharding> for an <Actor Type>
 * - every node starts a special <Shard Region> actor
 * - every <Shard Region> is responsible for <Shard ID>
 * - special <Shard Coordinator> starts as <Cluster singleton>
 *    + coordinates which <Shard ID> is located on some node
 *    + responsible for migration of shards between nodes
 *
 * Every message:
 * - is sent to the <Shard Region> on the local node
 * - local <Shard Region> will map msg to <Shard ID> and <Entity ID>
 * - local <Shard Region> will ask <Shard Coordinator> for the destination node
 * - <Shard Region> will forward msg to the correct node and entity
 *
 * =========================
 * Visualisation:
 * - Assume 5 nodes in the cluster, every node starts <Cluster Sharding>.
 * - New <Shard Region> will be assigned to each node.
 * - On the one of the nodes <Shard Coordinator> will be created.
 * - On Node3 SR receives message
 * - Node3 SR will calculate desination of the msg by calling 2 identyfing functions
 * - Assume that SR identified dst region as: [Shard 2]; [Entity 3]
 * - SR asks SC on which <Node> [Shard 2] region is located
 * - SC responds to the SR, for example: Node4
 * - SR of Node3 will forward this message to the SR of dst Node4
 * - At this point Node4 doesn't have [Entity 3] as a child - so it will create an entity for it and then will forward msg to this entity
 *
 * - Node1 {
 *    [ShardRegion]
 *    [ShardCoordinator]
 *    [Entity 0]
 *    [Entity 1]
 *  }
 * - Node2 {
 *    [ShardRegion]
 *    [Entity 11]
 *  }
 * - Node3 {
 *   [ShardRegion]
 *   [Entity 59]
 *   [Entity 27]
 * }
 * - Node4 {
 *   [ShardRegion]
 *   [Entity 3]
 * }
 * - Node5 {
 *   [ShardRegion]
 *   [Entity 19]
 *}
 * =========================
 * Other Cluster Sharding features:
 * <Shard Rebalancing>:
 * - when shard has too many entities, they can be migrated to other nodes (similar to handover in <Cluster Singleton>)
 * - during rebalancing messages for <Shard Region> are buffered until recover
 * - state of the entities is NOT recovered (use for example Akka Persistence instead)
 *
 * <Shard Passivation>:
 * - when entity is passive (doesn't receive any messages) for a while then it's stopped to avoid OutOfMemory errors
 * - entity sends <Passivate> msg to its parent ShardRegion
 * - Shard Region will create new entities if needed
 */
