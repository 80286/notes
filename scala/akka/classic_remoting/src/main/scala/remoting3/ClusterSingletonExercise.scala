package remoting3

import java.util.UUID
import scala.util.Random
import akka.actor.{Actor, ActorRef, ActorLogging, ActorSystem, Props}
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings, ClusterSingletonProxy, ClusterSingletonProxySettings}
import scala.concurrent.duration._
import akka.actor.ReceiveTimeout
import com.typesafe.config.ConfigFactory
import akka.actor.PoisonPill

case class Person(id: String, age: Int)

object Person {
  def generate() =
    Person(UUID.randomUUID().toString, 16 + Random.nextInt(30))
}

case class Vote(person: Person, candidate: String)
case object VoteAccepted
case class VoteRejected(reason: String)

class VotingAggregator extends Actor with ActorLogging {
  private val candidates = Set("X", "Y", "Z", "V")

  context.setReceiveTimeout(20 seconds)

  override def receive: Receive =
    online(personsWhoVoted = Set.empty, polls = Map.empty)

  private def offline: Receive = {
    case v: Vote =>
      log.warning("Vote received after valid time")
      sender() ! VoteRejected("Timeout")

    case other =>
      log.warning(s"Received other msg: $other - ignoring")
      sender() ! VoteRejected("Unknown msg")
  }

  private def online(personsWhoVoted: Set[String], polls: Map[String, Int]): Receive = {
    case Vote(Person(id, age), candidate) =>
      if(personsWhoVoted.contains(id))
        sender() ! VoteRejected(s"$id has already voted")
      else if(age < 18)
        sender() ! VoteRejected(s"$id is not above legal voting age")
      else if(!candidate.contains(candidate))
        sender() ! VoteRejected(s"Invalid candidate: $candidate")
      else {
        log.info(s"Recording vote for $id for $candidate...")

        val candidateVotes = polls.getOrElse(candidate, 0)
        sender() ! VoteAccepted

        context.become(online(personsWhoVoted + id, polls + (candidate -> (candidateVotes + 1))))
      }

    case ReceiveTimeout =>
      log.info(s"20 seconds passed - actual poll results")

      context.setReceiveTimeout(Duration.Undefined)

      polls.map { case (candidate, numOfVotes) =>
        log.info(s"Candidate [$candidate] = $numOfVotes")
      }

      context.become(offline)
  }
}

class VotingStation(votingAggregator: ActorRef) extends Actor with ActorLogging {
  override def receive: Receive = {
    case v: Vote =>
      votingAggregator ! v

    case VoteAccepted =>
      log.info("Vote accepted")

    case VoteRejected(reason) =>
      log.info(s"Vote rejected: $reason")
  }
}

object VotingStation {
  def props(votingAggregator: ActorRef): Props =
    Props(new VotingStation(votingAggregator))
}

object CSExerciseElectionSystemApp extends App {
  def startNode(port: Int) = {
    val config = ConfigFactory
      .parseString(s"akka.remote.artery.canonical.port = $port")
      .withFallback(ConfigFactory.load("remoting3/clusteringSingletonExercise.conf"))

    val as = ActorSystem("singletonCluster", config)

    val singletonManager = as.actorOf(ClusterSingletonManager.props(
      singletonProps = Props[VotingAggregator],
      terminationMessage = PoisonPill,
      ClusterSingletonManagerSettings(as)
    ), "votingAggregator")

    Thread.sleep(50000)
  }

  (2551 to 2553).foreach(startNode)
}

class CSExerciseVotingStationApp(port: Int) extends App {
  val config = ConfigFactory
    .parseString(s"akka.remote.artery.canonical.port = $port")
    .withFallback(ConfigFactory.load("remoting3/clusteringSingletonExercise.conf"))

  val as = ActorSystem("singletonCluster", config)

  val votingAggregatorProxy = as.actorOf(ClusterSingletonProxy.props(
    singletonManagerPath = "/user/votingAggregator",
    settings = ClusterSingletonProxySettings(as)
  ))

  val votingStation = as.actorOf(VotingStation.props(votingAggregatorProxy))

  println("Type your candidate (X/Y/Z/V):")
  scala.io.Source.stdin.getLines().foreach { line =>
    votingStation ! Vote(Person.generate(), line)
  }

  Thread.sleep(50000)
}

object CSExerciseVotingPlaceApp1 extends CSExerciseVotingStationApp(2561)
object CSExerciseVotingPlaceApp2 extends CSExerciseVotingStationApp(2562)
object CSExerciseVotingPlaceApp3 extends CSExerciseVotingStationApp(2563)

/* Testing:
 *
 * - run at same time instance using 'sbt run' of:
 * - CSExerciseElectionSystemApp (E)
 * - CSExerciseVotingPlaceApp1   (P1)
 * - CSExerciseVotingPlaceApp2   (P2)
 * - CSExerciseVotingPlaceApp3   (P3)
 * - type some candidates using stdin in launched apps P1, P2, P3
 * - wait 20 seconds for vote summary
 *
 * Instances in cluster per port:
 * - E:  2551, 2552, 2553
 * - P1: 2561
 * - P2: 2562
 * - P3: 2563
 */
