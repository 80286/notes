package remoting3

import akka.actor.{Actor, ActorRef, ActorSystem, ActorLogging, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings, ClusterSingletonProxy, ClusterSingletonProxySettings}
import com.typesafe.config.ConfigFactory
import java.util.UUID
import scala.concurrent.duration._
import scala.util.Random

/*
 * Cluster singleton
 *
 * Goals:
 * - ensure that single instance of an actor is running in the cluster
 *
 * ClusterSingletonManager
 * - ensures deployment of the singleton
 * - guarantees single instance per cluster
 * - is started as early as possible on every node
 *
 * Created singleton actor:
 * - is the child of ClusterSingletonManager
 * - runs on oldest running node
 * - is moved to the next oldest running node if current one leaves (handover process)
 *
 * Communication with singleton is done using instance of: ClusterSingletonProxy
 * - proxy knows where the singleton actually is
 * - is a mediator of singleton actor
 * - buffers incoming messages during singleton handover
 *
 * When to use singleton?
 * - single entry in app
 * - any action that has to be centralized
 * - single point of responsibility
 * 
 * Drawbacks:
 * - vulnerable for high loads of input
 * - there is no guarantee that singleton will be available during handover, it may take some time
 */

case class Order(items: List[String], total: Double)
case class Transaction(orderId: Int, transactionId: String, amount: Double)

class PaymentSystem extends Actor with ActorLogging {
  log.info(s"PaymentSystem actor started ${self.path}...")

  override def receive: Receive = {
    case t: Transaction =>
      log.info(s"Validating transaction: $t")

    case other =>
      log.info(s"Received other msg: $other")
  }
}

class PaymentSystemNodeApp(port: Int) extends App {
  val config = ConfigFactory
    .parseString(s"akka.remote.artery.canonical.port = $port")
    .withFallback(ConfigFactory.load("remoting3/clusterSingletonExample.conf"))

  val as = ActorSystem("singletonCluster", config)

  as.actorOf(
    props = ClusterSingletonManager.props(
      singletonProps = Props[PaymentSystem],
      terminationMessage = PoisonPill,
      ClusterSingletonManagerSettings(as)
    ),
    name = "paymentSystem"
  )

  Thread.sleep(50000)
}

object Node1App extends PaymentSystemNodeApp(port = 2551)
object Node2App extends PaymentSystemNodeApp(port = 2552)
object Node3App extends PaymentSystemNodeApp(port = 2553)

/* Test 1:
 *
 * - run app1 into background (oldest node)
 * - run app2 into background
 * - run app3 into background (newest node)
 * - so we have 3 nodes (actor systems) inside one cluster
 * - one singleton paymentSystem actor will be created
 * - stop first app
 *
 * Notice:
 * app2 node as second oldest will become new leader
 * cluster singleton was moved from node1 to node2
 * you can see and check in the logs of app2 that new singleton actor was created there
 * thanks to our cluster singleton mgr
 * */

class OnlineShopCheckout(paymentSystem: ActorRef) extends Actor with ActorLogging {
  private var orderId = 0

  override def receive: Receive = {
    case Order(_, totalAmount) =>
      log.info(s"Received order: $orderId for amount: $totalAmount - sending transaction to validate...")

      paymentSystem ! Transaction(orderId, UUID.randomUUID().toString, totalAmount)
      orderId += 1
  }
}

object OnlineShopCheckout {
  def props(paymentSystem: ActorRef): Props =
    Props(new OnlineShopCheckout(paymentSystem))
}

object PaymentSystemClientApp extends App {
  val config = ConfigFactory
    .parseString("akka.remote.artery.canonical.port = 0")
    .withFallback(ConfigFactory.load("remoting3/clusterSingletonExample.conf"))

  val as = ActorSystem("singletonCluster", config)

  val proxy = as.actorOf(
      ClusterSingletonProxy.props(
        singletonManagerPath = "/user/paymentSystem",
        settings = ClusterSingletonProxySettings(as)
      ),
    name = "paymentSystemProxy"
  )

  val onlineShopCheckout = as.actorOf(OnlineShopCheckout.props(paymentSystem = proxy))
  import as.dispatcher

  as.scheduler.schedule(5.seconds, 1.second, () => {
    val randomOrder = Order(List(), Random.nextDouble() * 10)

    onlineShopCheckout ! randomOrder
  })
}

/* Test 2:
 *
 * - run app1 into background (oldest node)
 * - run app2 into background
 * - run app3 into background (newest node)
 * - run PaymentSystemClientApp, its actor system will join cluster as node with random port
 * - scheduler will start sending of random orders
 * - singleton created on the oldest node will be logging reception of orders
 * - stop app1 and observe handover in logs - how app2 node is taking over reception of orders
 * - stop app2 and notice how cluster mgr moved singleton to the app3 node
 */
