package iot

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.wordspec.AnyWordSpecLike

class DeviceTest extends ScalaTestWithActorTestKit with AnyWordSpecLike {
  import Device._

  private val deviceId = "device"
  private def getTestDeviceActor = spawn(Device("group", deviceId))

  "Device actor" must {
    "reply with None if temperature was not set" in {
      val probe = createTestProbe[RespondTemperature]()
      val deviceActor = getTestDeviceActor

      deviceActor ! Device.ReadTemperature(10, probe.ref)
      probe.expectMessage(RespondTemperature(10, deviceId, None))
    }

    "reply with correct value if temperature was set" in {
      val inputTemp = 5.0
      val deviceActor = getTestDeviceActor

      val recordRequestId = 1
      val recordProbe = createTestProbe[TemperatureRecorded]()
      deviceActor ! Device.RecordTemperature(recordRequestId, inputTemp, recordProbe.ref)

      val recordResponse = recordProbe.receiveMessage()
      recordResponse.requestId should === (recordRequestId)

      val readRequestId = 2
      val readProbe = createTestProbe[RespondTemperature]()

      deviceActor ! Device.ReadTemperature(readRequestId, readProbe.ref)
      readProbe.expectMessage(RespondTemperature(readRequestId, deviceId, Some(inputTemp)))
    }
  }
}
