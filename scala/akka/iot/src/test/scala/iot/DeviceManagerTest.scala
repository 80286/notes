package iot

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.wordspec.AnyWordSpecLike

class DeviceManagerTest extends ScalaTestWithActorTestKit with AnyWordSpecLike {
  import Device._

  private def getTestDeviceManager = spawn(DeviceManager())

  private def deviceId(idx: Int): String = s"d$idx"
  private def groupId(idx: Int): String = s"g$idx"

  "Device manager actor" must {
    "should return empty list of actors after initialization" in {
      val probe = createTestProbe[DeviceManagerProtocol.ResponseDeviceList]()
      val manager = getTestDeviceManager

      manager ! DeviceManagerProtocol.RequestDeviceList(1, "", probe.ref)
      probe.expectMessage(DeviceManagerProtocol.ResponseDeviceList(requestId = 1, ids = Seq.empty))
    }

    "create group after request" in {
      val probe = createTestProbe[DeviceManagerProtocol.DeviceRegistered]()
      val manager = getTestDeviceManager

      val numOfGroups = 3
      val numOfDevicesInGroup = 3

      (1 to numOfGroups).foreach { groupIdx =>
        (1 to numOfDevicesInGroup).foreach { deviceIdx =>
          manager ! DeviceManagerProtocol.RequestTrackDevice(groupId(groupIdx), deviceId(deviceIdx), probe.ref)
          probe.expectMessageType[DeviceManagerProtocol.DeviceRegistered]
        }
      }

      val probeGroupResponse = createTestProbe[DeviceManagerProtocol.ResponseDeviceList]()
      (1 to numOfGroups).foreach { groupIdx =>
        manager ! DeviceManagerProtocol.RequestDeviceList(1, groupId(groupIdx), probeGroupResponse.ref)

        val expectedIds = ((1 to numOfDevicesInGroup).map(deviceId).toSeq)
        probeGroupResponse.expectMessage(DeviceManagerProtocol.ResponseDeviceList(requestId = 1, ids = expectedIds))
      }
    }

    "reply with correct value if temperature was set" in {
      val inputTemp = 5.0
      val deviceId = "device"
      val deviceActor = spawn(Device("group", deviceId))

      val recordRequestId = 1
      val recordProbe = createTestProbe[TemperatureRecorded]()
      deviceActor ! Device.RecordTemperature(recordRequestId, inputTemp, recordProbe.ref)
      recordProbe.expectMessage(Device.TemperatureRecorded(recordRequestId))

      val readRequestId = 2
      val readProbe = createTestProbe[RespondTemperature]()
      deviceActor ! Device.ReadTemperature(readRequestId, readProbe.ref)
      readProbe.expectMessage(Device.RespondTemperature(readRequestId, deviceId, Some(inputTemp)))
    }
  }
}
