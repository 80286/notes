package iot

import org.scalatest.wordspec.AnyWordSpecLike
import scala.concurrent.duration._
import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.testkit.typed.scaladsl.TestProbe
import akka.actor.typed.ActorRef
import iot.{DeviceGroupQuery, DeviceGroupQueryProtocol}

class DeviceGroupQueryTest extends ScalaTestWithActorTestKit with AnyWordSpecLike {
  private def createCommandProbe = createTestProbe[Device.Command]()
  private def getDeviceId(idx: Int) = s"device-$idx"

  private def wrapResponseForAllDevices(
    queryActor: ActorRef[DeviceGroupQuery.Command],
    deviceIdToProbe: Map[String, TestProbe[Device.Command]],
    deviceIdToActualTemp: Map[String, Option[Double]]) = {

    deviceIdToProbe.keys.foreach { deviceId =>
      val tempValue = deviceIdToActualTemp(deviceId)

      queryActor ! DeviceGroupQuery.WrappedRespondTemperature(
        Device.RespondTemperature(
          requestId = 0,
          deviceId = deviceId,
          value = tempValue
        )
      )
    }
  }

  private def doTest(
    deviceIdToActualTemp: Map[String, Option[Double]],
    deviceIdToExpectedTemp: Map[String, DeviceGroupQueryProtocol.TemperatureReading]) = {

    assert(deviceIdToExpectedTemp.size == deviceIdToActualTemp.size)

    val deviceIds = deviceIdToActualTemp.keys
    val requester = createTestProbe[DeviceGroupQueryProtocol.RequestAllTemperaturesResponse]()

    val deviceIdToProbe = deviceIds.map(_ -> createCommandProbe).toMap
    val deviceIdToActorRef = deviceIdToProbe.map { case (deviceId, probe) => deviceId -> probe.ref }

    val queryActor = spawn(DeviceGroupQuery(
      DeviceGroupQuerySettings(
        deviceIdToActorRef,
        requestId = 1,
        requester.ref,
        timeout = 3.seconds)
      )
    )

    deviceIdToProbe.values.foreach { deviceProbe =>
      deviceProbe.expectMessageType[Device.ReadTemperature]
    }

    deviceIdToProbe.keys.foreach { deviceId =>
      val tempValue = deviceIdToActualTemp(deviceId)

      queryActor ! DeviceGroupQuery.WrappedRespondTemperature(
        Device.RespondTemperature(
          requestId = 0,
          deviceId = deviceId,
          value = tempValue
        )
      )
    }

    requester.expectMessage(
      DeviceGroupQueryProtocol.RequestAllTemperaturesResponse(
        requestId = 1,
        temperatures = deviceIdToExpectedTemp
      )
    )
  }

  private def doNotAvailableDeviceTest(
    deviceIdToActualTemp: Map[String, Option[Double]],
    deviceIdToExpectedTemp: Map[String, DeviceGroupQueryProtocol.TemperatureReading]) = {

    assert(deviceIdToExpectedTemp.size == deviceIdToActualTemp.size)

    val deviceIds = deviceIdToActualTemp.keys
    val requester = createTestProbe[DeviceGroupQueryProtocol.RequestAllTemperaturesResponse]()

    val deviceIdToProbe = deviceIds.map(_ -> createCommandProbe).toMap
    val deviceIdToActorRef = deviceIdToProbe.map { case (deviceId, probe) => deviceId -> probe.ref }

    val queryActor = spawn(DeviceGroupQuery(
      DeviceGroupQuerySettings(
        deviceIdToActorRef,
        requestId = 1,
        requester.ref,
        timeout = 3.seconds)
      )
    )

    deviceIdToProbe.values.foreach { deviceProbe =>
      deviceProbe.expectMessageType[Device.ReadTemperature]
    }

    deviceIdToActualTemp.foreach { case (deviceId, actualTemp) =>
      actualTemp match {
        case tempValue @ Some(_) =>
          queryActor ! DeviceGroupQuery.WrappedRespondTemperature(
            Device.RespondTemperature(
              requestId = 0,
              deviceId = deviceId,
              value = tempValue
            )
          )

        case _ =>
          deviceIdToProbe(deviceId).stop()
      }
    }

    requester.expectMessage(
      DeviceGroupQueryProtocol.RequestAllTemperaturesResponse(
        requestId = 1,
        temperatures = deviceIdToExpectedTemp
      )
    )
  }

  private def doDeviceTimeoutTest(
    deviceIdToExpectedTemp: Map[String, DeviceGroupQueryProtocol.TemperatureReading]) = {
    val deviceIds = deviceIdToExpectedTemp.keys
    val requester = createTestProbe[DeviceGroupQueryProtocol.RequestAllTemperaturesResponse]()

    val deviceIdToProbe = deviceIds.map(_ -> createCommandProbe).toMap
    val deviceIdToActorRef = deviceIdToProbe.map { case (deviceId, probe) => deviceId -> probe.ref }

    val queryActor = spawn(DeviceGroupQuery(
      DeviceGroupQuerySettings(
        deviceIdToActorRef,
        requestId = 1,
        requester.ref,
        timeout = 1.seconds)
      )
    )

    deviceIdToProbe.values.foreach { deviceProbe =>
      deviceProbe.expectMessageType[Device.ReadTemperature]
    }

    requester.expectMessage(
      DeviceGroupQueryProtocol.RequestAllTemperaturesResponse(
        requestId = 1,
        temperatures = deviceIdToExpectedTemp
      )
    )
  }

  "return correct temperatures for working devices" in {
    val deviceIdToActualTemp = Map(
      getDeviceId(1) -> Some(1.0),
      getDeviceId(2) -> Some(2.0),
      getDeviceId(3) -> Some(3.0)
    )
    val deviceIdToExpectedTemp = Map(
      getDeviceId(1) -> DeviceGroupQueryProtocol.Temperature(1.0),
      getDeviceId(2) -> DeviceGroupQueryProtocol.Temperature(2.0),
      getDeviceId(3) -> DeviceGroupQueryProtocol.Temperature(3.0)
    )

    doTest(deviceIdToActualTemp, deviceIdToExpectedTemp)
  }

  "return correct temperatures for devices with no readings" in {
    val deviceIdToActualTemp = Map(
      getDeviceId(1) -> Some(1.0),
      getDeviceId(2) -> Some(2.0),
      getDeviceId(3) -> None
    )
    val deviceIdToExpectedTemp = Map(
      getDeviceId(1) -> DeviceGroupQueryProtocol.Temperature(1.0),
      getDeviceId(2) -> DeviceGroupQueryProtocol.Temperature(2.0),
      getDeviceId(3) -> DeviceGroupQueryProtocol.TemperatureNotAvailable
    )

    doTest(deviceIdToActualTemp, deviceIdToExpectedTemp)
  }

  "return DeviceNotAvailable if device stops before answering" in {
    val deviceIdToActualTemp = Map(
      getDeviceId(1) -> Some(1.0),
      getDeviceId(2) -> None
    )
    val deviceIdToExpectedTemp = Map(
      getDeviceId(1) -> DeviceGroupQueryProtocol.Temperature(1.0),
      getDeviceId(2) -> DeviceGroupQueryProtocol.DeviceNotAvailable
    )

    doNotAvailableDeviceTest(deviceIdToActualTemp, deviceIdToExpectedTemp)
  }

  "return DeviceTimeout if devices doesn't respond" in {
    val deviceIdToExpectedTemp = Map(
      getDeviceId(1) -> DeviceGroupQueryProtocol.DeviceTimedOut,
      getDeviceId(2) -> DeviceGroupQueryProtocol.DeviceTimedOut
    )

    doDeviceTimeoutTest(deviceIdToExpectedTemp)
  }
}
