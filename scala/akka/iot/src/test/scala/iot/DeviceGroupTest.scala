package iot

import org.scalatest.wordspec.AnyWordSpecLike
import scala.concurrent.duration._
import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.testkit.typed.scaladsl.TestProbe
import akka.actor.typed.ActorRef

class DeviceGroupTest extends ScalaTestWithActorTestKit with AnyWordSpecLike {
  import DeviceGroup._

  sealed case class TestGroup(
    actors: Seq[Device.DeviceActor],
    groupActor: ActorRef[DeviceGroupProtocol.Command],
    probe: TestProbe[DeviceManagerProtocol.DeviceRegistered])

  private val groupId = "group"

  private def getNonUniqueDeviceId(idx: Int) = "deviceId"
  private def getUniqueDeviceId(idx: Int) = s"device-$idx"

  private def createGroupAndTrackDevices(numOfDevices: Int, getDeviceId: Int => String) = {
    val probe: TestProbe[DeviceManagerProtocol.DeviceRegistered] = createTestProbe[DeviceManagerProtocol.DeviceRegistered]()
    val groupActor: ActorRef[DeviceGroupProtocol.Command] = spawn(DeviceGroup(groupId))

    val actors: IndexedSeq[Device.DeviceActor] = (1 to numOfDevices).map { idx =>
      groupActor ! DeviceManagerProtocol.RequestTrackDevice(groupId, getDeviceId(idx), probe.ref)

      val registered = probe.receiveMessage()
      val deviceActor = registered.device

      deviceActor
    }

    TestGroup(actors, groupActor, probe)
  }

  "DeviceGroup" must {
    "be able to register new device" in {
      val group = createGroupAndTrackDevices(4, getUniqueDeviceId)

      assert(group.actors.toSet.size == group.actors.size)
    }

    "ignore requests for wrong groupId" in {
      val probe = createTestProbe[DeviceManagerProtocol.DeviceRegistered]()
      val groupActor = spawn(DeviceGroup(groupId))

      val notCreatedGroupId = "???"
      groupActor ! DeviceManagerProtocol.RequestTrackDevice(notCreatedGroupId, "device-0", probe.ref)
      probe.expectNoMessage(500.milliseconds)
    }

    "return same ActorRef for 2 requests with same deviceId" in {
      val group = createGroupAndTrackDevices(2, getNonUniqueDeviceId)

      assert(group.actors.toSet.size == 1)
    }

    "return list of actors if requested" in {
      val numOfActorsInGroup = 4

      val group = createGroupAndTrackDevices(numOfActorsInGroup, getUniqueDeviceId)
      val deviceIds = (1 to numOfActorsInGroup).map(getUniqueDeviceId)

      val deviceListProbe = createTestProbe[DeviceManagerProtocol.ResponseDeviceList]()
      group.groupActor ! DeviceManagerProtocol.RequestDeviceList(1, groupId, deviceListProbe.ref)

      val response = deviceListProbe.receiveMessage()
      assert(response.ids == deviceIds)
    }

    "return update list of actors after one of them will be stopped" in {
      val numOfActorsInGroup = 4

      val deviceIdFunc = getUniqueDeviceId _
      val group = createGroupAndTrackDevices(numOfActorsInGroup, deviceIdFunc)

      val deviceListProbe = createTestProbe[DeviceManagerProtocol.ResponseDeviceList]()
      group.groupActor ! DeviceManagerProtocol.RequestDeviceList(1, groupId, deviceListProbe.ref)

      val responseBeforeStop = deviceListProbe.receiveMessage()

      val firstActorFromGroup = group.actors.head
      firstActorFromGroup ! Device.StopDevice

      val groupProbe = group.probe

      groupProbe.expectTerminated(firstActorFromGroup, groupProbe.remainingOrDefault)

      groupProbe.awaitAssert {
        val expectedGroupActorNames = (1 to numOfActorsInGroup).map(deviceIdFunc).tail

        group.groupActor ! DeviceManagerProtocol.RequestDeviceList(2, groupId, deviceListProbe.ref)
        deviceListProbe.expectMessage(DeviceManagerProtocol.ResponseDeviceList(requestId = 2, expectedGroupActorNames))
      }
    }

    "collect temperatures from all devices" in {
      val numOfActorsInGroup = 4
      val deviceIdFunc = getUniqueDeviceId _
      val group = createGroupAndTrackDevices(numOfActorsInGroup, deviceIdFunc)
      val notAllActors = group.actors.tail

      val recordProbe = createTestProbe[Device.TemperatureRecorded]()
      notAllActors.zipWithIndex.foreach { case (deviceActor, idx) =>
        val requestId = idx + 2

        deviceActor ! Device.RecordTemperature(requestId, value = requestId.toDouble, recordProbe.ref)
        recordProbe.expectMessage(Device.TemperatureRecorded(requestId))
      }

      val allTemperaturesProbe = createTestProbe[DeviceGroupQueryProtocol.RequestAllTemperaturesResponse]()
      val requestId = 0

      group.groupActor ! DeviceManagerProtocol.RequestAllTemperatures(
        requestId,
        groupId = groupId,
        replyTo = allTemperaturesProbe.ref)

      allTemperaturesProbe.expectMessage(
        DeviceGroupQueryProtocol.RequestAllTemperaturesResponse(
          requestId,
          temperatures = Map(
            deviceIdFunc(1) -> DeviceGroupQueryProtocol.TemperatureNotAvailable,
            deviceIdFunc(2) -> DeviceGroupQueryProtocol.Temperature(2.0),
            deviceIdFunc(3) -> DeviceGroupQueryProtocol.Temperature(3.0),
            deviceIdFunc(4) -> DeviceGroupQueryProtocol.Temperature(4.0)
          )
        )
      )
    }
  }
}
