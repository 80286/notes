import akka.actor.typed.ActorSystem
import iot.IotSupervisor
import org.slf4j.LoggerFactory

object Main extends App {
  private val logger = LoggerFactory.getLogger(classOf[App])

  private def main = {
    logger.info("Main.main()")

    val actorSystem = ActorSystem[Nothing](IotSupervisor(), "iot-actor-system")

    logger.info("~Main.main()")
  }

  main
}