package iot

import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors, LoggerOps}
import akka.actor.typed.{PostStop, Behavior, ActorRef, Signal}

object DeviceManagerProtocol {
  trait Command

  case class RequestTrackDevice(
    groupId: String,
    deviceId: String,
    replyTo: ActorRef[DeviceRegistered])
  extends DeviceManagerProtocol.Command
  with DeviceGroupProtocol.Command

  case class DeviceRegistered(device: ActorRef[Device.Command])

  case class RequestDeviceList(
    requestId: Long,
    groupId: String,
    replyTo: ActorRef[ResponseDeviceList])
  extends DeviceManagerProtocol.Command
  with DeviceGroupProtocol.Command

  case class ResponseDeviceList(
    requestId: Long,
    ids: Seq[String])

  case class DeviceGroupTerminated(groupId: String) extends DeviceManagerProtocol.Command

  case class RequestAllTemperatures(
    requestId: Long,
    groupId: String,
    replyTo: ActorRef[DeviceGroupQueryProtocol.RequestAllTemperaturesResponse])
  extends DeviceManagerProtocol.Command
  with DeviceGroupProtocol.Command
}

object DeviceManager {
  def apply(): Behavior[DeviceManagerProtocol.Command] = {
    Behaviors.setup(context => new DeviceManager(context))
  }
}

class DeviceManager(context: ActorContext[DeviceManagerProtocol.Command]) 
      extends AbstractBehavior[DeviceManagerProtocol.Command](context) {
  import DeviceManager._

  var groupIdToActor = Map.empty[String, ActorRef[DeviceGroupProtocol.Command]]

  context.log.info("DeviceManager started.")

  override def onMessage(msg: DeviceManagerProtocol.Command): Behavior[DeviceManagerProtocol.Command] = {
    msg match {
      case req @ DeviceManagerProtocol.RequestTrackDevice(groupId, _, replyTo) =>
        groupIdToActor.get(groupId) match {
          case Some(deviceGroupActor) =>
            context.log.info(s"DeviceGroup $groupId exists - forwarding RequestTrackDevice msg to device group actor")
            deviceGroupActor ! req

          case None =>
            context.log.info(s"DeviceGroup $groupId doesn't exist, creating new DeviceGroup actor")

            val deviceGroupActor = context.spawn(DeviceGroup(groupId), groupId)
            context.watchWith(deviceGroupActor, DeviceManagerProtocol.DeviceGroupTerminated(groupId))

            deviceGroupActor ! req
            groupIdToActor += groupId -> deviceGroupActor

        }
        this

      case req @ DeviceManagerProtocol.RequestDeviceList(requestId, groupId, replyTo) =>
        context.log.info(s"DeviceManager received $req")
        groupIdToActor.get(groupId) match {
          case Some(ref) =>
            context.log.info(s"DeviceGroup $groupId exists - asking groupDeviceActor for list of devices")
            ref ! req

          case None =>
            context.log.info(s"DeviceGroup $groupId doesn't exist, replying with empty list of devices")
            replyTo ! DeviceManagerProtocol.ResponseDeviceList(requestId, Seq())
        }
        this

      case req @ DeviceManagerProtocol.DeviceGroupTerminated(groupId) =>
        context.log.info(s"DeviceManager received $req - unregistering requested DeviceGroup")
        groupIdToActor -= groupId
        this
    }
    this
  }

  override def onSignal: PartialFunction[Signal, Behavior[DeviceManagerProtocol.Command]] = {
    case PostStop =>
      context.log.info("DeviceManager received PostStop")
      this
  }

}
