package iot

import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors, LoggerOps}
import akka.actor.typed.{ActorRef, Behavior, Signal, PostStop}

object Device {
  sealed trait Command

  final case class ReadTemperature(requestId: Long, replyTo: ActorRef[RespondTemperature]) extends Command
  final case class RespondTemperature(requestId: Long, deviceId: String, value: Option[Double])

  final case class RecordTemperature(requestId: Long, value: Double, replyTo: ActorRef[TemperatureRecorded]) extends Command
  final case class TemperatureRecorded(requestId: Long)

  case object StopDevice extends Command

  def apply(groupId: String, deviceId: String): Behavior[Command] = {
    Behaviors.setup(context => new Device(context, groupId, deviceId))
  }

  type DeviceActor = ActorRef[Command]
}

class Device(
  context: ActorContext[Device.Command],
  groupId: String,
  deviceId: String)
      extends AbstractBehavior[Device.Command](context) {

  import Device._

  private def getTitle: String = s"Device($groupId, $deviceId)"

  context.log.info(s"${getTitle} started")

  private var lastTemperature: Option[Double] = None

  override def onMessage(msg: Command): Behavior[Command] = {
    msg match {
      case req @ Device.ReadTemperature(id, replyTo) =>
        context.log.info(s"$req received; Current temp: $lastTemperature")
        replyTo ! Device.RespondTemperature(id, deviceId, lastTemperature)
        this

      case req @ Device.RecordTemperature(id, value, replyTo) =>
        context.log.info(s"$req received. Updating temp: $lastTemperature -> $value")

        lastTemperature = Some(value)
        replyTo ! Device.TemperatureRecorded(id)
        this

      case StopDevice =>
        Behaviors.stopped
    }
  }

  override def onSignal: PartialFunction[Signal, Behavior[Command]] = {
    case PostStop =>
      context.log.info(s"${getTitle} received PostStop signal")
      this
  }
}
