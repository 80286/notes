package iot

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.Behavior
import scala.concurrent.duration.FiniteDuration
import akka.actor.typed.scaladsl.AbstractBehavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.TimerScheduler

object DeviceGroupQueryProtocol {
  trait TemperatureReading
  case class Temperature(value: Double) extends TemperatureReading
  case object TemperatureNotAvailable extends TemperatureReading
  case object DeviceNotAvailable extends TemperatureReading
  case object DeviceTimedOut extends TemperatureReading

  trait Command

  case class RequestAllTemperatures(
    requestId: Long,
    groupId: String,
    replyTo: ActorRef[RequestAllTemperaturesResponse]
  ) extends Command
    with DeviceGroupProtocol.Command
    with DeviceManagerProtocol.Command

  case class RequestAllTemperaturesResponse(
    requestId: Long,
    temperatures: Map[String, TemperatureReading]
  )
}

case class DeviceGroupQuerySettings(
  deviceIdToActor: Map[String, ActorRef[Device.Command]],
  requestId: Long,
  requester: ActorRef[DeviceGroupQueryProtocol.RequestAllTemperaturesResponse],
  timeout: FiniteDuration
)

object DeviceGroupQuery {
  trait Command

  case object CollectionTimeout extends Command
  case class WrappedRespondTemperature(response: Device.RespondTemperature) extends Command
  case class DeviceTerminated(deviceId: String) extends Command

  def apply(settings: DeviceGroupQuerySettings): Behavior[Command] = {
    Behaviors.setup { context =>
      Behaviors.withTimers { timers =>
        new DeviceGroupQuery(settings, context, timers)
      }
    }
  }
}

class DeviceGroupQuery(
  settings: DeviceGroupQuerySettings,
  context: ActorContext[DeviceGroupQuery.Command],
  timers: TimerScheduler[DeviceGroupQuery.Command]
  ) extends AbstractBehavior[DeviceGroupQuery.Command](context) {

  context.log.info(s"Starting DeviceGroupQuery for: $settings")

  // Schedule CollectionTimeout message that will be sent after given delay:
  timers.startSingleTimer(
    key = DeviceGroupQuery.CollectionTimeout,
    msg = DeviceGroupQuery.CollectionTimeout,
    delay = settings.timeout)

  private val respondTemperatureAdapter = context.messageAdapter(DeviceGroupQuery.WrappedRespondTemperature.apply)

  settings.deviceIdToActor.foreach {
    case (deviceId, deviceActor) =>
      context.watchWith(deviceActor, DeviceGroupQuery.DeviceTerminated(deviceId))
      deviceActor ! Device.ReadTemperature(requestId = 0, replyTo = respondTemperatureAdapter)
  }

  private var collectedReplies = Map.empty[String, DeviceGroupQueryProtocol.TemperatureReading]
  private var pendingDevices = settings.deviceIdToActor.keySet

  override def onMessage(msg: DeviceGroupQuery.Command): Behavior[DeviceGroupQuery.Command] = msg match {
    case DeviceGroupQuery.WrappedRespondTemperature(response) =>
      onRespondTemperature(response)

    case DeviceGroupQuery.DeviceTerminated(deviceId) =>
      onDeviceTerminated(deviceId)

    case DeviceGroupQuery.CollectionTimeout =>
      onCollectionTimeout

    case msg =>
      context.log.info(s"DeviceGroupQuery received unknown msg: $msg")
      this
  }

  private def onRespondTemperature(response: Device.RespondTemperature): Behavior[DeviceGroupQuery.Command] = {
    val reading = response.value match {
      case Some(value) => DeviceGroupQueryProtocol.Temperature(value)
      case _           => DeviceGroupQueryProtocol.TemperatureNotAvailable
    }

    import response.deviceId
    collectedReplies += (deviceId -> reading)
    pendingDevices   -= deviceId

    respondWhenAllCollected()
  }

  private def onDeviceTerminated(terminatedDeviceId: String): Behavior[DeviceGroupQuery.Command] = {
    if(pendingDevices(terminatedDeviceId)) {
      collectedReplies += (terminatedDeviceId -> DeviceGroupQueryProtocol.DeviceNotAvailable)
      pendingDevices   -= terminatedDeviceId
    }

    respondWhenAllCollected()
  }

  private def onCollectionTimeout: Behavior[DeviceGroupQuery.Command] = {
    collectedReplies ++= pendingDevices.map(deviceId => deviceId -> DeviceGroupQueryProtocol.DeviceTimedOut)
    pendingDevices = Set.empty

    respondWhenAllCollected()
  }

  private def respondWhenAllCollected(): Behavior[DeviceGroupQuery.Command] = {
    if(pendingDevices.isEmpty) {
      context.log.info(s"DeviceGroupQuery collected all ${collectedReplies.size} replies.")

      val response = DeviceGroupQueryProtocol.RequestAllTemperaturesResponse(settings.requestId, collectedReplies)
      context.log.info(s"Sending response to requester: $response")

      settings.requester ! response
      Behaviors.stopped
    } else {
      this
    }
  }
}
