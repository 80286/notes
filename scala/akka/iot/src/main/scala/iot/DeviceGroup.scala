package iot

import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.actor.typed.{Behavior, PostStop, ActorRef, Signal}
import iot.{Device, DeviceManager}
import iot.Device.DeviceActor
import scala.concurrent.duration._

object DeviceGroupProtocol {
  trait Command

  final case class DeviceTerminated(
    device: ActorRef[Device.Command],
    groupId: String,
    deviceId: String
  ) extends Command
}

object DeviceGroup {
  def apply(groupId: String): Behavior[DeviceGroupProtocol.Command] = {
    Behaviors.setup(context => new DeviceGroup(context, groupId))
  }
}

class DeviceGroup(
    context: ActorContext[DeviceGroupProtocol.Command],
    groupId: String
  ) extends AbstractBehavior[DeviceGroupProtocol.Command](context) {

  context.log.info(s"DeviceGroup '$groupId' started")

  private var deviceIdToActor = Map.empty[String, DeviceActor]

  override def onMessage(msg: DeviceGroupProtocol.Command): Behavior[DeviceGroupProtocol.Command] = {
    context.log.info(s"DeviceGroup '$groupId' received $msg")

    msg match {
      case DeviceManagerProtocol.RequestTrackDevice(`groupId`, deviceId, replyTo) =>
        deviceIdToActor.get(deviceId) match {
          case Some(actorRef) =>
            context.log.info(s"Device $deviceId exists - forwarding DeviceRegistered to Device")
            replyTo ! DeviceManagerProtocol.DeviceRegistered(actorRef)

          case None =>
            context.log.info(s"Device $deviceId doesn't exist - creating new Device for $deviceId")

            val deviceActor = context.spawn(Device(groupId, deviceId), deviceId)

            context.watchWith(deviceActor, DeviceGroupProtocol.DeviceTerminated(deviceActor, groupId, deviceId))

            deviceIdToActor += deviceId -> deviceActor

            replyTo ! DeviceManagerProtocol.DeviceRegistered(deviceActor)
        }
        this

      case req @ DeviceGroupProtocol.DeviceTerminated(_, _, deviceId) =>
        context.log.info(s"Received $req - removing device $deviceId")
        deviceIdToActor -= deviceId
        this

      case req @ DeviceManagerProtocol.RequestDeviceList(requestId, requestedGroupId, replyTo) =>
        if(groupId == requestedGroupId) {
          context.log.info(s"Received $req (actual devices: $deviceIdToActor)")

          replyTo ! DeviceManagerProtocol.ResponseDeviceList(requestId, deviceIdToActor.keys.toSeq)
          this
        } else {
          Behaviors.unhandled
        }

      case req @ DeviceManagerProtocol.RequestAllTemperatures(requestId, requestedGroupId, replyTo) =>
        if(groupId == requestedGroupId) {
          context.log.info(s"Received $req - preparing DeviceGroupQuery")
          context.spawnAnonymous(
            DeviceGroupQuery(DeviceGroupQuerySettings(deviceIdToActor, requestId, replyTo, 3.seconds))
          )
          this
        } else {
          Behaviors.unhandled
        }

      case req =>
        context.log.info(s"Received unknown req: $req")
        this
    }
  }

  override def onSignal: PartialFunction[Signal, Behavior[DeviceGroupProtocol.Command]] = {
    case PostStop =>
      context.log.info(s"DeviceGroup $groupId received PostStop signal")
      this
  }
}
