import zio._
import zio.Console._
import java.io.IOException
import scala.util.Try
import scala.concurrent.Future
import scala.io.{Codec, StdIn, Source}
import java.io.File
import java.io.FileInputStream

/* 5: https://zio.dev/overview/handling-resources/ */
private object HandlingResources {
  // Finalizing
  val finalizer: UIO[Unit] =
    ZIO.succeed(println("test"))

  val finalized: IO[String, Unit] =
    ZIO.fail("failed").ensuring(finalizer)

  // Acquire/release
  def openFile(n: String): Task[File] = ???
  def closeFile(f: File): URIO[Any, Unit] = ???

  ZIO.acquireReleaseWith(openFile("data.json"))(closeFile) { file =>
    ???
  }

  // Same case for classes that implements AutoClosable java interface:
  def openFileInputStream(n: String): Task[FileInputStream] = ???
  def readAllBytes(f: FileInputStream): Array[Byte] = ???
  val a: ZIO[Any, Throwable, Int] = ZIO.scoped {
    for {
      stream <- ZIO.fromAutoCloseable(openFileInputStream("some.file"))
      data   <- ZIO.attemptBlockingIO(readAllBytes(stream))
    } yield data.length
  }
}
