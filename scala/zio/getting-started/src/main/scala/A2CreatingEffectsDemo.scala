import zio._
import zio.Console._
import java.io.IOException
import scala.util.Try
import scala.concurrent.Future
import scala.io.{Codec, StdIn, Source}
import java.net.ServerSocket

/* 2: https://zio.dev/overview/creating-effects
 *
 * ZIO[R, E, A] - R: environment, E: failure type, A: success type
 *
 * Example ZIO effects:
 * - ZIO[HttpRequest, HttpFailure,  HttpSuccess]
 * - ZIO[Connection,  SQLException, ResultSet]
 * - ZIO[Any,         IOException,  Byte]
 *
 * ZIO Effect can be thought as function of type: R => Either[E, A]
 *
 * Type aliases:
 * - UIO [A]      -> ZIO[Any, Nothing,   A]
 * - URIO[R, A]   -> ZIO[R,   Nothing,   A]
 * - Task[A]      -> ZIO[Any, Throwable, A]
 * - RIO [R, A]   -> ZIO[R,   Throwable, A]
 * - IO  [E, A]   -> ZIO[Any, E,         A]
 * */

private object CreatingEffectsDemo {
  val okValue: ZIO[Any, Nothing, Int] =
    ZIO.succeed(2)

  val failValue: IO[String, Nothing] =
    ZIO.fail("test")

  val fromOpt: IO[Option[Nothing], Int] =
    ZIO.fromOption(Some(0))

  val fromOpt2: ZIO[Any, String, Int] =
    ZIO.fromOption(Some(0)).orElseFail("value not set")

  val fromRight: IO[Nothing, Int] =
    ZIO.fromEither(Right(1))

  val fromTry: Task[Int] =
    ZIO.fromTry(Try(10 / 0))

  val fromScalaFuture: Task[Int] =
    ZIO.fromFuture { implicit executionContext =>
      Future(10)
    }

  /* ==================================================================================================== */
  /* Converting synchronous code to ZIO effect: */
  /* ==================================================================================================== */
  val opThatCanThrowException: ZIO[Any, IOException, String] =
    ZIO.attempt(StdIn.readLine()) // Task[String] -> ZIO[Any, Throwable, String]
      .refineToOrDie[IOException] // Zio[Any, IOException, String]

  val opThatCantThrowException: ZIO[Any, Nothing, Unit] =
    ZIO.succeed(println("sth"))

  /* ==================================================================================================== */
  /* Async code with callback based API: */
  /* ==================================================================================================== */
  case class AuthError(error: String)
  case class User(id: String)
  def login(onSuccess: User => Unit, onFail: AuthError => Unit): Unit = ???
  val l: ZIO[Any, AuthError, User] =
    ZIO.async[Any, AuthError, User] { callback =>
      login(
        user => callback(ZIO.succeed(user)),
        authError => callback(ZIO.fail(authError))
      )
    }

  /* ==================================================================================================== */
  /* Blocking synchronous code: */
  /* ==================================================================================================== */
  def downloadFile(url: String): Task[String] =
    ZIO.attempt {
      Source.fromURL(url)(Codec.UTF8).mkString
    }

  def safeDownload(url: String): ZIO[Any, Throwable, String] =
    ZIO.blocking(downloadFile(url))

  val sleepingTask: Task[Unit] = // effect will be executed on ZIO's blocking thread pool
    ZIO.attemptBlocking(Thread.sleep(500))

  // Converting to effect synchronous code that can be cancelled only by invoking some other code:
  def accept(socket: ServerSocket) =
    ZIO.attemptBlockingCancelable(effect = socket.accept())(cancel = ZIO.succeed(socket.close()))
}

