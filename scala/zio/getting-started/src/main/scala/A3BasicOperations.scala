import zio._
import zio.Console._
import java.io.IOException
import scala.util.Try
import scala.concurrent.Future
import scala.io.{Codec, StdIn, Source}

/* 3: https://zio.dev/overview/basic-operations
 *
 * There are 2 types of operations of ZIO effects:
 * - Transformations, for example calling .timeout(60.seconds) on effect will create new one (will apply timeout when effect is executed)
 * - Combinations, such functions can combine one or more effects into single effect, for example: effect1.orElse(effect2)
 */
private object BasicOperations {
  /* ==================================================================================================== */
  /* Mapping */
  /* ==================================================================================================== */

  val ok: ZIO[Any, Nothing, Int] =
    ZIO.succeed(2).map(_ * 2)

  val failed: ZIO[Any, RuntimeException, Nothing] =
    ZIO.fail("Some error").mapError(err => new RuntimeException(err))

  /* ==================================================================================================== */
  /* Chaining */
  /* ==================================================================================================== */

  // If the first effect fails (readLine), then callback used in flatMap will be never invoked
  // and effect returned by flatMap will also fail:
  val sequenced: ZIO[Any, IOException, Unit] =
    Console.readLine.flatMap(line => Console.printLine(s"### $line"))
  // In general, any chain of effects created using flatMap will short circuit the whole chain

  // flatMap is used in for comprehension chains:
  val a: ZIO[Any, IOException, Unit] = for {
    a <- Console.readLine(">")
    _ <- Console.printLine(s"# $a")
  } yield ()

  /* ==================================================================================================== */
  /* Zipping */
  /* ==================================================================================================== */
  val zipped: ZIO[Any, Nothing, (Int, String)] =
    ZIO.succeed(10).zip(ZIO.succeed("test"))

  val idFromUser: ZIO[Any, IOException, String] =
    Console.printLine("Type id").zipRight(Console.readLine)

  val zipRight2: ZIO[Any, IOException, String] =
    Console.printLine("Type sth") *> Console.readLine
}
