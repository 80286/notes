import zio._
import zio.Console._
import java.io.IOException
import scala.util.Try
import scala.concurrent.Future
import scala.io.{Codec, StdIn, Source}

/* 6: https://zio.dev/overview/basic-concurrency */
/* Fibers
 * ----------------------------------------------------------------------------------------------------
 * All effects in ZIO are executed by some fiber.
 *
 * If you did not create the fiber, then fiber was created by:
 * - some operation you are using (if the op is concurrent/parallel)
 * - or by ZIO runtime system
 *
 * Even on single threaded code with no parallel execution there will be at least one main fiber that
 * executes your effect.
 *
 * Every fiber exits with failure or success, depending on related effect result.
 * 
 * Fiber has:
 * - unique identity
 * - stacks (and stacktraces)
 * - local state
 * - status (done/running/suspended)
 * */
private object BasicConcurrency {
  /* ---------------------------------------------------------------------------------------------------
   * Forking fibers
   * ------------------------------------------------------------------------------------------------ */
  def fib(n: Long): UIO[Long] = {
    ZIO.suspendSucceed {
      if(n <= 1)
        ZIO.succeed(n)
      else
        fib(n - 1).zipWith(fib(n - 2))(_ + _)
    }
  }

  /* Forking an effect begins execution on new fiber, giving you ref to new fiber: */
  val fib100Fiber: UIO[Fiber[Nothing, Long]] =
    for {
      fiber <- fib(100).fork
    } yield fiber

  /* Joining fibers */
  val a: ZIO[Any, Nothing, String] = for {
    fiber <- ZIO.succeed("Test").fork
    msg   <- fiber.join // will merge local states of fibers
  } yield msg

  /* Awaiting fibers */
  val b: ZIO[Any, Nothing, Exit[Nothing, String]] = for {
    fiber <- ZIO.succeed("Test").fork
    exit  <- fiber.await // will NOT merge local states
  } yield exit          // exit provides info about how fiber completed

  /* Interrupting fibers */
  val c: ZIO[Any,Nothing,Exit[Nothing,Nothing]] = for {
    fiber <- ZIO.succeed("test").forever.fork
    exit  <- fiber.interrupt
  } yield exit

  /* Composing fibers */
  val d = for {
    fiber1 <- ZIO.fail("A").fork
    fiber2 <- ZIO.succeed("B").fork
    fiber   = fiber1.orElse(fiber2)
    msg    <- fiber.join
  } yield msg

  /* Parallelism
   *
   * - zip        / zipPar          # Zips 2 effects into 1
   * - zipWith    / zipWithPar      # Zips 2 effects into 1
   * - tupled     / tupledPar       # Zips N effects into 1
   * - collectAll / collectAllPar   # Collect from many effects
   * - foreach    / foreachPar      # Effectfully loop over values
   * - reduceAll  / reduceAllPar    # Reduces many values
   * - mergeAll   / mergeAllPar     # Merges many values
   * */

  /* Racing */
  for {
    winner <- ZIO.succeed("A").race(ZIO.succeed("B")) // race multiple effects concurrently
  } yield winner

  /* Timeout */
  val t: ZIO[Any, Nothing, Option[String]] = ZIO.succeed("A").timeout(10.seconds) // will produce effect with option
}
