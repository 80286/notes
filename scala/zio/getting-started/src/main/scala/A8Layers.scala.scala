import zio._
import zio.Console._
import java.io.IOException
import scala.util.Try
import scala.concurrent.Future
import scala.io.{Codec, StdIn, Source}

/*
 * ZLayer[-RIn, +E, +ROut]
 * 
 * Example layer: ZLayer[Socket & Persistance, Throwable, Database] can be thought as func: (Socker, Persistance) => Database
 *
 * Aliases:
 *
 * RLayer   [-RIn, +ROut]  = ZLayer[RIn, Throwable, ROut]
 * ULayer   [+ROut]        = ZLayer[Any, Nothing,   ROut]
 * Layer    [+E, +ROut]    = ZLayer[Any, E,         ROut]
 * URLayer  [-RIn, +ROut]  = ZLayer[RIn, Nothing,   ROut]
 * TaskLayer[+ROut]        = ZLayer[Any, Throwable, ROut]
 *
 */

case class User(name: String, mail: String)

object UserEmailer {
  trait Service {
    def notify(user: User, msg: String): Task[Unit]
  }

  val live: ZLayer[Any, Nothing, Service] = ZLayer.succeed(new Service {
    override def notify(user: User, msg: String): Task[Unit] = ZIO.succeed {
      println(s"[mailer]: Sending mail $msg to $user...")
    }
  })
}

object UserDb {
  trait Service {
    def insert(user: User): Task[Unit]
  }

  val live: ZLayer[Any, Nothing, Service] = ZLayer.succeed(new Service {
    override def insert(user: User): Task[Unit] = ZIO.succeed {
      println(s"[db] Inserting $user...")
    }
  })
}

object UserSubscription {
  class Service(emailer: UserEmailer.Service, db: UserDb.Service) {
    def subscribe(user: User): Task[Unit] = for {
      _ <- db.insert(user)
      _ <- emailer.notify(user, s"Hi ${user.name}")
    } yield ()
  }

  val live = ZLayer.fromZIO {
    for {
      mailerService <- ZIO.service[UserEmailer.Service]
      dbService     <- ZIO.service[UserDb.Service]
    } yield new Service(mailerService, dbService)
  }
}

case class App(us: UserSubscription.Service) {
  def execute: Task[Unit] =
    us.subscribe(User("test", "x"))
}

object App {
  val live: ZLayer[UserSubscription.Service, Nothing, App] = ZLayer.fromFunction(App.apply _)
}

object LayersDemoApp extends ZIOAppDefault {
  override def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] = {

    val a: ZLayer[Any, Nothing, UserEmailer.Service with UserDb.Service]  = UserEmailer.live ++ UserDb.live
    val b: ZLayer[Any, Nothing, UserSubscription.Service]                 = a >>> UserSubscription.live
    val c: ZLayer[Any, Nothing, App]                                      = b >>> App.live

    ZIO.serviceWithZIO[App](_.execute)
      .provideLayer(c)
      .exitCode
  }
}
