import zio._
import zio.Console._
import java.io.IOException
import scala.util.Try
import scala.concurrent.Future
import scala.io.{Codec, StdIn, Source}
import java.net.ServerSocket

/* 1: https://zio.dev/overview/getting-started/ */
object MainApp extends ZIOAppDefault {
  def run = mainAppLogic

  val mainAppLogic: ZIO[Any, IOException, Unit] = for {
    _    <- printLine("Type sth")
    name <- readLine
    _    <- printLine(s"Out: $name")
  } yield ()
}
