import zio._
import zio.Console._
import java.io.IOException
import scala.util.Try
import scala.concurrent.Future
import scala.io.{Codec, StdIn, Source}

/* 7: https://zio.dev/overview/running-effects/ */
private object RunningEffectsViaApp extends ZIOAppDefault {
  def run =
    for {
      _ <- Console.printLine("Type sth:")
      n <- Console.readLine
      _ <- Console.printLine(s"Typed: $n")
    } yield ()
}

private object RunningEffectsViaRuntime {
  val runtime = Runtime.default

  Unsafe.unsafe { implicit unsafe =>
    runtime.unsafe.run(ZIO.attempt(println("A"))).getOrThrowFiberFailure()
  }
}

private object CustomRuntime {
  val runtime: Runtime[Int] =
    Runtime(ZEnvironment[Int](42), FiberRefs.empty, RuntimeFlags.default)
}
