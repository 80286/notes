import zio._
import zio.Console._
import java.io.IOException
import scala.util.Try
import scala.concurrent.Future
import scala.io.{Codec, StdIn, Source}
import java.io.File
import java.io.FileNotFoundException

/* 4: https://zio.dev/overview/handling-errors */
private object HandlingErrors {
  private type Bytes = Array[Byte]

  // Either:
  val a: URIO[Any, Either[String, Nothing]] =
    ZIO
      .fail("error") // IO  [String, Nothing]
      .either        // URIO[Any,    Either[String, Nothing]]

   // Catching all errors:
   val defaultData: Bytes = Array.empty[Byte]
   def openFile(path: String): ZIO[Any, IOException, Bytes] = ???
   val b: ZIO[Any, IOException, Bytes] =
     openFile("primary.json").catchAll { error => 
       for {
         _    <- ZIO.logErrorCause("Unable to open file", Cause.fail(error))
         file <- openFile("secondary.json")
       } yield file
     }

   // Catching some types of errors:
   val c: ZIO[Any, IOException, Bytes] =
     openFile("some.file").catchSome {
        case _: FileNotFoundException =>
          openFile("backup.file")
   }

   // Fallback:
   val d: ZIO[Any, IOException, Bytes] =
     openFile("test").orElse(openFile("backup"))

   // Folding:
   val e1: URIO[Any, Bytes] = openFile("file1").fold(
     ioException => defaultData,
     bytes       => bytes    
   )

   // Folding with effects (foldZIO):
   val e2: ZIO[Any, IOException, Bytes] = openFile("file1").foldZIO(
     ioException => openFile("backup"),
     bytes       => ZIO.succeed(bytes)
   )

   // Retrying:
   val f1: ZIO[Any, IOException, Bytes] =
     openFile("f").retry(Schedule.recurs(5))

   val f2 =
     openFile("f").retryOrElse(Schedule.recurs(5), (x: IOException, y: Long) => ZIO.succeed(defaultData))
}
