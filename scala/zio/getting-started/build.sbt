scalaVersion := "2.13.8"

name := "zio-getting-started"
organization := "com.zelatyna"
version := "1.0"

libraryDependencies += "dev.zio" %% "zio" % "2.0.6"
// libraryDependencies += "dev.zio" %% "zio-streams" % "2.1.9"
