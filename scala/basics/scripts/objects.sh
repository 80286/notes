#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

object Obj {
  def m(idx: Int) = {
    println("Obj.m(idx=%d)".format(idx))
  }
}

class Browser(var name: String) {
  override def toString = s"Browser(name='${name}')"
}

// Companion object - singleton that implements factory methods for Browser class:
object Browser {
  def build(name: String) = {
    var browser = new Browser(name)

    browser.name = browser.name + "?"
    browser
  }
}

object ObjectTest {
  def objTest = {
    println(Obj)
    println(Obj.m _)
    Obj.m(0)
  }

  def browserTest = {
    val browser = Browser.build("Test")
    println(browser)
    println(browser.name)

    browser.name = "x"
    println(browser.name)
  }

  def main(args: Array[String]) = {
    objTest
    browserTest
  }
}
