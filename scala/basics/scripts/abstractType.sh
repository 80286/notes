#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

/*
 * Programowanie w języku Scala / Moduł 4. Parametryzacja typów i typy generyczne / Cz. 3
 * https://www.youtube.com/watch?v=sQb7aATWCDU&list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&index=15
 */

class TestClass1 {
  /* MyType has to be subtype of List[String]: */
  type MyType <: List[String]

  def f(v: MyType) = v.length
}

object AbstractTypeExample {
  def showBasicExample(idx: Int) = {
    /* +T - covariant */
    abstract class Buffer[+T] {
      val element: T
    }

    abstract class SeqBuffer[U, T <: Seq[U]] extends Buffer[T] {
      def length = element.length
      override def toString = element.toString
    }

    object AbstractSeqBufferTest {
      /* variable list of args: */
      def apply(elems: Int*) = {
        /* creating instance of anonymous class: */
        new SeqBuffer[Int, Seq[Int]] {
          val element = List() ::: elems.toList
        }
      }
    }

    println("%d: %s".format(idx, AbstractSeqBufferTest(1, 2, 3)))
  }

  def showAnonymousAbstractTypeExample(idx: Int) = {
    abstract class TestClassWithAbstractType {
      type T
      val element: T
    }

    /* Create instance of anonymous object.
     * We have to set all missing informations to create object of such class: */
    val b = new TestClassWithAbstractType {
      type T = Int
      val element = 5
    }

    println("%d: %s; element: %s".format(idx, b, b.element))
  }

  def showPathDependentTypesExample(idx: Int) = {
    // abstract class Service {
    //   abstract class Logger {
    //     def log(msg: String): Unit
    //   }

    //   val logger: Logger
    //   def run = logger.log("Starting logger for: %s".format(this.getClass))
    //   protected def doRun: Boolean
    // }

    // object MyService1 extends Service {
    //   class MyService1Logger extends Logger {
    //     def log(msg: String) = println("Logging msg in MyService1: %s".format(msg))
    //   }

    //   override val logger = new MyService1Logger
    //   def doRun = true
    // }

    // object MyService2 extends Service {
    //   class MyService2Logger extends Logger {
    //     def log(msg: String) = println("Logging msg in MyService2: %s".format(msg))
    //   }

    //   // below assignment will fail compilation - we have to move def. of abstract class Logger outside def. of class Service
    //   override val logger = new MyService1.logger
    //   def doRun = true
    // }
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showBasicExample _,
      showAnonymousAbstractTypeExample _
    )

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
