#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

/* https://en.wikipedia.org/wiki/Duck_typing 
 * "If it walks like a duck and it quacks like a duck, then it must be a duck"
 * */
object DuckTypingExample {
  /* https://youtu.be/sQb7aATWCDU?list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&t=2177 */
  def showBasicExample(idx: Int) = {
    class MyFile {
      def open = println("MyFile.open")
      def close = println("MyFile.close")
      override def toString = "File"
    }

    class MyStream {
      def open = println("MyStream.open")
      def close = println("MyStream.close")
      override def toString = "Stream"
    }

    // alt. version:
    // type T = {def open(): Unit; def close(): Unit}
    // def process(in: T) = {
    // ...

    def process(in: {def open; def close}) = {
      println("processing '%s'...".format(in))
      in.open
      in.close
    }

    def process2(in: {def open; def amen(s: String): String}) = {
      println("process2: %s; in.amen('?') -> '%s'".format(in, in.amen("?")))
    }

    process(new MyFile())
    process(new MyStream())
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showBasicExample _
    )

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
