#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

abstract class Person(val id: String, val age: Int)

/*
 * https://alvinalexander.com/scala/how-to-class-superclass-constructor-in-scala-parent-class/
 */

// 2-arg constructor:
class PersonStr(override val id: String, override val age: Int) extends Person(id, age) {
  // 1-arg constructor:
  def this(id: String) = {
    this(id, 1)
  }

  // 0-arg constructor:
  def this() = {
    this("N/A", -1)
  }

  override def toString = "PersonStr[id=%s; age=%d]".format(id, age)
}

class PersonStr2 {
  var id: String = ""
  var age: Int = 1
}

class BaseClass(arg: String) {
  // override def toString = "BaseClass[arg=%s]".format(arg)
  def test = "BaseClass.test"
}

class DerivedClass(arg: String) extends BaseClass(arg) {
  // override def toString = "DerivedClass[arg=%s]".format(arg)
  def test2 = "DerivedClass.test"
}

object ClassTest extends App {
  def showDifferentConstructorExample = {
    val p1 = new PersonStr("0x01", 10)
    val p2 = new PersonStr("0x02")
    val p3 = new PersonStr()

    println(p1.toString())
    println(p2.toString())
    println(p3.toString())
  }

  def showMutableObjectExample = {
    var p = new PersonStr2
    p.id = "?"
    p.age = 1
    println("p4: id=%s; age: %d".format(p.id, p.age))
  }

  def showInheritanceExample = {
    val a = new BaseClass("a")
    println("a1: %s".format(a))

    val b = new DerivedClass("b")
    println("b1: %s".format(b))

    // Cast to base class:
    val c = b.asInstanceOf[BaseClass]
    println("c1: %s".format(c))
    println("c2: %s".format(c.test))

    // below instruction will fail - test2 is not method of BaseClass
    // println(c.test2)
  }

  def showSuperExample = {
    class Color {
      def getName(x: String) = x
    }
    class Red extends Color
    class Pink extends Red {
      val test = super.getName("?")
    }

    val p = new Pink
    println("showSuperExample: p.test=%s".format(p.test))
  }

  /* https://youtu.be/sQb7aATWCDU?list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&t=1827 */
  def showMultiInheritanceExample = {
    class Color {
      def getName(s: String) = s
    }

    trait T1 extends Color {
      override def getName(s: String) = "T1:" + s
    }
    trait T2 extends Color {
      override def getName(s: String) = "T2:" + s
    }

    class Red extends Color
    class Pink extends Red with T1 with T2 {
      val name = super.getName("?")

      // below instructions will call getName from specified base type:
      val nameT1 = super[T1].getName(".")
      val nameT2 = super[T2].getName(".")
      val nameRed = super[Red].getName(".")
    }

    val p = new Pink()
    // below line will print T2:? - so super will call implementation of last base class (T2)
    println("showMultiInheritanceExample: p.name ='%s'; p.nameT1='%s'; p.nameT2='%s'; p.nameRed='%s'".format(p.name, p.nameT1, p.nameT2, p.nameRed))
  }

  // thanks to the base class App below code acts like implementation of "def main(args: Array[String]) = { (...) }"
  showDifferentConstructorExample
  showMutableObjectExample
  showInheritanceExample
  showSuperExample
  showMultiInheritanceExample
}
