#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

object SetsTest {
  def showSetExample(idx: Int) = {
    val a = Set(1, 2, 3, 3, 4)
    println(Set)

    val bList = List(1, 2, 2, 2, 3, 3, 4)
    val bSet = bList.distinct
    println("%d: %s.distinct -> %s".format(idx, bList, bSet))
  }

  def showArrayExample(idx: Int) = {
    val array = Array(1, 2, 3)

    val printArray = (x: Array[Int], s: String) => x.foreach(i => println("%s: %s".format(s, i)))

    printArray(array, "%da".format(idx))

    array.update(0, 15)
    printArray(array, "%db".format(idx))
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showSetExample _,
      showArrayExample _
    )

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
