#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

/*
 * Programowanie w języku Scala / Moduł 4. Parametryzacja typów i typy generyczne / Cz. 1
 * https://www.youtube.com/watch?v=yaAhGD0KSQg&list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&index=14
 */

// begin nonvariant example ----------------------------------------------
trait QueueNonVariant[T] {
  def head: T
  def tail: QueueNonVariant[T]
  def append(item: T): QueueNonVariant[T]
}

// Example nonvariant (default) implementation of queue:
object QueueNonVariant {
  private class QueueImpl[T](elems: List[T]) extends QueueNonVariant[T] {
    def head = elems.head
    def tail = new QueueImpl[T](elems.tail)
    def append(item: T) = new QueueImpl[T](elems ::: List(item))
  }

  // T* stands for variable amount of arguments:
  def apply[T](xs: T*): QueueNonVariant[T] = new QueueImpl[T](xs.toList)
}
// end nonvariant example ------------------------------------------------

// begin covariant example------------------------------------------------
trait QueueVariant[+T] {
  def head[U >: T]: U
  def tail[U >: T]: QueueVariant[U]
  def append[U >: T](item: U): QueueVariant[U]
}

// Example covariant implementation of queue:
object QueueVariant {
  private class QueueImpl[T](elems: List[T]) extends QueueVariant[T] {
    def head[U >: T] = elems.head
    def tail[U >: T] = new QueueImpl[U](elems.tail)
    def append[U >: T](item: U) = new QueueImpl[U](elems ::: List(item))
  }

  // T* stands for variable amount of arguments:
  def apply[T](xs: T*): QueueVariant[T] = new QueueImpl[T](xs.toList)
}
// end covariant example -------------------------------------------------

object TemplateTest {
  def showNonVariantExample(idx: Int) = {
    // Let's define our implementation for type String:
    val q: QueueNonVariant[String] = QueueNonVariant("a")

    // below assignement will produce compilation error:
    // > Note: String <: AnyRef, but trait QueueNonVariant is invariant in type T.
    // > You may wish to define T as +T instead. (SLS 4.5)

    // val q2: QueueNonVariant[AnyRef] = q

    // As we know String class extends AnyRef type
    // but we're not able to assign Queue[String] to Queue[AnyRef]
    // to allow this we have to apply covariant in definition of our impl. of Queue

    val x: QueueVariant[String] = QueueVariant("x")
    val y: QueueVariant[AnyRef] = x

    println(x)
    println(y)
  }
  
  /* Illustration of problem in java:
   *
   * public static void main(String [] args) {
   *    String a = "a";
   *    Object b = "b";
   *    String [] strings = {a};
   *    Object [] objects = strings;
   *
   *    // below line will raise java.lang.ArrayStoreException
   *    objects[0] = new Integer(1);
   * }
   */
  def showManifestExample(idx: Int) = {
    def getInfo[T](xs: List[T]) = xs.getClass

    def getInfoManifset[T](xs: List[T])(implicit m: Manifest[T]) = {
      val isSubTypeOfAnyRef = m <:< classManifest[AnyRef]
      println("%d: m.isSubTypeOfAnyRef: %s".format(idx, isSubTypeOfAnyRef))

      /* a <:< b - a is subtype of b
       * a =:= b - a has type of b
       */

      "%s: erasure='%s'".format(m.toString, m.erasure)
    }

    println("%d: getInfo(List(1, 2, 3)) = %s".format(idx, getInfo(List(1, 2, 3))))
    println("%d: getInfoManifset(List(1, 2, 3)) = %s".format(idx, getInfoManifset(List(1, 2, 3))))
    println("%d: getInfoManifset(List(1, 2, 'a')) = %s".format(idx, getInfoManifset(List(1, 2, "a"))))
    println("%d: getInfoManifset(List('a')) = %s".format(idx, getInfoManifset(List("a"))))
  }

  def showUpperAndLowerBound(idx: Int) = {
    /* T <: AnyRef -> T must be subtype of AnyVal: */
    def testPrint[T <: AnyRef](value: T)(implicit m: Manifest[T]) = {
      println("%da: OK testPrint(%s); manifest='%s' (is subtype of AnyRef)".format(idx, value, m.toString))
    }

    // OK:
    testPrint("str")

    // error: inferred type arguments [Int] do not conform to method testPrint's type parameter bounds [T <: AnyRef]
    // Below line will not compile, since Int is not subtype of AnyRef (Int inherits from AnyVal)
    // testPrint(10)

    class TestClass(name: String, value: Int)
    class TestClass2(name: String, value: Int) extends TestClass(name, value)

    /* T >: TestClass - T must extends TestClass */
    def testPrint2[T >: TestClass](value: T)(implicit m: Manifest[T]) = {
      println("%db: OK testPrint2(%s); manifest='%s' (extends TestClass)".format(idx, value, m.toString))
    }

    testPrint2(new TestClass("x", 1))
    testPrint2(new TestClass2("y", 2))

    def testPrint3[T >: Int <: AnyVal](value: T)(implicit m: Manifest[T]) = {
      println("%dc: OK testPrint3(%s); manifest='%s'".format(idx, value, m.toString))
    }

    testPrint3(4)
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showNonVariantExample _,
      showManifestExample _,
      showUpperAndLowerBound _
    )

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
