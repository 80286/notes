#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

object JavaCodeTest extends App {
  def showJavaCodeDateExample = {
    // https://alvinalexander.com/scala/scala-import-java-classes-packages-examples/
    import java.time.format.DateTimeFormatter;
    import java.time.LocalDateTime;

    val dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")
    val now = LocalDateTime.now();

    println(dtf.format(now))
  }

  def showJavaCodeFileExample = {
    import java.io.File

    val dirPath = "/tmp"
    val dir = new File(dirPath)
    val files = dir.listFiles()

    println("All %d files from '%s':".format(files.length, dirPath))

    for ((file, idx) <- files.zipWithIndex) println("[%d]: %s".format(idx, file))

    // alternative version of above code:
    // for (i <- (0 to (files.length - 1))) println("[%d]: %s".format(i, files(i)))
  }

  def showReflectionExample = {
    val c = ::

    println("c = :: = %s".format(c))
    println("c = ::.getClass = %s".format(c.getClass))

    val str = ""

    val m = str.getClass.getMethods
    println("String('%s').getClass.getMethods:\n%s".format(str, m.mkString("\n")))
  }


  /*
   * Programowanie w języku Scala / Moduł 4. Parametryzacja typów i typy generyczne / Cz. 2
   * https://www.youtube.com/watch?v=TUkjGpBLaOE&list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&index=14
   */
  def showTypeErasureExample = {
    val list = List(1, 2, 3)

    // below line will produce warning:
    // warning: fruitless type test: a value of type List[Int] cannot also be a List[String] (the underlying of List[String]) (but still might match its erasure)
    // case l:List[String] => println("list of strings")
    //
    // and finally will print:
    // "list of strings"
    // due to the "type erasure" (https://www.baeldung.com/java-type-erasure)

    list match {
      case l:List[String] => println("list of strings")
      case _ => println("list of other type")
    }
  }

  showJavaCodeDateExample
  showJavaCodeFileExample
  showReflectionExample
  showTypeErasureExample
}
