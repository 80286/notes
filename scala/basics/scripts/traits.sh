#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

/*
 * Traits vs Abstract classes:
 * Traits doesn’t comes with constructor. That is the main difference between traits and abstract classes.
 */

object TraitTest {
  /*
   * Programowanie w języku Scala / Moduł 5. Traits oraz niejawne konwersje / Cz. 2
   * https://www.youtube.com/watch?v=kc63XFa8A7w&list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&index=17 */
  def showBasicExample(idx: Int) = {
    abstract class Animal {
      def getName(): String
    }
    
    trait HasLegs {
      /* self type reference - it means that it should be used only by classes that extend from Animal class */
      self: Animal =>

      val legs: List[String]
      def walk = println(getName() + " walking...")
    }

    trait MakeNoise {
      def makeNoise: Unit
    }

    class Frog(name: String) extends Animal with HasLegs with MakeNoise {
      def getName = name
      override val legs = List("1", "2")
      override def makeNoise = println("amen")
    }
  }

  /* https://youtu.be/kc63XFa8A7w?list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&t=314 */
  def showStackableModifications(idx: Int) = {
    trait Queue[T] {
      def append(x: T): Unit
    }

    trait IntQueue extends Queue[Int]

    trait Adder extends IntQueue {
      abstract override def append(x: Int) = super.append(x + 1)
    }

    trait Twicer extends IntQueue {
      abstract override def append(x: Int) = super.append(x * 2)
    }

    class BasicIntQueue(var elems: List[Int]) extends IntQueue {
      override def append(x: Int) = elems = x :: elems
      override def toString = elems.toString
    }

    /* Order of listed traits will impact the order of applied operations (this explains stack in the name) */
    val a = new BasicIntQueue(List(1, 2, 3))
    val b = new BasicIntQueue(List(1, 2, 3)) with Adder
    val c = new BasicIntQueue(List(1, 2, 3)) with Adder with Twicer

    // Will produce compilation error:
    // val d = new BasicIntQueue(List(1, 2, 3)) with Adder with Twicer with Twicer

    def testQueue(q: BasicIntQueue, prefix: String) = {
      println("%d: %s: initial=%s".format(idx, prefix, q))
      q.append(5)
      println("%d: %s: after append: %s".format(idx, prefix, q))
    }

    testQueue(a, "[default]")
    testQueue(b, "[adder]")
    testQueue(c, "[mixed]")
  }

  /* https://youtu.be/RA7-Gy77_Gs?list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&t=66 */
  def showCakePatternExample = {
    class User

    trait UserDAO {
      val dao: UserDaoImpl

      class UserDaoImpl {
        def getUser(username: String): User = {
          println("getting user from database: " + username)
          new User()
        }

        def saveUser(u: User) = println("Saving user to database...")
      }
    }

    trait UserServiceProvider {
      def authenticate(username: String, password: String)
    }

    trait UserService {
      self: UserDAO =>
      val service: UserServiceProvider

      class UserServiceImpl {
        def authenticate(username: String, password: String) = {
          println("Authenticating user: %s...".format(username))
          dao.getUser(username)
        }
      }
    }

    object UserAuth extends UserDAO with UserService {
      class MyUserServiceProvider extends UserServiceProvider {
        override def authenticate(username: String, password: String) = {
          println("MyUserServiceProvider.authenticate(username=%s)...".format(username))
        }
      }

      val service = new UserServiceImpl
      val dao = new UserDaoImpl
    }

    UserAuth.service.authenticate("user", "pass")
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showBasicExample _,
      showStackableModifications _
    )

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
