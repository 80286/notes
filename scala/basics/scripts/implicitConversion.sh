#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

/* 
 * Programowanie w języku Scala / Moduł 5. Traits oraz niejawne konwersje / Cz. 1
 * https://www.youtube.com/watch?v=wy0tFSC_VEg&list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&index=16
 */
object SetsTest {
  def showBasicExample(idx: Int) = {
    class Rational(val nom: Int, val denom: Int) {
      def this(nom: Int) = this(nom, 1)
      override def toString = "%d/%d".format(nom, denom)
      def *(other: Rational) = new Rational(this.nom * other.nom, this.denom * other.denom)
      def *(n: Int) = new Rational(n * this.nom, this.denom)
    }

    val half = new Rational(1, 2)
    val quarter = new Rational(1, 4)

    val result = half * quarter
    println(result)

    println(half * 3)

    /* Int can be multiplied by Rational only when compiler will be able to find implicit conversion function
     * that was defined in local scope: */
    implicit def IntToRational(i: Int): Rational = new Rational(i)
    println(3 * half)

  }

  /* https://youtu.be/wy0tFSC_VEg?list=PL_nu3rOfoPo6tTl6kR2YKGAMWx-4esAxx&t=1558 */
  def showImplicitParamsExample(idx: Int) = {
    case class Person(age: Int, name: String, address: Address)
    class Address(val no: Int, val street: String)

    abstract class Printable {
      def print
    }

    class Label(val name: String) extends Printable {
      def print = println(name)
    }

    def printCard[T](toPrint: List[T])(implicit convert: T => Printable) = {
      toPrint.foreach(_.print)
    }

    /* Alternative version that make use of view bound: */
    // def printCard[T <% Printable](toPrint: List[T]) = {
    //   toPrint.foreach(_.print)
    // }

    implicit val addressToPrintable = (a: Address) => new Printable { def print = println("street=%s; no=%d".format(a.street, a.no)) }
    implicit val personToPrintable = (p: Person) => new Printable { def print = println("age=%d; name=%s; addr=%s".format(p.age, p.name, p.address)) }

    val addr = new Address(1, "Test")
    // val label = new Label("red")

    /* explicit conversion: */
    printCard(List(addressToPrintable(addr)))
    printCard(List(addr))(addressToPrintable)

    /* conversion by implicit functions in local scope: */
    printCard(List(addr))
    printCard(List(new Person(20, "x", new Address(10, "z"))))
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showBasicExample _,
      showImplicitParamsExample _
    )

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
