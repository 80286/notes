#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

/*
 * https://docs.scala-lang.org/tour/case-classes.html
 *
 * Case classes are like regular classes with a few key differences which we will go over.
 * >> Case classes are good for modeling immutable data <<
 */

case class Package(val address: String)

object CaseClassTest {
  def main(args: Array[String]) = {
    // Creating case classes doesn't need 'new' keyword:
    val p1 = Package("addr1")
    val p2 = Package("addr2")

    val printPackage = (prefix: String, p: Package) => "%s: %s: hash: %s".format(prefix, p, p.hashCode())
    println(printPackage("p1", p1))
    println(printPackage("p2", p2))

    val p3 = Package("addr1")
    println(printPackage("p3", p3))

    // true (because hash(p1) == hash(p2))
    println("p1 == p3: " + (p1 == p3))

    // false:
    println("p1 == p2: " + (p1 == p2))

    // true:
    println("p1 == Package(p1.address): " + (p1 == Package(p1.address)))
  }
}
