#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

case class Package(name: String, packageType: String)

object NoneTest {
  def showBasicExample(idx: Int) = {
    // Create map of 3 test packages:
    val packages = (1 to 3).map(i => (i, Package("n%d".format(i), "t%d".format(i)))).toMap

    packages.foreach(el => println("%da: packages[%s] = %s".format(idx, el._1, el._2)))

    val pIdx = 4
    val maybePackage_? : Option[Package] = packages.get(pIdx)
    println("%db: packages[%d] = maybePackage_? = %s".format(idx, pIdx, maybePackage_?))

    maybePackage_? match {
      case Some(p: Package) => println("%dc: Found package: (%s, %s)".format(idx, p.name, p.packageType))
      case None => println("%dc: Found None.".format(idx))
    }

    val elseTest = maybePackage_?.getOrElse(Package("defaultName", "defaultPackageType"))
    println("%dd: getOrElse: %s".format(idx, elseTest))

    // instances of Option can use map method - if value is not present (is None) then map call does nothing
    val packageName_? = maybePackage_?.map(_.name)
    println("%de: maybePackage_?.map(_.name) = %s".format(idx, packageName_?))

    // Below call of get will raise java.util.NoSuchElementException if package was not found
    // val packageName_! = maybePackage_?.map(_.name).get

    val packageName = maybePackage_?.map(_.name).getOrElse(20)
    println("%df: maybePackage_?.map(_.name).getOrElse(20) = %s".format(idx, packageName))
  }

  def showOptionExample(idx: Int) = {
    val packages = List(Package("p1", "t1"))
    val option = packages.headOption

    option match {
      case Some(p) => println("%da: found package: %s".format(idx, p))
      case None => println("%da: none found.".format(idx))
    }

    val newPackage = option.map(p => Package(p.name, "newType"))
    newPackage match {
      case Some(p) => println("%db: found package: %s".format(idx, p))
      case None => println("%db: none found")
    }
  }

  def showNullExample(idx: Int) = {
    // null can be used in Scala but usually it's used only in java code
    val n: Null = null

    val s: String = null

    def f = throw new RuntimeException
  }

  def showUnitExample(idx: Int) = {
    def testMethod : Unit = {
      println("%d: testMethod (returning void)".format(idx))
    }

    val t = testMethod
    println("%d: testMethod returned: %s".format(idx, t))
  }

  def showDefaultValueOfParamExample(idx: Int) = {
    def testMethod(a: Int, b: String, c: Option[String]) : Unit = {
      println("%d: testMethod(a=%s, b='%s', c='%s')".format(idx, a, b, c.getOrElse("-")))

      c match {
        case Some(v) => println("%d: c=%s".format(idx, c))
        case None => println("%d: c=None(%s)".format(idx, c.getOrElse("-")))
      }
    }

    testMethod(1, "a", Some("x"))
    testMethod(1, "a", None)
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showBasicExample _,
      showOptionExample _,
      showNullExample _,
      showUnitExample _,
      showDefaultValueOfParamExample _
    )

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
