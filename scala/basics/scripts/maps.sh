#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

object MapTest {
  type MapAlias = Map[String, List[Map[Int, String]]]
  val m: MapAlias = Map()

  def showBasicMapExample(idx: Int) = {
    val m = Map(
      1 -> "a", 
      2 -> "b",
      3 -> "c")

    println("%sa: %s".format(idx, m))
    println("%sb: %s(2) -> %s".format(idx, m, m(2)))
    println("%sc: %s.get(2) -> %s".format(idx, m, m.get(2)))

    val n = m.map(el => el._2 -> el._1)
    println("%sd: swap keys and values: %s".format(idx, n))

    val listOfTuples = (1 to 3).map(i => i -> i).toList
    println("%se: %s.toMap: %s".format(idx, listOfTuples, listOfTuples.toMap))

    val ot = (4 -> "d")
    val o = m + ot
    println("%sf: %s + %s: %s".format(idx, m, ot, o))
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showBasicMapExample _
    )

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
