#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

object PatternMatchingExample {
  def doTestMatch(inputItem: Any) = {
    inputItem match {
      case "test" => "_a"
      case "/" => "_b"
      case list @ List(1, 2) => "matched list: " + list.toString
      case _ => "default-value"
    }
  }

  def showBasicExample(idx: Int) = {
    val inputItems = List("test", "/", "axzx", List(1, 2))

    inputItems.foreach(x => println("%d: doTestMatch(%s) -> %s".format(idx, x, doTestMatch(x))))
  }

  def showExample2(idx: Int) = {
    case class Address(street: String, no: Int)
    case class Person(age: Int, name: String, address: Address)

    val testData = Map(
      "a1" -> (Person(1, "a1", Address("addr1", 1))),
      "a2" -> (Person(2, "a2", Address("addr2", 2))),
      "amen" -> "x")

    testData.values.foreach(_ match {
      case p:Person if(p.name.startsWith("a1")) => println("%d: found person (a1): %s".format(idx, p))
      case Person(2, _, _) => println("%d: found person with age = 2".format(idx))
      case Person(age, name, addr) => println("%d: found person: (age, name, addr) = (%s, %s, %s)".format(idx, age, name, addr))
      case x:Any => println("%d: not a person: %s".format(idx, x))
    })
  }

  def showNoneExample(idx: Int) = {
    val matchValue = (v: Any, s: String) => {
      v match {
        case Some(str) => println("%s: found str: %s".format(s, str))
        case None => println("%s: found None".format(idx))
        case _ => println("%s: default match for '%s'".format(s, v))
      }
    }

    val none: Option[String] = None
    matchValue(none, "%da".format(idx))
    matchValue("x", "%db".format(idx))
  }

  def showEitherExample(idx: Int) = {
    val matchValue = (v: Any) => {
      v match {
        case Left(n) => println("%d: found num: %d".format(idx, n))
        case Right(str) => println("%d: found str: %s".format(idx, str))
        case _ => println("%d: default value for: %s".format(idx, v))
      }
    }

    val e1: Either[Int, String] = Left(1)
    val e2: Either[Int, String] = Right("test")

    matchValue(e1)
    matchValue(e2)
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showBasicExample _,
      showExample2 _,
      showNoneExample _,
      showEitherExample _
    )

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
