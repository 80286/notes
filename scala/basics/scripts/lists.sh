#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

object ListTest {
  def showForeachExample(idx: Int) = {
    val a = List(1, 2, 3)
    val b = List("x", "y", "z")

    a.foreach(s => println("%da: %s".format(idx, s.toString())))
    b.foreach(s => println("%db: %s".format(idx, s)))

    println("%dc: first item from %s: %s".format(idx, a, a.head))
    println("%dd: last item from %s: %s".format(idx, a, a.last))
    println("%de: %s.tail: %s".format(idx, a, a.tail))
  }

  def showMapReduceFilterExample(idx: Int) = {
    val a = List(1, 2, 3)
    val b = List("x", "y", "z")
    val idxStr = idx.toString()

    // List(1, 2, 3) -> map -> List(2, 4, 6) -> reduce -> List(2 + 4 + 6) -> 12
    println(idxStr + "a: double vales then sum them: " + a.map(_ * 2).reduce(_ + _))

    // List("x", "y", "z") -> map -> List("xx", "yy", "zz") -> filter -> List("zz")
    println(idxStr + "b: double values then filter zz: " + b.map(_ * 2).filter(s => s == "zz"))

    val c = (1 to 4).toList
    println("%dc: filter even numbers from %s -> %s".format(idx, c, c.filter(_ % 2 == 0)))

    println("%dd: filterNot even numbers from %s -> %s".format(idx, c, c.filterNot(_ % 2 == 0)))

    val d = (1 to 6).toList
    println("%de: find 5 in %s: %s".format(idx, d, d.find(_ == 5)))
    println("%df: find 100 in %s: %s".format(idx, d, d.find(_ == 100)))

    println("%dg: 5 exists in %s: %s".format(idx, d, d.exists(_ == 5)))
    println("%dh: -1 doesn't exist in %s: %s".format(idx, d, d.exists(_ == -1)))
  }

  def showConsExample(idx: Int) = {
    val c = 1 :: 2 :: 3 :: Nil

    println("%d: (1 :: 2 :: 3 :: Nil) -> %s".format(idx, c))
  }

  def showReduceExample(idx: Int) = {
    val a = List(1, 2, 3)

    // List[Int] -> List[Float]
    val d = a.map(_.toFloat)

    // (1.0, 2.0, 3.0) -> reduce -> ((1.0 / 2.0) / 3.0) -> (0.5 / 3.0) -> 0.166(6)
    println("%da: %s.reduceLeft(_ / _) -> %g".format(idx, d, d.reduceLeft(_ / _)))

    // (1.0, 2.0, 3.0) -> reduce -> (1.0 / (2.0 / 3.0)) -> (1.0 / 0.66(6)) -> 1.555(5)
    println("%db: %s.reduceRight(_ / _) -> %g".format(idx, d, d.reduceRight(_ / _)))
  }

  def showToExample(idx: Int) = {
    // 1, 2, 3
    for (i <- (1 to 3)) println("%da: 1 to 3: %d (step=1)".format(idx, i))

    // 1, 3, 5, 7, 9
    for (i <- (1 to(10, 2))) println("%db: 1 to 10 (step=2): %d".format(idx, i))

    // note: method 'contains' doesn't expect type related to the list but expects argument of type Any
    if ((1 to 10).filter((x: Int) => x % 2 == 0).contains(6)) {
      println("%dc: (1..10).filter(even).contains(6) -> true".format(idx))
    }

    println("%dd: 4 exists in 1..5: %s".format(idx, (1 to 5).exists(x => x == 4)))
  }

  def showHeadTailExample(idx: Int) = {
    def reverse(l: List[Int]) : List[Int] = {
      if (l.isEmpty)
        l
      else
        (reverse(l.tail) ++ List(l.head))
    }

    val inList = (1 to 4).toList
    val outList = reverse(inList)
    println("%da: reverse_tail_recursive_custom_impl(%s) -> %s".format(idx, inList, outList))
    println("%db: %s.reverse -> %s".format(idx, inList, inList.reverse))
  }

  def showFlattenExample(idx: Int) = {
    val a = (1 to 3).toList
    val b = (23 to 26).toList
    val c = (30 to 33).toList

    val d = List(a, b, c)
    println("%d: %s.flatten() -> %s".format(idx, d, d.flatten))
  }

  def showFlatMapExample(idx: Int) = {
    case class TC(x: Int, y: Int)

    val a = (1 to 2).map(i => TC(i, 2 * i)).toList
    val b = (10 to 12).map(i => TC(i, 2 * i)).toList
    val c = List(a, b)

    println("%d: %s.flatMap() -> %s".format(idx, c, c.flatMap(_.map(_.x % 2 == 0))))
  }

  def showFoldExample(idx: Int) = {
    val a = (1 to 5).toList

    val b = a.fold(1)(_ * _)
    println("%da: (%s).fold(1)(_ * _) -> %s".format(idx, a, b))

    val c = a.fold(0)(_ + _)
    println("%db: (%s).fold(0)(_ + _) -> %s".format(idx, a, c))
  }

  def showPartitionExample(idx: Int) = {
    val a = (1 to 10).toList

    val b = a.partition(_ % 3 == 0)
    println("%d: %s.partition(_ %% 3 == 0) -> %s".format(idx, a, b))

    val (divisibleBy3, notDivisibleBy3) = b
  }

  def showJoinExample(idx: Int) = {
    val a = (1 to 10).toList

    println("%d: %s.mkString('/') -> %s".format(idx, a, a.mkString("/")))
  }

  def showSortExample(idx: Int) = {
    import java.util.Random
    val r = new Random

    val a = (1 to 5).map(i => r.nextInt.abs % 25).toList
    val b = a.sortWith(_ < _)

    println("%da: %s.sortWith(_ < _) -> %s".format(idx, a, b))
    println("%db: %s.sorted -> %s".format(idx, a, a.sorted))
  }

  def showMutableListExample(idx: Int) = {
    import scala.collection.mutable.ListBuffer

    val lb = ListBuffer(1, 2, 3)
    lb.append(4)
    
    println("%d: %s".format(idx, lb))
  }

  def main(args: Array[String]) = {
    // https://alvinalexander.com/scala/fp-book/how-to-use-scala-methods-like-functions/
    // Convert methods (defined using keyword 'def') to functions:
    val l = List(
      showForeachExample _,
      showMapReduceFilterExample _,
      showConsExample _,
      showReduceExample _,
      showToExample _,
      showHeadTailExample _,
      showFlattenExample _,
      showFlatMapExample _,
      showFoldExample _,
      showPartitionExample _,
      showJoinExample _,
      showSortExample _,
      showMutableListExample _)

    (1 to l.length).foreach(i => l(i - 1)(i))
  }
}
