// 11 Nested methods (https://docs.scala-lang.org/tour/nested-functions.html)

def factorial(n: Int): Int = {
  def f(x: Int): Int = {
    if(x <= 1) {
      1
    } else {
      f(x - 1) * x
    }
  }

  f(n)
}

val values = for(i <- 0 until 6) yield i
values.foreach(println)
values.foreach(x => println(factorial(x)))
