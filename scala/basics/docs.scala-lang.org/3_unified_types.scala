// 3. Unified types (https://docs.scala-lang.org/tour/unified-types.html)

val list: List[Any] = List(
  "Test",
  1,
  0.235,
  "abc",
  () => 45,
  true
)

list.foreach(println)

val x: Long = 987654321
val y: Float = x  // 9.8765434E8
val z: Long = y  // Does not conform

val x: Long = 987654321
val y: Float = x  // 9.8765434E8 (note that some precision is lost in this case)

val face: Char = '☺'
val number: Int = face  // 9786
