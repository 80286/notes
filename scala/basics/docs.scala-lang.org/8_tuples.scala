// 8. Tuples (https://docs.scala-lang.org/tour/tuples.html)

val t = ("abcd", 1)

println(t._1)
prittln(t._2)

val (x, y) = t
println(x)
println(y)

val items = List(
  ("x", 0),
  ("y", 1),
  ("z", 2)
)

items.foreach{
  case ("y", 1) => println("MATCHED")
  case _ =>
}

for((a, b) <- items) {
  println(a)
  println(b)
}
