// 7. Traits (https://docs.scala-lang.org/tour/traits.html)

// ------------------------------------
// Basic usage:
trait Iterator[A] {
  def hasNext: Boolean
  def next: A
}

class IntIterator(val to: Int) extends Iterator[Int] {
  private var current: Int = 0

  override def hasNext: Boolean = {
    current < to
  }

  override def next: Int = {
    if(hasNext) {
      val t = current

      current += 1
      t
    } else {
      0
    }
  }
}

val it = new IntIterator(10)
println(it.next)
println(it.next)

// ------------------------------------
// Subtypes
import scala.collection.mutable.ArrayBuffer

trait Pet {
  val name: String
}

class Cat(val name: String) extends Pet
class Dog(val name: String) extends Pet

val cat = new Cat("x")
val dog = new Dog("y")

val animals = ArrayBuffer.empty[Pet]
animals.append(cat)
animals.append(dog)
animals.foreach(obj => println(obj.name))
