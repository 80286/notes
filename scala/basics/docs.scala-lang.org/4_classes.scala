// 4. Classes (https://docs.scala-lang.org/tour/basics.html)

// Minimal:
class User
var user = new User

// ------------------------------------------
// Example class:
class Point(val x: Int, val y: Int) {
  override def toString(): String = s"($x, $y)"
}

val p = Point(1, 2)
println(p.x)
println(p)

// ------------------------------------------
// Constructor
class Point(var x: Int = 0, var y: Int = 0)

val origin = new Point  // x and y are both set to 0
val point1 = new Point(1)
println(point1.x)  // prints 1

// ------------------------------------------
// Private members
class Point(private var x: Int, private var y: Int) {
  private def test(): Unit = println(s"test: x=$x; y=$y")
  def publicTest() = test
}
val p = new Point(0, 1)
p.publicTest
