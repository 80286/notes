// 12. Multiple parameter lists (Currying) : https://docs.scala-lang.org/tour/for-comprehensions.html

val numbers = for(i <- 0 until 6) yield i
val res = numbers.foldLeft(0)((m, n) => m + n)
println(res)

// Partial application
val numbers = for(i <- 0 until 8) yield i
val numberFunc = numbers.foldLeft(List[Int]()) _

val squares = numberFunc((xs, x) => xs :+ x * x)
println(squares)
