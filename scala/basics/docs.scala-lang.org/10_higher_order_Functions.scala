// 10. Higher-order functions (https://docs.scala-lang.org/tour/higher-order-functions.html)

// 10.1a Usage of map() function:
val salaries = Seq(100, 150, 160)
val doubleValue = (x: Int) => 2 * x

val doubledSalaries = salaries.map(doubleValue)

salaries.foreach(println)
doubledSalaries.foreach(println)

// 10.1b another notation:
val a = Seq(1, 2, 3)
val b = a.map(_ * 2)

// 10.2 Coercing methods into functions

case class TestClass(values: Seq[Double]) {
  def convert(v: Double) = v * 2.0 + 1.0
  def getSeq(): Seq[Double] = values.map(convert)
}

// 10.3 Functions that accept functions

object TestObj {
  private def applyPromotion(salaries: List[Double], f: Double => Double): List[Double] = salaries.map(f)

  def smallPromotion(salaries: List[Double]): List[Double] = applyPromotion(salaries, _ * 1.5)
  def regularPromotion(salaries: List[Double]): List[Double] = applyPromotion(salaries, _ * 1.9)
  def bigPromotion(salaries: List[Double]): List[Double] = applyPromotion(salaries, _ * 2.5)
}

val v = List(1.0, 2.0, 3.0)
TestObj.smallPromotion(v).foreach(println)
TestObj.regularPromotion(v).foreach(println)
TestObj.bigPromotion(v).foreach(println)

// 10.4 Functions that return functions

def urlBuilder(ssl: Boolean, domainName: String) : (String, String) => String = {
  val schema = if (ssl) "https://" else "http://"
  (endpoint: String, query: String) => s"$schema$domainName/$endpoint?$query"
}

val build = urlBuilder(false, "mocz.com")
println(build("test", "a=1"))
