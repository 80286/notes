// 2. Basics (https://docs.scala-lang.org/tour/basics.html)

// ------------------------------------
// 1a. Immutable values (val):
val x = 1
val y: Int = 25

println(x + y)


// ------------------------------------
// 1b. Mutable values (var):
var z: Int = 10
z = 9


// ------------------------------------
// 1c. Blocks (whole block expression will evaluate to last expression)
val x: Int = {
  val z = 1
  val y = 2
  z + y
}

// ------------------------------------
// 1d. Functions
val f1 = (x: Int, y: Int) => x * 2 + y
prinln(f1(1, 0))

val f2 = () => 15
println(f2())

// ------------------------------------
// 1e. Methods
def testMethod(x: Int) = x * 2
println(testMethod(0))

def testMethod1(x: Int, y: Double): String = {
  s"${x}; ${y}"
}

// ------------------------------------
// 1f. Classes
class TestClass(a: String, b: Int) {
  def getString(): String = s"${a} : ${b}"
  def print(): Unit = println(getString)
}

val t = new TestClass("abc", 1)
t.print()

// ------------------------------------
// 1g. Case classes (Instances of case classes are compared by value, not by reference)
case class Point(x: Int, y: Int)

// ------------------------------------
// 1h. Objects (singletons)

object TestApp {
  def run(argv: Array[String]): Unit = {
    argv.foreach(println)
  }
}

var argv = Array[String]("a", "b", "c")
TestApp.run(argv)

// ------------------------------------
// 1i: Traits (interfaces)
trait AbstractRenderer {
  def render: Unit
}

class SoftwareRenderer extends AbstractRenderer {
  override def render: Unit = println("SoftwareRenderer.render()")
}

var r = new SoftwareRenderer()
r.render

// ------------------------------------
// 1j: Main method
object Main {
  def main(args: Array[String]): Unit = println("main_test")
}
