// 6. Named arguments (https://docs.scala-lang.org/tour/named-arguments.html)

def printName(x: String, y: String): Unit = {
  println(s"$x - $y")
}

printName(y="abc", x="def")
printName("abc", "def")
