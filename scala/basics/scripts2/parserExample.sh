#!/bin/sh
exec scala -nocompdaemon -savecompiled "$0" "$@"
!#
// vim: set ft=scala:

/* Check "Programming in Scala" p. 653/754 (Combinator parsing)
 *
 * "..."          - literal
 * "...".r        - regexp
 * P ~ Q          - sequential composition
 * P <~ Q, P ~> Q - sequential composition (keep left/right only)
 * P | Q          - alternative
 * opt(P)         - option
 * rep(P)         - repetition
 * repsep(P, Q)   - interleaved repetition
 * P ^^ f         - result conversion
 */

import scala.util.parsing.combinator._

class NumExprParser extends JavaTokenParsers {
  /*
   * expr   ::= term { "+" term | "-" term }
   * term   ::= factor { "*" factor | "/" factor }
   * factor ::= floatingPointNumber | "(" expr ")"
   */
  def expr: Parser[Any] = term ~ rep("+" ~ term | "-" ~ term)

  def term: Parser[Any] = factor ~ rep("*" ~ factor | "/" ~ factor)

  def factor: Parser[Any] = floatingPointNumber | "(" ~ expr ~ ")"

  def parse(inputStr: String) = {
    println(parseAll(expr, inputStr))
  }
}

class JSONExampleParser extends JavaTokenParsers {
  /*
   * value    ::= obj | arr | stringLiteral | floatingPointNumber | "null" | "true" | "false"
   * obj      ::= "{" [members] "}"
   * arr      ::= "[" [values] "]"
   * members  ::= member { "," member }
   * member   ::= stringLiteral ":" value
   * values   ::= { "," value }
   */

  def value: Parser[Any] = (
                           obj
                         | arr
                         | stringLiteral
                         | floatingPointNumber ^^ (_.toDouble)
                         | "null" ^^ (x => null)
                         | "true" ^^ (x => true)
                         | "false" ^^ (x => false)
                         )

  def obj: Parser[Map[String, Any]] = "{" ~> repsep(member, ",") <~ "}" ^^ (Map() ++ _)

  def arr: Parser[List[Any]] = "[" ~> repsep(value, ",") <~ "]"

  def member: Parser[(String, Any)] = stringLiteral ~ ":" ~ value ^^ { case name ~ ":" ~ value => (name, value) }

  def parse(inputStr: String) = {
    println(parseAll(value, inputStr))
  }
}

object ParserExample {
  def testNumExpr = {
    val inputStr = "2 * (3 + 7)"

    new NumExprParser().parse(inputStr)
  }

  def testJsonExpr = {
    val inputJsonStr = """
    { "a": [ 1, 2, 3],
      "b": { "b1": 4, "b2": 5 }
    }
    """

    new JSONExampleParser().parse(inputJsonStr)
  }

  def main(args: Array[String]) = {
    testNumExpr
    testJsonExpr
  }
}
