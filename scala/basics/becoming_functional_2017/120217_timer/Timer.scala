object Timer {
    /* Funkcja oczekuje 0-argumentowej funkcji nie zwracającej żadnego typu (procedury): */
    def call( callback: () => Unit ) = {

        /* "Thread sleep 1000" oznacza to samo co: Thread.sleep(1000); */
        while(true) { callback(); Thread sleep 1000 }
    }

    def main(args: Array[String]): Unit = {
        /* Przekazujemy funkcję anonimową do metody call: */
        call( { () => println("Amen") } )
    }
}
