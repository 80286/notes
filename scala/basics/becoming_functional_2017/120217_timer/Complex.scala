class Complex( real: Double, imaginary: Double) {
    def re() = this.real
    def im() = this.imaginary
    override def toString() = "(%f, %f)".format(this.real, this.imaginary)
}

object Complex {
    def main(args: Array[String]): Unit = {

        val c1 = new Complex(0.0, 1.0)

        println(c1.toString())

        println("Re: %g; Im: %g".format(c1.re(), c1.im()))
    }
}
