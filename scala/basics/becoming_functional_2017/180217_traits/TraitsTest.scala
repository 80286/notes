
trait Ord {
    def <   (that: Any): Boolean
    def <=  (that: Any): Boolean =  (this < that) || (this == that)
    def >   (that: Any): Boolean = !(this <= that)
    def >=  (that: Any): Boolean = !(this < that)
}

case class Date(y: Int, m: Int, d: Int) extends Ord {
    def year = y
    def month = m
    def day = d

    override def toString() : String = "%d:%d:%d".format(year, month, day)

    override def equals(that: Any): Boolean = that.isInstanceOf[Date] && {
        val obj = that.asInstanceOf[Date]

        obj.day == day && obj.month == month && obj.year == year
    }

    def <(that: Any): Boolean = {
        if(!that.isInstanceOf[Date])
            error("Cannot compare %s and Date".format(that))

        val obj = that.asInstanceOf[Date]
        (year < obj.year) ||
        (year == obj.year && (month < obj.month ||
                (month == obj.month && day < obj.day)))
    }
}

object TraitsTest {
    def main(args: Array[String]) {
        val dates = List(Date(1945, 5, 1), Date(1981, 12, 13), Date(1944, 7, 22)).sorted
    }
}
