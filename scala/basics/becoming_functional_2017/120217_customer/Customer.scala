class Contact ( val email: String) {

    override def toString() : String = {
      "Contact: email='%s'".format(this.email)
    }
}

class Customer(
        val customer_id: Integer,
        val name: String,
        val contact: List[Contact],
        val enabled: Boolean
        ) {

    override def toString() : String = {
      "Customer id=%d; name='%s'; contact=%s".format(this.customer_id, this.name, this.contact.toString())
    }

    def error(msg: String) {
        println(msg)
        None
    }

    (name, contact) match {
        case ("", _) => error("Name cannot be empty.")
        case (_, List()) => error("List of contacts cannot be empty.")
        case _ => new Customer(
        )
    }
}

object Customer {
    def main( args : Array[String] ) = {

        /* Generate list of customers: */
        val customers = List(1, 2, 3, 4).map({ i =>
            new Customer(i, "customer_%d".format(i), List(new Contact("contact_%d@mpo.com".format(i))), true )
        })

        /* Insert function into map method: */
        customers.map({ customer => println(customer.toString()) })

        def printList( inList: List[Customer] ) { inList.map({ obj => println(obj.toString()) }) }

        /* Filter customers with odd identifiers: */
        val oddCustomers = customers.filter( { c => c.customer_id % 2 == 1 } )

        printList( oddCustomers )
    }
}
