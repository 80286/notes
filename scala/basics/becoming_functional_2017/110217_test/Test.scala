/* vim: set syntax=groovy: */

/* Klasy nie posiadają geterów i seterów, przyjmują zestaw właściwości
 * tylko przy tworzeniu obiektu, ponieważ setery i getery 
 * nie są funkcjami czystymi, generują efekty uboczne. */
class Contact(
            val id: Integer,
            val name: String,
            val email: String,
            val enabled: Boolean
            ) {

    override def toString() : String = {
        /* Wyrażenie warunkowe zapisać można tak jak instrukcję if: */
        String.format( "id:%d; name:%s; email:%s; enabled:%s", this.id, this.name, this.email, if (this.enabled) {"yes"} else {"no"} )
    }
}

/* Zwyczajna definicja klasy: */
class Customer(
            val id: Integer,
            val contact: Contact
            ) {

    /* Przeciążony toString(): */
    override def toString() : String = {

        /* Słowo return nie jest wymagane, ostatnia instrukcja jest wartością zwracaną. */
        String.format( "Customer: id: %d; Contact: %s", this.id, this.contact.toString() )
    }
}

/* Tworzona jest pojedyncza instancja obiektu (coś jak singleton): */
object Test {

    /* Unit - synonim słowa void w Javie/C: */
    def main(args : Array[String]): Unit = {

        /*
         * var: zmienna
         * val: stała
         */
        var customer = new Customer( 0, new Contact(0, "contact-0", "contact0@mpo.com", true) )

        /* Średniki są opcjonalne. Wymagane są jeśli w jednej linii jest kilka instrukcji. */
        println("main();");
        
        System.out.println( customer.toString() );

        /* Słowo kluczowe 'lazy': 
         * Wartość testList użyje leniwej(nierygorystycznej) ewaluacji, obliczona
         * zostanie przy pierwszym odwołaniu się do niej. */

        lazy val testList = List(1, 2, 3, 4).map({ i => 2 * i })

        println( String.format("testList: %s", testList.toString()) );
    }
}
