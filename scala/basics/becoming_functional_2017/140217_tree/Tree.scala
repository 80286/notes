/* http://www.scala-lang.org/docu/files/ScalaTutorial.pdf */

/* Case klasy różnią się od zwykłych klas tym, że:
 *  - słowo new nie jest potrzebne przy tworzeniu obiektu
 *  - getery są automatycznie zdefiniowane dla takiej klasy, 
 *      np. dla obiektu klasy Const, możemy pobrać wartość v przez obj.v
 */

abstract class Tree

case class Product(left: Tree, right: Tree) extends Tree
case class Diff(left: Tree, right: Tree) extends Tree
case class Sum(left: Tree, right: Tree) extends Tree
case class Var(n: String) extends Tree
case class Const(v: Int) extends Tree

object Tree {
    /* Definicja typu funkcji(można definiować tylko wewnątrz class/object/trait): */
    type Environment = String => Int

    /* Funkcja ewaluacji wyrażenia:
     *
     * @param expressionTree Wyrażenie w postaci drzewa zbudowanego z węzłów typu Sum, Var, Const.
     * @param env: Funkcja ustalająca zmienne środowiskowe ("nazwa" - wartość zmiennej)
     */
    def eval(expressionTree: Tree, env: Environment) : Int = expressionTree match {
        case Product(leftTree, rightTree) => eval(leftTree, env) * eval(rightTree, env)
        case Diff(leftTree, rightTree) => eval(leftTree, env) - eval(rightTree, env)
        case Sum(leftTree, rightTree) => eval(leftTree, env) + eval(rightTree, env)
        case Var(n) => env(n)
        case Const(v) => v
        /* W razie braku dopasowania, wyrzucony zostanie wyjątek. */
    }

    /* Obliczanie pochodnej względem zmiennej: */
    def derive(expressionTree: Tree, variable: String) : Tree = expressionTree match {
        case Sum(l, r) => Sum(derive(l, variable), derive(r, variable))
        case Var(name) if(name == variable) => Const(1)
        case _ => Const(0)
    }

    def main(args: Array[String]) = {
        val env : Environment = { 
            case "x" => 5 
            case "y" => 6 
        }

        /* (x + 4) + (3 + y) */
        /* val exp: Tree = Sum( Sum(Var("x"), Const(4)), Sum(Const(3), Var("y")) ) */

        /* /1* Zostanie wyświetlone wyrażenie w postaci kodu Scala (Sum jest rodzaju case class): *1/ */
        /* println( exp ) */

        /* /1* Ewaluujemy wyrażenie: *1/ */
        /* println( eval(exp, env) ) */

        /* println( "Derivation with respect to x: %d".format( eval( derive(exp, "x"), env)) ) */
        /* println( "Derivation with respect to y: %d".format( eval( derive(exp, "y"), env)) ) */

        /* (x + 4) * (3 + y) */
        val exp: Tree = Product( Sum(Var("x"), Const(4)), Sum(Const(3), Var("y")) )
        println( eval(exp, env) )
    }
}
