package exclass

import (
    "fmt"
    "log"
)

// Below func will be private due to the lowercase of first letter:
func test() {
    log.SetPrefix("exclass/test.go: test: ")
    log.SetFlags(0)

    log.Println("main()")

    obj := NewExClass()
    log.Printf("new ExClass obj: %p; (obj.id, obj.a) = ('%s', %d); obj.sum: (%g, %g, %t)\n", 
        obj,
        obj.id, obj.a,
        obj.sub.x, obj.sub.y, obj.sub.z)

    obj.DoTestActionMethod()

    if testError, testErrorValue := obj.DoTestError(); testErrorValue == ExClassExampleIntConst {
        log.Printf("obj.DoTestError() -> (%s, %d)\n", testError, testErrorValue)
    } else {
        panic(fmt.Sprintf("%d != %d", testErrorValue, ExClassExampleIntConst))
    }

    obj.DoDeferredExample()

    log.Printf("obj.ExClassExampleConst = '%s'\n", ExClassExampleStrConst)

    obj.DoForRangeExample()

    log.Printf("obj.ToString() -> %s\n", obj.ToString())
}

/* In Go, a name is exported if it begins with a capital letter. */
func Test() {
    test()
}
