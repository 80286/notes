package exclass

import (
    "fmt"
    "errors"
)

type ExFuncType func(in string) string

const (
    ExClassExampleStrConst = "test"
    ExClassExampleIntConst = 25
    ExClassExampleConst
)

type ExSubClass struct {
    x float32
    y float32
    z bool
}

type ExClass struct {
    id  string
    a   int32
    sub ExSubClass
}

func (c *ExClass) DoTestActionMethod() {
    fmt.Printf("ExClass.DoTestActionMethod(this=%p)\n", c)
}

func (c *ExClass) DoTestError() (error, int32) {
    return errors.New("[DoTestError msg error]"), ExClassExampleIntConst
}

func (c *ExClass) DoDeferredExample() {
    defer func() {
        fmt.Println("DoDeferredExample: <this code will be called while leaving scope of this method>")
    }()

    fmt.Println("ExClass.DoDeferredExample()")
}

func buildTestArray(n uint32) []ExFuncType {
    testArray := [] ExFuncType {}

    for i:= 0; i < int(n); i++ {
        f := func(in string) string { return fmt.Sprintf("f(%s)", in) }
        testArray = append(testArray, f)
    }

    return testArray
}

func examplePrivateFunction(n uint32) {
    testArray := buildTestArray(n)

    for i, f := range testArray {
        in := fmt.Sprintf("%d", i)
        out := f(in)

        fmt.Printf("DoForRangeExample: %d, f('%s') -> '%s'\n", i, in, out)
    }
}

func (c *ExClass) DoForRangeExample() {
    examplePrivateFunction(5)
}

func (c *ExClass) ToString() string {
    return fmt.Sprintf("%#v", c)
}

func NewExClass() *ExClass {
    // unlike in C, it's perfectly OK to return the address of a local variable
    return &ExClass{
        "test-id",
        1,
        ExSubClass { 3.0, 4.0, true },
    }
}
