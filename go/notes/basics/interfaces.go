package basics

import (
    "log"
    "fmt"
)

type ExInterface interface {
    String() string
    DoExInterfaceAction(msg string)
}

type AnotherInterface interface {
    DoAnotherInterfaceAction(msg string)
}

type MixedInterface interface {
    ExInterface
    AnotherInterface
}

func PrintObj(obj ExInterface) {
    log.Printf("PrintObj: obj=%p; %#v", obj, obj)

    obj.DoExInterfaceAction("printObj")
}

func CheckObj(value interface{}) {
    if _, ok := value.(ExInterface); ok {
        log.Printf("CheckObj(value): value is of type ExInterface (%#v)", value)
    } else {
        log.Printf("CheckObj(value): unrecognized type (%#v)", value)
    }
}

func ShowMixedInterfaceObjExample(obj MixedInterface) {
    log.Printf("ShowMixedInterfaceObjExample()")

    msg := "interface embedding example"
    obj.DoExInterfaceAction(msg)
    obj.DoAnotherInterfaceAction(msg)
}

type ExClass struct {
    a int32
    b int32
}

func (c *ExClass) DoAnotherInterfaceAction(msg string) {
    log.Printf("DoAnotherInterfaceAction(msg='%s')", msg)
}

func (c *ExClass) String() string {
    return fmt.Sprintf("[%d:%d]", c.a, c.b)
}

func (c *ExClass) DoExInterfaceAction(arg string) {
    log.Printf("DoExInterfaceAction(arg='%s')", arg)
}

func RunInterfacesExamples() {
    log.SetPrefix("basics.interfaces: ")

    obj := &ExClass{1, 2}
    log.Printf("obj=%p", obj)
    PrintObj(obj)

    s := "test"
    CheckObj(s)
    CheckObj(obj)

    ShowMixedInterfaceObjExample(obj)
}
