package basics

import (
    "log"
)

func testFunc(prefix string, slice [] byte) {
    log.Printf("testFunc: slice=%p, cap(slice)=%d\n", slice, cap(slice))

    for i, b := range slice {
        log.Printf("printSliceExample: testFunc: %s: i=%d, s='%c'\n", prefix, i, b)
    }
}

func printSliceExample() {
    array := []byte("abcd efgh zxcv")
    log.Printf("printSliceExample: array=%p, cap(array)=%d\n", array, cap(array))

    testFunc("0:4", array[0:4])
    testFunc("5:9", array[5:9])
}

func printTwoDimSliceExample() {
    text := [][]byte{
        []byte("line1"),
        []byte("line2"),
        []byte("line3"),
    }

    for i, line := range text {
        log.Printf("printTwoDimSliceExample: text[%d] = '%s'\n", i, line)
    }
}

func RunSlicesExamples() {
    log.SetPrefix("basics.slices: ")

    printSliceExample()
    printTwoDimSliceExample()
}
