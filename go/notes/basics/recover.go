package basics

import (
    "log"
)

type Error string

func (e Error) Error() string {
    return string(e)
}

func doExamplePanicCode() {
    panic(Error("example-of-internal-module-error"))
}

func doAction() (ok bool, err Error) {
    ok = true

    defer func() {
        if e := recover(); e != nil {
            ok = false
            err = e.(Error)
        }
    }()
    
    doExamplePanicCode()

    return
}

func RunRecoverExample() {
    log.SetPrefix("basics.recover: ")

    log.Println("Executing unsafe code...")
    if ok, err := doAction(); !ok {
        log.Printf("doAction: recovered panic error msg: %s\n", err)
    }
}
