package basics

import (
    "log"
    "time"
    "runtime"
    "math"
)

func runExample(msg string, delay time.Duration) {
    /* https://go.dev/doc/effective_go#concurrency
     * Unbuffered channels combine communication—the exchange of a value—with synchronization—guaranteeing that two calculations (goroutines) are in a known state. */

    // Allocate unbuffered channel that stores integers:
    c := make(chan int)

    go func () {
        log.Printf("%s: example goroutine started. Sleeping for %d\n", msg, delay)
        time.Sleep(delay)
        log.Printf("%s: example goroutine leaving\n", msg)

        signal := 1
        log.Printf("%s: Broadcasting end of the goroutine...", msg)
        log.Printf("%s: Sending signal=%d to the channel..", msg, signal)
        c <- signal
    }()

    // Wait for the end of goroutine:
    signal := <- c
    log.Printf("main thread: received signal: %d\n", signal)
}

type Vector []float64

func (v Vector) PartialDot(i int, n int, u Vector, c chan float64) {
    var sum float64 = 0

    for ; i < n; i++ {
        sum += v[i] * u[i]
    }

    log.Printf("PartialDot(i=%d; n=%d) -> %g\n", i, n, sum)

    c <- sum
}

func (v Vector) CalcDot(u Vector) float64 {
    prefix := "CalcDot"
    vlen := len(v)

    n := int(math.Min(float64(runtime.NumCPU()), float64(vlen)))

    // Buffered channel:
    c := make(chan float64, n)

    log.Printf("%s: launching %d goroutines...\n", prefix, n)
    for i := 0; i < n; i++ {
        startIdx := i * vlen / n
        endIdx := (i + 1) * vlen / n

        go v.PartialDot(startIdx, endIdx, u, c)
    }

    log.Printf("%s: waiting for goroutines...\n", prefix)
    var dot float64 = 0

    for i := 0; i < n; i++ {
        sum := <- c
        dot += sum
    }

    return dot
}

func runParallelExample() {
    prefix := "runParallelExample"

    v := Vector{ 1.0, 2.0, 3.0, 4.0 }
    u := Vector{ 2.0, 3.0, 4.0, 5.0 }

    var expectedDot float64 = 0.0
    for i, v := range v { expectedDot += v * u[i] }

    log.Printf("%s: computing dot(%#v, %#v)...\n", prefix, u, v)
    producedDot := v.CalcDot(u)
    log.Printf("%s: result: %g (expected=%g)\n", prefix, producedDot, expectedDot)
}

func RunConcurrencyExamples() {
    log.SetPrefix("basics.goroutines: ")

    runExample("goroutine", 2 * time.Second)
    runParallelExample()
}

