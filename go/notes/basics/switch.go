package basics

import "log"

func numToLetter(x uint8) string {
    switch {
    case x < 32:
        return "A"
    case x >= 32 && x < 64:
        return "B"
    case x >= 64 && x < 128:
        return "C"
    }

    return "?"
}

func listSwitchExample(c byte) bool {
    switch(c) {
    case 'a', 'b', '?':
        return true
    }

    return false
}

func forSwitchExample() {
    prefix := "forSwitchExample"

    log.Printf("%s: begin loop", prefix)

    Loop:
        for i := 0; i < 12; i = i + 1 {
            switch(i) {
            case 3, 4, 5:
                log.Printf("%s: 3 <= i <= 5 skip", prefix)
                continue Loop
            case 6, 7:
                log.Printf("%s: i=6 or i=7", prefix)
                break
            case 9:
                log.Printf("%s: i=9; break", prefix)
                break Loop
            }

            log.Printf("%s: i=%d; i++", prefix, i)
        }

    log.Printf("%s: end loop", prefix)
}

func RunSwitchExample() {
    log.SetPrefix("basics.switch: ")

    forSwitchExample()
}
