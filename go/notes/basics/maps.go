package basics

import (
    "log"
)

func doMapExample() {
    prefix := "doMapExample"

    boolStrMap := map[string]bool {
        "a": true,
        "b": true,
        "c": false,
    }

    if !boolStrMap["f"] {
        log.Printf("%s: f is not present in the map\n", prefix)
    }

    _, isPresentD := boolStrMap["d"]
    log.Printf("%s: isPresentD=%t\n", prefix, isPresentD)

    if _, isPresentX := boolStrMap["x"]; !isPresentX {
        log.Printf("%s: isPresentX=%t\n", prefix, isPresentX)
    }

    // remove value under key "b":
    delete(boolStrMap, "b")

    for k, v := range boolStrMap {
        log.Printf("%s: boolStrMap[%s]=%t\n", prefix, k, v)
    }
}

func RunMapExamples() {
    log.SetPrefix("basics.maps: ")

    doMapExample()
}

