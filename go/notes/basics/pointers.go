package basics

import (
    "fmt"
    "log"
)

type testStruct struct {
    a string
    b uint8
}

func init() {
    fmt.Println("init(): initializing pointers.go")
}

func testStructToStr(t *testStruct) string {
    // ptr to testStruct is passed as func arg t
    return fmt.Sprintf("testStruct(%p) = { a='%s'(%p), b=%d(%p) }", t, t.a, &t.a, t.b, &t.b)
}

func testStructFunc(t testStruct) {
    // copy of testStruct obj is passed as func arg t
    log.Printf("functions: testStructFunc(): t=%s\n", testStructToStr(&t))
}

func testArrayFuncNoCopy(array *[]testStruct) {
    // using pointer we don't have to copy all items of array
    log.Printf("testArrayFuncNoCopy(array=%p)", array)
}

func testArrayFuncCopyAllItems(array []testStruct) {
    // all items of array are copied (arrays are values)
    log.Printf("testArrayFuncCopyAllItems(array=%p)", &array)
}

func RunPointerExamples() {
    log.SetPrefix("basics:pointers: ")

    t := testStruct{"x", 0}
    log.Printf("main testStruct=%s\n", testStructToStr(&t))
    testStructFunc(t)

    // new() will return pointer to testStruct (*testStruct that points to zeroed memory)
    var v *testStruct = new(testStruct)
    log.Printf("new(testStruct): %s\n", testStructToStr(v))

    var x testStruct = testStruct{}
    log.Printf("var(testStruct): %s\n", testStructToStr(&x))

    // > Remember that make applies only to maps, slices and channels and does not return a pointer
    // allocate array of 2 items (memory is not zeroed):
    var y []testStruct = make([]testStruct, 2)
    log.Printf("y=%p; y[0] = %s; y[1] = %s\n", &y, testStructToStr(&y[0]), testStructToStr(&y[1]))

    testArrayFuncNoCopy(&y)
    testArrayFuncCopyAllItems(y)

    wrongIdx := 2
    if wrongIdx < len(y) {
        // below code will produce panic with following msg:
        // panic: runtime error: index out of range [2] with length 2
        y[wrongIdx].a = "b"
    }
}
