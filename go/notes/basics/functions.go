package basics

import (
    "fmt"
    "errors"
    "log"
    "math/rand"
    "time"
)

func init() {
    fmt.Println("init(): initializing functions.go")
}

func testArrayRandomizer() string {
    allowedValues := [] string {
        "a",
        "b",
        "c",
    }

    allowedValues[1] = "."

    return allowedValues[rand.Intn(len(allowedValues))]
}

func testErrorCall(a int32) (string, error) {
    if a == 0 {
        return fmt.Sprintf("%d", a), errors.New("found 0")
    }

    return "OK", nil
}

func reverseIntArray(in []int32) []int32 {
    // Allocate len(in) items:
    out := make([]int32, len(in))

    // Copy all items from in array to out array:
    copy(out, in)

    for i, j := 0, len(in) - 1;
        i < j;
        i, j = i + 1, j - 1 {
        out[i], out[j] = out[j], out[i]
    }

    return out
}

func multipleValuesReturn() (n int, err error) {
    // Named parameters in definition of result tuple (n and err) will be used to create
    // local variables in the scope of implemented function.
    // Usage of 'return' will make use of these locals to build result tuple so there is no need to write 'return n, err'.

    n = 10
    err = errors.New("example-error")

    return
}

func RunFunctionExamples() {
    rand.Seed(time.Now().UnixNano())
    log.SetPrefix("basics.functions: ")

    resultStr, err := testErrorCall(0)
    log.Printf("testErrorCall(0) = (%s, '%s')\n", resultStr, err)

    log.Printf("testArrayRandomizer: '%s'", testArrayRandomizer())

    testIntArray := [] int32 { 1, 2, 3 }
    reversedTestIntArray := reverseIntArray(testIntArray)
    log.Printf("reverseIntArray(%s) -> %s\n", fmt.Sprint(testIntArray), fmt.Sprint(reversedTestIntArray))
}
