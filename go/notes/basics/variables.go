package basics

import (
    "fmt"
)

var testVar int = 10

var (
    a string = "a"
    b float64 = 0.1
    c bool = false
    d uint8 = 255
)

func init() {
    fmt.Println("init(): initializing variables.go")
}

func RunVarExamples() {
    // Defining list of struct items in C style:
    items := [] struct {
        a string
        b func(a string) string
    } {
        {"a", func(a string) string { return fmt.Sprintf("[%s]", a) } },
        {"b", func(a string) string { return fmt.Sprintf("{%s}", a) } },
    }

    for i, item := range items {
        fmt.Printf("vars: item = items[%d]; item.a = '%s'; item.b(item.a) = '%s'\n",
            i, item.a, item.b(item.a))
    }

    a = "b"

    fmt.Printf("vars: a, b, c, d: '%s', %g, %t, %d, %d\n", a, b, c, d, testVar)
}

