package main

import (
    // all capital letter types, functions and constants will be imported from 'basics/exclass' dir:
    "notes/basics/exclass"

    "notes/basics"
)

func main() {
    basics.RunFunctionExamples()
    basics.RunPointerExamples()
    basics.RunVarExamples()
    basics.RunSwitchExample()
    basics.RunSlicesExamples()
    basics.RunMapExamples()
    basics.RunInterfacesExamples()
    basics.RunConcurrencyExamples()
    basics.RunRecoverExample()

    exclass.Test()
}
