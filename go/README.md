## Golang snippets

Installing go:

- visit [go.dev](https://go.dev/) and follow instructions

Initializing project:

```
mkdir project
cd project
go mod init project
# <create and edit main.go file>
```

After importing some external packages do `go mod tidy` to download all dependencies, it'll also update `go.mod` file.

Building project:

```
go build
```

Launching app:

```
go run . # or ./project
```

Testing (all `Test...(t *T.Testing)` functions from `*_test.go` files will be executed):

```
go test -v
```

## Refs

- (https://go.dev/doc/tutorial/getting-started)
- (https://blog.francium.tech/go-modules-go-project-set-up-without-gopath-1ae601a4e868)
- (https://go.dev/doc/effective_go)
