package handlers

import "github.com/gin-gonic/gin"
import "rsc.io/quote"

func HandlePing(c *gin.Context) {
    c.JSON(200, gin.H{
        "message": quote.Go(),
    })
}
