package main

import (
    "github.com/gin-gonic/gin"
    "fmt"
    "ginexample/handlers"
)

func main() {
    r := gin.Default()

    fmt.Println("Listening on 127.0.0.1/8080 port...")

    r.GET("/ping", handlers.HandlePing)
    r.GET("/test", func(c *gin.Context) {
        c.JSON(200, gin.H{ "message": "/test"})
    })

    r.Run()
}
